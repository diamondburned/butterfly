#!/usr/bin/env bash
. config # loads config

BEARER=$(printf "$SPOTIFY_CLIENT_ID:$SPOTIFY_CLIENT_SECRET" | base64 -w 0 -)
CURL_TOKEN=$(curl -s -X "POST" -H "Authorization: Basic ${BEARER}" -d "grant_type=client_credentials" https://accounts.spotify.com/api/token | sh JSON.sh)

ACCESSTOKEN=$(echo "$CURL_TOKEN" | grep -F '["access_token"]' | awk -F '	' '{print $2}' | cut -d '"' -f 2)
