#!/usr/bin/env bash
function err {
	echo "Error triggered, exiting"
}
trap err ERR

set -- $1

urlencode() {
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
		*) printf "$c" | xxd -p -c1 | while read x;do printf "%%%s" "$x";done
	esac
done
}

if [[ $1 = "track" || $1 = "album" || $1 = "artist" || $1 = "trackid" || $1 = "trackid-url" ]]; then
	STRING="${@:2}"
else
	echo "Invalid args!"
	exit 1
fi

. spotify/access-token.sh # $ACCESSTOKEN

STRING=$(urlencode "$STRING")

if [[ $1 = "track" ]]; then
	searchurl='https://api.spotify.com/v1/search?type=track&limit=5&q='"$STRING"
	TYPE="track"
elif [[ $1 = "album" ]]; then
	searchurl='https://api.spotify.com/v1/search?type=album&limit=1&q='"$STRING"
	TYPE="album"
elif [[ $1 = "artist" ]]; then
	searchurl='https://api.spotify.com/v1/search?type=artist&limit=1&q='"$STRING"
	TYPE="artist"
elif [[ $1 = "trackid" ]]; then
	searchurl='https://api.spotify.com/v1/tracks/'"$STRING"
	TYPE="trackid"
elif [[ $1 = "trackid-url" ]]; then
	searchurl='https://api.spotify.com/v1/tracks/'"$STRING"
	TYPE="trackid-url"
else
	echo "Invalid args!"
	exit 1
fi

CURL=$(curl -s -H "Authorization: Bearer ${ACCESSTOKEN}" "${searchurl}" | sh JSON.sh -b)

checkCURLnull() {
	if [[ $CURL_null = "0" ]]; then
		echo Nothing found
		exit 0
	fi
}

if [[ $TYPE = "track" ]]; then 
	CURL_null=$(echo "$CURL" | grep -F '["tracks","total"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
	checkCURLnull
	echo "Showing 5 results"
	for e in {0..4}; do
		CURL_TRACK=$(echo "$CURL" | grep -F '["tracks","items",'"$e"',"name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
		CURL_ARTIST=$(echo "$CURL" | grep -F '["tracks","items",'"$e"',"artists",0,"name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
		CURL_URL=$(echo "$CURL" | grep -F '["tracks","items",'"$e"',"external_urls","spotify"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
		LINE1='#'"$((${e}+1))"' / ['"$CURL_ARTIST"' - '"$CURL_TRACK"']('"$CURL_URL"')'
		echo "$LINE1"
	done
elif [[ $TYPE = "artist" ]]; then
	CURL_null=$(echo "$CURL" | grep -F '["artists","total"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
	checkCURLnull
	CURL_ARTIST=$(echo "$CURL" | grep -F '["artists","items",0,"name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	CURL_URL=$(echo "$CURL" | grep -F '["artists","items",0,"external_urls","spotify"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	LINE1="$CURL_ARTIST"
	echo '**'"$LINE1"'** / '"$CURL_URL" # line3
elif [[ $TYPE = "album" ]]; then
	CURL_null=$(echo "$CURL" | grep -F '["albums","total"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
	checkCURLnull
	CURL_ALBUM=$(echo "$CURL" | grep -F '["albums","items",0,"name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	CURL_ARTIST=$(echo "$CURL" | grep -F '["albums","items",0,"artists",0,"name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	CURL_URL=$(echo "$CURL" | grep -F '["albums","items",0,"external_urls","spotify"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	LINE1="$CURL_ARTIST"': '"$CURL_ALBUM"
	echo '**'"$LINE1"'** / '"$CURL_URL" # line3
elif [[ $TYPE =~ "trackid" ]]; then
	CURL_null=$(echo "$CURL" | grep -F '["track_number"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
	checkCURLnull
	CURL_TRACK=$(echo "$CURL" | grep -F '["name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	CURL_ARTIST=$(echo "$CURL" | grep -F '["album","artists",0,"name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
	CURL_ALBUM=$(echo "$CURL" | grep -F '["album","name"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//') 
	CURL_URL=$(echo "$CURL" | grep -F '["external_urls","spotify"]' | awk -F '	' '{print $2}' | sed -e 's/^"//' -e 's/"$//')
	LINE1="$CURL_ARTIST"': '"$CURL_ALBUM"
	if [[ $TYPE = "trackid-url" ]]; then 
		echo "$CURL_URL"
	else
		echo '**'"$LINE1"'** / '"$CURL_URL" # line3
	fi
else
	exit 1
fi

