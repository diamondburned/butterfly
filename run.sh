#!/usr/bin/env bash
set -e

cd $HOME/butterfly/

if [[ ! -f oppai/oppai ]]; then
	git submodule init
	git submodule update

	(
		cd oppai/
		dir="$(dirname "$0")"
		. "$dir"/cflags
		$cc $cflags "$@" -DOPPAI_IMPLEMENTATION main.c oppai.c $ldflags -o oppai
	)
fi

[[ ! -d $HOME/.logs ]] && mkdir -p $HOME/.logs

[[ ! -d tmp/ ]] && mkdir -p tmp/

#if [[ ! $1 ]]; then
	if [[ "$-" =~ i || $1 ]]; then
		exec node bot.js
	else
		echo Logging into $HOME/.logs/butterfly.log
		exec node bot.js &>> $HOME/.logs/butterfly.log
	fi
#else
	"$@"
#fi
