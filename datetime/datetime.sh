#!/usr/bin/env bash
function isinTZ() {
	timedatectl list-timezones | grep -F "$1" &> /dev/null
	return $?
}

function setTime() {
	if isinTZ "$1"; then
		redis-cli set "${2}:timezone" "$1" 1> /dev/null || { echo "Can't write to database! Please report!"; exit 0; }
		echo "Saved \`$1\`."
	else
		echo 'Invalid timezone!'
		echo 'Refer to `~time <city>` for the Linux Timezone code.'
		echo '> Example: `America/Los_Angeles`' 
	fi
}

function getTime() {
	export TZ="$1"
	printf "%(%A, %B %d, %Y, %I:%M %p)T"
}

snowflake="$1"

arr=( ${@:2} )
set -- "${arr[@]}"

STRING=()
until [[ $# = 0 ]]; do
	case $1 in
		-s|--set)
			setTime "$2" "$snowflake"
			exit
			;;
		*)
			STRING+=( "$1" )
			shift
			;;
	esac
done

set -- "${STRING[@]}"

if [[ $* = "" ]]; then
	timezone=$(redis-cli get "${snowflake}:timezone")
	if [[ -z $timezone ]]; then
		exit 1
	else
		echo "$timezone"
		getTime "$timezone"
		exit
	fi
fi

string="$@"
CMD="
var cityTimezones = require('city-timezones');
const cityLookup = cityTimezones.lookupViaCity('$string');
console.log(cityLookup)
"

NODE=$(node -e "$CMD")
if [[ "$?" -gt 0 || -z $NODE ]]; then
	exit 1
fi

IFS=$'\n'
CITY=( $(echo "$NODE" | grep -F "city_ascii:" | grep -o -P "(?<=\').*(?=\')") )

COUNTRY=( $(echo "$NODE" | grep -F "country:" | grep -o -P "(?<=\').*(?=\')") )

TIMEZONE=( $(echo "$NODE" | grep -F "timezone:" | grep -o -P "(?<=\').*(?=\')") )

echo 'Timezones found: '${#TIMEZONE[@]}
INDEX=0
for i in "${TIMEZONE[@]}"; do
	echo '**'"${CITY[$INDEX]}"', '"${COUNTRY[$INDEX]}"'** [`'"${TIMEZONE[$INDEX]}"'`]'
	getTime "${TIMEZONE[$INDEX]}"
	INDEX=$((INDEX+1))
done
