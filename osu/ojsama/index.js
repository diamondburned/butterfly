var readline = require("readline");
var osu = require("./ojsama");

var mods = osu.modbits.none;
var acc_percent;
var combo;
var nmiss;

var argv = process.argv;

for (var i = 2; i < argv.length; ++i) {
	if (argv[i].startsWith("+")) {
		mods = osu.modbits.from_string(argv[i].slice(1) || "");
	}

	else if (argv[i].endsWith("%")) {
		acc_percent = parseFloat(argv[i]);
	}

	else if (argv[i].endsWith("x")) {
		combo = parseInt(argv[i]);
	}

	else if (argv[i].endsWith("xmiss")) {
		nmiss = parseInt(argv[i]);
	}
}

var jsonBody = {
	"mods": "",
	"stars": "",
	"combo": "",
	"maxcombo": "",
	"accuracy": "",
	"pp": ""
}

var parser = new osu.parser();
readline.createInterface({
  input: process.stdin, terminal: false
}).on("line", parser.feed_line.bind(parser))
  .on("close", function() {
	// The beatmap file 
	var map = parser.map;

	// Looks to be the entire map
	//console.log(map.toString());

	var stars = new osu.diff().calc({
		map: map, 
		mods: mods
	});

	var pp = osu.ppv2({
		stars: stars,
		combo: combo,
		nmiss: nmiss,
		acc_percent: acc_percent,
	});

	var max_combo = map.max_combo();
	combo = combo || max_combo;

	jsonBody.mods = osu.modbits.string(mods);
	jsonBody.combo = combo;
	jsonBody.maxcombo = max_combo;
	jsonBody.pp = pp.total;
	jsonBody.stars = stars.total;
	jsonBody.accuracy = pp.computed_accuracy;

	console.log(JSON.stringify(jsonBody))
});