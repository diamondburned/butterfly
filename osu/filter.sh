#!/usr/bin/env bash
. config
source bin/funcs.bash

genreParse() {
	read -d"" -r input

	declare -a genres
	genres[0]="Any"
	genres[1]="Unspecified"
	genres[2]="Video Game"
	genres[3]="Anime"
	genres[4]="Rock"
	genres[5]="Pop"
	genres[6]="Other"
	genres[7]="Novelty"
	genres[9]="Hip Hop"
	genres[10]="Electronic"

	genre=${genres[$input]}
	[[ -z $genre ]] && echo "N/A" || echo "$genre"
}

languageParse() {
	read -d"" -r input

	declare -a languages 
	languages[0]="Any"
	languages[1]="Other"
	languages[2]="English"
	languages[3]="Japanese"
	languages[4]="Chinese"
	languages[5]="Instrumental"
	languages[6]="Korean"
	languages[7]="French"
	languages[8]="German"
	languages[9]="Swedish"
	languages[10]="Spanish"
	languages[11]="Italian"

	language=${languages[$input]}
	[[ -z $language ]] && echo "N/A" || echo "$language"
}

# It will just not return a plot if it's not installed
#[[ -z $(command -v gnuplot) ]] && {
#	echo '`gnuplot` is not installed.'
#	exit
#}

snowflake="$1"
shift

[[ -z "$gamemode" ]] && gamemode=0

input="$@"
string=$(echo "$input" | tr ' ' '\n' | grep -F "https://osu.ppy.sh/")
IFS=$'\n　　'
if [[ $string = *".ppy.sh/b/"* || $string = *".ppy.sh/beatmaps/"* ]] || [[ $string = *"#"* && $string = "https://osu.ppy.sh/beatmapsets/"* ]]; then
	id=$(echo "${string##*/}" | sed 's/?.*//')
	gamemode=$(osugm "$(echo "${string##*\#}" | cut -d/ -f1)")
	[[ -z "$gamemode" ]] && gamemode=0
	FILE=$(curl -s "osu.ppy.sh/api/get_beatmaps?k=$API&b=$id&m=${gamemode}" | sh JSON.sh -b) || { echo "Unknown link"; exit 1; }
	bmid=$id
	bmsid=$(jsonparse "$FILE" '"beatmapset_id"]' | lastlineP)
elif [[ $string = *".ppy.sh/s/"* ]]; then
	bmsid=$(echo $string | cut -d'/' -f5 ) 
	FILE=$(curl -s "osu.ppy.sh/api/get_beatmaps?k=$API&s=$bmsid&m=${gamemode}" | sh JSON.sh -b) || { echo "Unknown link"; exit 1; }
	readarray bmid <<< "$(jsonparse "$FILE" '"beatmap_id"]	')"
elif [[ $string = *".ppy.sh/beatmapsets"* ]]; then
	bmsid="${string//#*/}"; bmsid="${bmsid##*/}"
	FILE=$(curl -s "osu.ppy.sh/api/get_beatmaps?k=$API&s=$bmsid&m=${gamemode}" | sh JSON.sh -b) || { echo "Unknown link"; exit 1; }
	readarray bmid <<< "$(jsonparse "$FILE" '"beatmap_id"]	')"
else
	echo Unknown link
	exit
fi

[[ $FILE = "[]" ]] && { echo "Data is not available, please try again later."; exit; }

FILENAME="tmp/${bmsid}.mp3"
wget -q -O "$FILENAME" "https://b.ppy.sh/preview/${bmsid}.mp3" &

title=$(jsonparse "${FILE}" '"title"]' | lastlineP)
[[ -z "$title" ]] && echo "Unknown link" && exit 1

artist=$(jsonparse "${FILE}" '"artist"]' | lastlineP)
source=$(jsonparse "${FILE}" '"source"]' | lastlineP)
[[ ! "$id" ]] && tags=$(jsonparse "${FILE}" '"tags"]' | lastlineP | escMD)
creator=$(jsonparse "${FILE}" '"creator"]' | lastlineP)
creator_id=$(jsonparse "${FILE}" '"creator_id"]' | lastlineP)

osumode=$(osugm "$(jsonparse "${FILE}" '"mode"]' | lastlineP)")

genre=$(jsonparse "${FILE}" '"genre_id"]' | lastlineP | genreParse)
language=$(jsonparse "${FILE}" '"language_id"]' | lastlineP | languageParse)

approved=$(jsonparse "${FILE}" '"approved"]' | lastlineP)
last_update=$(jsonparse "${FILE}" '"last_update"]' | lastlineP)

# Hey Stevy, I heard you added timestamp into your osu-only bot.
# My friend used to tell me my bot should only be an osu bot.
# Well guess what? Turns out I can do better than you.
timezone=$(redis-cli get "${snowflake}:timezone")
if [[ -z $timezone || -z $snowflake ]]; then
	export TZ="Europe/London"
else
	export TZ="$timezone"
fi

approved_string=$(TZ="$TZ" date -d "$last_update" +"%A, %B %d, %Y, %I:%M %p")

length=$(convertsecs $(jsonparse "${FILE}" '"hit_length"]' | lastlineP))
bpm=$(jsonparse "${FILE}" '"bpm"]' | lastlineP)

echo "$artist - $title"
echo 'https://osu.ppy.sh/beatmapsets/'${bmsid}
echo 'https://b.ppy.sh/thumb/'"${bmsid}"'l.jpg'
echo $creator
echo "https://osu.ppy.sh/u/${creator_id}"
echo 'https://a.ppy.sh/'"$creator_id"'?query='"$((RANDOM%100))"
echo "$osumode / $(statuscode "$approved") $approved_string (${TZ})"
echo $FILENAME

IFS=$'\n' readarray -t __vers <<< "$(jsonparse "$FILE" '"version"]')"
IFS=$'\n' readarray -t __star <<< "$(jsonparse "$FILE" '"difficultyrating"]')"
IFS=$'\n' readarray -t __CS <<< "$(jsonparse "$FILE" '"diff_size"]')"
IFS=$'\n' readarray -t __OD <<< "$(jsonparse "$FILE" '"diff_overall"]')"
IFS=$'\n' readarray -t __AR <<< "$(jsonparse "$FILE" '"diff_approach"]')"
IFS=$'\n' readarray -t __HP <<< "$(jsonparse "$FILE" '"diff_drain"]')"
IFS=$'\n' readarray -t __max_combo <<< "$(jsonparse "$FILE" '"max_combo"]' | thousandths)"
IFS=$'\n' readarray -t __playcount <<< "$(jsonparse "$FILE" '"playcount"]' | thousandths)"
IFS=$'\n' readarray -t __passcount <<< "$(jsonparse "$FILE" '"passcount"]' | thousandths)"
IFS=$'\n' readarray -t __favourite_count <<< "$(jsonparse "$FILE" '"favourite_count"]' | thousandths)"

[[ "$source" ]] && source=$'\n'"　　Source: $source"

info=$(cat<<EOF
__Beatmap information__
　　Length: $length
　　BPM: ${bpm}${source}
　　Genre: ${genre}
　　Language: ${language}
　　Favorites: \\❤${__favourite_count}
EOF
)

if [[ "$id" ]]; then
	__bmdata=$(fetchBeatmap "$id")
	[[ "$__bmdata" != "osu file format"* ]] && {
		echo ""
	} || {
		echo "${__bmdata}" | osu/oppai - -ognuplot | plot "$id"
	}

	#echo "__[${__vers}](https://osu.ppy.sh/b/${bmid})__ (\❤${__favourite_count} / \🎮${__playcount} / \🏆${__passcount})"
	#echo "　　**${__star}**★ / AR${__AR} OD${__OD} CS${__CS} HP${__HP} ${__max_combo}x FC"
else 
	echo "" # plotfile

fi

echo "$info"
echo 
s=0
sortCache=""

for i in "${__star[@]}"; do
	[[ -z "${__star[$s]}" ]] && ((s++)) && continue
	sortCache+="${__star[$s]}|__[${__vers[$s]}](https://osu.ppy.sh/b/${bmid[$s]//$'\n'})__ / \\🎮${__playcount[$s]} / \\🏆${__passcount[$s]}AR${__AR[$s]} OD${__OD[$s]} CS${__CS[$s]} HP${__HP[$s]} ${__max_combo[$s]}x FC"$'\n'
	((s++))
done

IFS=$'\n' readarray -t sorted <<< "$(echo "$sortCache" | sort -n -t\|)"

for s in "${sorted[@]}"; do
	[[ -z "$s" ]] && continue
	value=${s#*|}
	star=$(roundpipe 2 <<< "${s%|*}")
	echo "${value%*}"
	echo $'　　'"**${star}**★ / ${value#*}"
done

echo

[[ "$id" ]] && exit
echo "__Tags__"
echo $'　　'"$tags"



exit

