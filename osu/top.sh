#!/usr/bin/env bash
round() {
	printf "%.$1f" "$2"
}

smol() {
	normal=('1' '2' '3' '4' '5' '6' '7' '8' '9' '0' 'p')
	small=('₁' '₂' '₃' '₄' '₅' '₆' '₇' '₈' '₉' '₀' 'ₚ')
	CACHE="$@"
	for each in {0..10}; do
		CACHE=$(echo "${CACHE}" | sed "s~${normal[$each]}~${small[$each]}~g")
	done
	echo "$CACHE"
}

bitmod() {
	local modvalue="$1"
	local e=0
	local bits=( 0 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 16777216 33554432 67108864 134217728 268435456 )
	local mods=( "" "NF" "EZ" "" "HD" "HR" "SD" "DT" "RX" "HT" "NC" "FL" "Auto" "SO" "AP" "Perfect" "Key4" "Key5" "Key6" "Key7" "Key8" "FadeIn" "Random" "LastMod" "Key9" "Key10" "Key1" "Key3" "Key2" )
	local OUTPUT=()
	if [[ $modvalue -ne "0" ]]; then
		until [[ $e = ${#bits[@]} ]]; do
			local BITINCLUDE=$(( $modvalue & ${bits[$e]} ))
			if [[ $BITINCLUDE -ne 0 ]]; then
				OUTPUT+=( "${mods[$e]}" )
			fi
			((e++))
		done
		printf '%s' "${OUTPUT[@]}"
	else
		echo -n ""
	fi
};

displaytime() {
	local T=$1
	local D=$((T/60/60/24))
	local H=$((T/60/60%24))
	local M=$((T/60%60))
	local S=$((T%60))
	(( $D > 0 )) && printf '%dd ' $D
	(( $H > 0 )) && printf '%dh ' $H
	(( $M > 0 )) && printf '%dm ' $M
	(( $D > 0 || $H > 0 || $M > 0 )) && printf ''
	printf '%ds\n' $S
};

urlencode() {
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
		*) printf "$c" | xxd -p -c1 | while read x;do printf "%%%s" "$x";done
	esac
done
}

details=1

FAIL=0

if [[ $1 = "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
elif [[ $1 = "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
elif [[ $1 = "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi

uuid=$2
. config
source bin/funcs.bash

shift
shift
args=( $@ )
set -- "${args[@]}"

RIPPLE="false"

shift=1

STRING=()
until [[ $# = 0 ]]; do
	key="$1"
	case $key in
		--ripple|-r)
			RIPPLE="true"
			mode="ripple"
			shift
			;;
		--shift|-s)
			shift=$(sed 's/[^0-9]*//g' <<< "$2")
			shift
			shift
			;;
		*)
			STRING+=( "$1" )
			shift
			;;
	esac
done

if ! [[ $shift -ge 1 && $shift -le 20 ]]; then
	echo "\`-s [n]\` in which \`n\` has to be in the range of 1-20."
	exit
fi

shift=$(( shift - 1 ))

set -- "${STRING[@]}"
input="$@"

command="$input"

[ -z $mode ] && mode="osu"

if [[ $command = "" ]]; then
	IFS=$'\n'
	REDIS=( $(redis-cli get "${uuid}:${mode}") ) || exit 1
	ENTRY="${REDIS[0]}"
	alias="${REDIS[*]:1}"
	[[ $alias = $ENTRY ]] && alias=""

	if [[ -z $ENTRY ]]; then
		if [[ $RIPPLE = "true" ]]; then
			echo $'You haven\'t set a username yet! Set one with `~osuset --ripple [username]`!'
			exit 
		else
			echo $'You haven\'t set a username yet! Set one with `~osuset [username]`!'
			exit 
		fi
	fi
	username="$ENTRY"
else
	username="$command"
fi

rangestart=$(( 5*shift ))
rangeend=$(( rangestart + 4 ))
ENTRY=$rangestart

if [[ $RIPPLE = "true" ]]; then
	URLname=$(urlencode "$username")
	CURL=$(curl -s "ripple.moe/api/get_user_best?u=$URLname&m=$gamemode&limit=$(( rangeend + 1 ))" | sh JSON.sh -b) || { echo "Error! API fetching failed!" ; exit 1; }
else
	URLname=$(urlencode "$username")
	CURL=$(curl -s "osu.ppy.sh/api/get_user_best?k=$API&u=$URLname&m=$gamemode&limit=$(( rangeend + 1 ))" | sh JSON.sh -b) || { echo "Error! API fetching failed!" ; exit 1; }
fi

if [[ $CURL = "[]" ]]; then
	echo Invalid username! 
	exit
fi

if [[ $(echo "$CURL" | grep -F '[') = "" ]]; then
	echo "No recent is found, invalid username or user is most likely inactive..."
	exit 0
fi

osuID=$(curl -s -I https://osu.ppy.sh/u/"$username" | grep -F 'location: ' | cut -d':' -f2- | cut -d'/' -f5 | sed 's/[^0-9]*//g') || { osuID="?" ;}

CURLuid=$(echo "$CURL"		| grep -F '[0,"user_id"]'		| cut -d'	' -f2	| sed -e 's/^"//' -e 's/"$//')

IFS=$'\t\n'
ARRAY=()

while true; do
	CURLbmid=$(jsonparse "$CURL" '['${ENTRY}',"beatmap_id"]	')
	CURLscore=$(jsonparse "$CURL" '['${ENTRY}',"score"]	')
	CURLcombo=$(jsonparse "$CURL" '['${ENTRY}',"maxcombo"]	')
	CURL50=$(jsonparse "$CURL" '['${ENTRY}',"count50"]	')
	CURL100=$(jsonparse "$CURL"	'['${ENTRY}',"count100"]	')
	CURL300=$(jsonparse "$CURL"	'['${ENTRY}',"count300"]	')
	CURLmiss=$(jsonparse "$CURL" '['${ENTRY}',"countmiss"]	')
	CURLkatu=$(jsonparse "$CURL" '['${ENTRY}',"countkatu"]	')
	CURLgeki=$(jsonparse "$CURL" '['${ENTRY}',"countgeki"]	')
	CURLperfect=$(jsonparse "$CURL"	'['${ENTRY}',"perfect"]	')
	CURLmods=$(jsonparse "$CURL" '['${ENTRY}',"enabled_mods"]	')
	CURLdate=$(jsonparse "$CURL" '['${ENTRY}',"date"]	')
	CURLrank=$(jsonparse "$CURL" '['${ENTRY}',"rank"]	')
	CURLpp=$(jsonparse "$CURL" '['${ENTRY}',"pp"]	' | roundpipe 2)
	
	if [[ -z $CURLpp ]]; then break; fi

	## DATE FUNCTION
		TIMEepoch=$(command date -d"${CURLdate}" -u +%s)
		NOWepoch=$(printf "%(%s)T")
		if [[ $RIPPLE = "true" ]]; then
			DIFF=$(( $NOWepoch - ( $TIMEepoch + 21600) ))
		else
			DIFF=$(( $NOWepoch - $TIMEepoch ))
		fi
		TIMEAGO=$(displaytime ${DIFF})
		HAPPENED="$TIMEAGO ago"
	
	#MODS=$(bitmod "$CURLmods" | sed 's~\n~~g')
	case $CURLmods in
		0)	MODS="";;
		*)	MODS="$(bitmod $CURLmods)";;
	esac

	if [[ ! -z $MODS ]]; then 
		if [[ $MODS =~ "NC" && $MODS =~ "DT" ]]; then
			MODS="+${MODS//DT/}"
		else
			MODS="+$MODS"
		fi
	fi

	WEIGHEDpp=$(bin/weighing.zsh "$(( ${ENTRY} + 1 ))" "${CURLpp}" | roundpipe 2)
	smolweigh=$(smol "${WEIGHEDpp}")
	
	CURLbmid_CURL=$(fetchBeatmap "$CURLbmid" | ./osu/oppai - $MODS -ojson | sh JSON.sh)

	CURLbmid_CURLartist=$(jsonparse "$CURLbmid_CURL" '["artist"]')
	CURLbmid_CURLtitle=$(jsonparse "$CURLbmid_CURL" '["title"]')
	CURLbmid_CURLversion=$(jsonparse "$CURLbmid_CURL" '["version"]')
	
	info="${CURLbmid_CURLartist} - ${CURLbmid_CURLtitle} [${CURLbmid_CURLversion}]"
	# aritst title diffname

	STARS=$(jsonparse "$CURLbmid_CURL" '["stars"]' | roundpipe 2)

	#CURLcombo

	CURLbmid_CURLmax_combo=$(jsonparse "$CURLbmid_CURL" '["max_combo"]')
	[[ $CURLbmid_CURLmax_combo = "null" ]] && CURLbmid_CURLmax_combo="?"

	COMBO="${CURLcombo}/${CURLbmid_CURLmax_combo}x"
	ACCURACY="$(bin/acc.zsh "$gamemode" "$CURL50" "$CURL100" "$CURL300" "$CURLmiss" "$CURLkatu" "$CURLgeki")"

	#echo ${ENTRY}
	RESULTS="**$(( ${ENTRY} + 1 ))"'/** ['"$info"'](https://osu.ppy.sh/b/'"$CURLbmid"')'" ${MODS} / ${STARS}★"' \\n'"$(rank-to-emoji "$CURLrank") ▷**${CURLscore}** / **${CURLpp}**pp ${smolweigh}ₚₚ / $COMBO $ACCURACY "'\\n'"$HAPPENED"'\\n'

	ARRAY+=( "$RESULTS" )

	if [[ $ENTRY -lt $rangeend ]]; then
		ENTRY=$(( ENTRY + 1 ))
	else
		break
	fi
done

[[ $RIPPLE = "true" ]] && server="Ripple" || server="osu!"
[[ ! -z "$alias" ]] && username="${username} (${alias})"

echo "$gametype"' / Top plays for '"${username}, page $((shift+1)) (${server})"
echo 'https://osu.ppy.sh/u/'"$CURLuid"
#echo 'https://osu.ppy.sh/images/flags/'"$CURLcountry"'.png'
echo 'https://a.ppy.sh/'"$CURLuid"'?query='"$((RANDOM%100))"
IFS=$'\n'
OUTPUT=$(printf '%s\n' "${ARRAY[@]}" | sed 's~\\\\n~\n~g')
echo -n "$OUTPUT"
