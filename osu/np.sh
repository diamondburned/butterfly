#!/usr/bin/env bash
set -eo pipefail

function err {
	echo "Error triggered, exiting"
}
trap err ERR
STATE="$1"
shift
args=( $@ )
set -- "${args[@]}"

ARTIST=$(echo "$@" | awk -F ' - ' '{print $1}')
TITLE=$(echo "$@" | awk -F ' - ' '{print $2}' | sed 's~\[.*\]~~g')
DIFFNAME=$(echo "$@" | awk -F ' - ' '{print $2}' | sed 's~.*\[~\[~g' | sed -e 's/^\[//' -e 's/\]$//')

if [[ $STATE = "Clicking circles" ]]; then
	MAPSEARCH=$(. osu/mapsearch.sh "title ${TITLE} | artist ${ARTIST} | diffname ${DIFFNAME} | exact | standard")
elif [[ $STATE = "Banging drums" ]]; then
	MAPSEARCH=$(. osu/mapsearch.sh "title ${TITLE} | artist ${ARTIST} | diffname ${DIFFNAME} | exact | taiko")
elif [[ $STATE = "Catching fruit" ]]; then
	MAPSEARCH=$(. osu/mapsearch.sh "title ${TITLE} | artist ${ARTIST} | diffname ${DIFFNAME} | exact | ctb")
elif [[ $STATE = "Smashing keys" ]]; then
	MAPSEARCH=$(. osu/mapsearch.sh "title ${TITLE} | artist ${ARTIST} | diffname ${DIFFNAME} | exact | mania")
else
	MAPSEARCH=$(. osu/mapsearch.sh "title ${TITLE} | artist ${ARTIST} | diffname ${DIFFNAME} | exact")
fi

echo "$MAPSEARCH"
