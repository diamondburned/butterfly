#!/usr/bin/env bash
#set -eo pipefail

#function err {
#	echo "Error triggered, exiting"
#}
#trap err ERR

. bin/funcs.bash
. config

if [[ $2 = "NPmode" ]]; then
	NPmode=1
fi

args=( $@ )
set -- "${args[@]}"

if [[ $1 = "-s" ]]; then
				shift=$2
				shift
				shift
fi

#STRING=$(echo -e "${*//\| /\\n}")
STRING=$(echo $* | sed 's~| ~\n~g')

SAVEIFS=$IFS
IFS=$'\n'
ARRAY=( $STRING )
IFS=$SAVEIFS

STATUSES=()

for i in "${ARRAY[@]}"; do
	set -- $i
	POSITIONAL=()
	while [[ $# -gt 0 ]]; do
		key="$1"
		case $key in
			title)
				title=$(echo "${@:2}")
				title=$(urlencode "$title")
				shift # past argument
				shift # past value
				;;
			artist)
				artist=$(echo "${@:2}")
				artist=$(urlencode "$artist")
				shift # past argument
				shift # past value
				;;
			mapper)
				mapper=$(echo "${@:2}")
				mapper=$(urlencode "$mapper")
				shift # past argument
				shift # past value
				;;
			diffname)
				diffname=$(echo "${@:2}")
				diffname=$(urlencode "$diffname")
				shift # past argument
				shift # past value
				;;
			exact)
				EXACT=1
				shift
				shift
				;;
			standard)
				MODES="Standard"
				shift
				shift
				;;
			mania)
				MODES="Mania"
				shift
				shift
				;;
			taiko)
				MODES="Taiko"
				shift
				shift
				;;
			ctb)
				MODES="CtB"
				shift
				shift
				;;
			ranked)
				STATUSES+=("Ranked")
				shift
				shift
				;;
			qualified)
				STATUSES+=("Qualified")
				shift
				shift
				;;
			loved)
				STATUSES+=("Loved")
				shift
				shift
				;;
			*)		# unknown option
				POSITIONAL+=("$1") # save it in an array for later
				shift # past argument
				;;
		esac
	done
done

if [[ $title = "" && $artist = "" && $mapper = "" && $diffname = "" ]]; then
	echo Invalid args!
	exit
fi

[[ -z $shift ]] && shift=1

useragent="Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"
searchurl=( 'https://osusearch.com/query/?' )

if [[ ! -z $title ]]; then
	if [[ $EXACT = "1" ]]; then
		searchurl+=( "title=\"${title}\"&" )
	else
		searchurl+=( "title=${title}&" )
	fi
fi

if [[ ! -z $artist ]]; then
    if [[ $EXACT = "1" ]]; then
            searchurl+=( "artist=\"${artist}\"&" )
    else
            searchurl+=( "artist=${artist}&" )
    fi
fi

if [[ ! -z $mapper ]]; then
    if [[ $EXACT = "1" ]]; then
            searchurl+=( "mapper=\"${mapper}\"&" )
    else
            searchurl+=( "mapper=${mapper}&" )
    fi
fi

if [[ ! -z $diffname ]]; then
    if [[ $EXACT = "1" ]]; then
            searchurl+=( "diff_name=\"${diffname}\"&" )
    else
            searchurl+=( "diff_name=${diffname}&" )
    fi
fi

if [[ $MODES = "Standard" ]]; then
	searchurl+=( "modes=Standard&" )
elif [[ $MODES = "Mania" ]]; then
        searchurl+=( "modes=Mania&" )
elif [[ $MODES = "Taiko" ]]; then
        searchurl+=( "modes=Taiko&" )
elif [[ $MODES = "CtB" ]]; then
        searchurl+=( "modes=CtB&" )
fi

if ! [[ $STATUSES = "" ]]; then
	STATUSES=$(echo "${STATUSES[@]}" | tr '\n' ',')
	searchurl+=( "statuses=${STATUSES%?}" )
fi

URL=$(printf "%s" ${searchurl[@]})

JSON=$(curl -s -A "$useragent" "$URL" | sh JSON.sh)

[[ $(json-parse "$JSON" '["result_count"]') -eq 0 ]] && echo "No results found." && exit

shift=$(( shift - 1 ))
rangestart=$(( 5 * shift))
rangeend=$(( rangestart + 4))
i=$rangestart

if [[ $NPmode = 1 ]]; then
	:
else
  echo "Showing page $((shift+1)) (page \`-s $((shift+1))\`)"
	echo "${URL/\/query/\/search}"
fi

while true; do
	TITLE=$(echo "$JSON" | grep -F '["beatmaps",'$i',"title"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	ARTIST=$(echo "$JSON" | grep -F '["beatmaps",'$i',"artist"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	BPM=$(echo "$JSON" | grep -F '["beatmaps",'$i',"bpm"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	MAPPER=$(echo "$JSON" | grep -F '["beatmaps",'$i',"mapper"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	BMID=$(echo "$JSON" | grep -F '["beatmaps",'$i',"beatmapset_id"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	URL="https://osu.ppy.sh/s/${BMID}/"
	LENGTHseconds=$(echo "$JSON" | grep -F '["beatmaps",'$i',"total_length"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	LENGTHconverted=$(date -d@"${LENGTHseconds}" -u +%M:%S 2> /dev/null)
	#PASSCOUNT=$(echo "$JSON" | grep -F '["beatmaps",'$i',"pass_count"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')
	STATUScode=$(echo "$JSON" | grep -F '["beatmaps",'$i',"beatmap_status"]' | cut -d$'\t' -f2- | sed -e 's/^"//' -e 's/"$//')

	RANKED=$(statuscode "$STATUScode")

	#if ! [[ $PASSCOUNT = "0" ]]; then
	#	RANKED=' / Ranked'
	#else
	#	RANKED=''
	#fi

	if [[ $TITLE = "" ]]; then
		:
	else
		if [[ $NPmode = 1 ]]; then
			echo 'TITLE|'"$TITLE"
			echo 'ARTIST|'"$ARTIST"
			echo 'URL|'"$URL"
		else
			RESULTS+="$((i+1))/ $RANKED $ARTIST - $TITLE"
			RESULTS+=$'\n'
			RESULTS+="[osu! link](${URL}) / Length: ${LENGTHconverted} / BPM: ${BPM} / Mapper: ${MAPPER}"
			RESULTS+=$'\n'
		fi
	fi
	((i++))
	[[ $((i-1)) = $rangeend ]] && break
done

echo -ne "${RESULTS%?}"

#date -d@226 -u +%M:%S
