#!/usr/bin/env bash

bitmod() {
	local modvalue="$1"
	local e=0
	local bits=(0 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 16777216 33554432 67108864 134217728 268435456)
	local mods=("" "NF" "EZ" "" "HD" "HR" "SD" "DT" "RX" "HT" "NC" "FL" "Auto" "SO" "AP" "Perfect" "Key4" "Key5" "Key6" "Key7" "Key8" "FadeIn" "Random" "LastMod" "Key9" "Key10" "Key1" "Key3" "Key2")
	local OUTPUT=()
	if [[ $modvalue -ne "0" ]]; then
		until [[ $e == ${#bits[@]} ]]; do
			local BITINCLUDE=$(($modvalue & ${bits[$e]}))
			if [[ $BITINCLUDE -ne 0 ]]; then
				OUTPUT+=("${mods[$e]}")
			fi
			((e++))
		done
		printf '%s' "${OUTPUT[@]^^}"
	else
		echo -n ""
	fi
}

round() {
	printf "%.$1f" "$2"
}

displaytime() {
	local T=$1
	local D=$((T / 60 / 60 / 24))
	local H=$((T / 60 / 60 % 24))
	local M=$((T / 60 % 60))
	local S=$((T % 60))
	(($D > 0)) && printf '%d days ' $D
	(($H > 0)) && printf '%d hours ' $H
	(($M > 0)) && printf '%d minutes ' $M
	(($D > 0 || $H > 0 || $M > 0)) && printf 'and '
	printf '%d seconds\n' $S
}

acc() {
	ACCURACY=$(bin/acc.zsh ${gamemode} ${CURL50} ${CURL100} ${CURL300} ${CURLmiss} ${CURLkatu} ${CURLgeki})

	echo "$ACCURACY"
}

urlencode() {
	local length="${#1}"
	for ((i = 0; i < length; i++)); do
		local c="${1:i:1}"
		case $c in
		[a-zA-Z0-9.~_-]) printf "$c" ;;
		*) printf "$c" | xxd -p -c1 | while read x; do printf "%%%s" "$x"; done ;;
		esac
	done
}

if [[ $1 == "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
elif [[ $1 == "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
elif [[ $1 == "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi
uuid=$2
. config
source bin/funcs.bash
#array=( $3 )
shift
shift

args=($@)
set -- "${args[@]}"

STRING=()
until [[ $# == 0 ]]; do
	key="$1"
	case $key in
	--calc)
		CALCmode=true
		CALC="${@:2}"
		break
		;;
	--mini | -m)
		MINI="true"
		shift
		;;
	--ripple | -r)
		RIPPLE="true"
		mode="ripple"
		shift
		;;
	--shift | -s)
		offset_invalid() {
			echo "\`-s [n]\` in which \`n\` has to be greater than 0 and smaller than or equal to 10."
			exit 0
		}
		OFFSET=$(($2)) &>/dev/null || offset_invalid
		(($OFFSET > 10)) && offset_invalid
		(($OFFSET < 1)) && offset_invalid
		shift
		shift
		;;
	--mode)
		case $2 in
		mania)
			gamemode="3"
			gametype="osu! Mania"
			;;
		taiko)
			gamemode="1"
			gametype="osu! Taiko"
			;;
		ctb)
			echo "Feature not supported yet." && exit 1
			;;
		*)
			gamemode="0"
			gametype="osu!"
			;;
		esac
		shift
		shift
		;;
	--angle)
		anglerework=true
		shift
		;;
	*)
		STRING+=("$1")
		shift
		;;
	esac
done

[ -z $mode ] && mode="osu"

[ -z $OFFSET ] && OFFSET=1

#if ! [[ $LIMIT -gt 0 && $LIMIT -le 10 ]]; then
#	echo "`-s [n]` in which `n` has to be greater than 0 and smaller than or equal to 10."
#	exit 1
#fi

set -- "${STRING[*]}"

command="$*"

if [[ $command == "" || $command == " " ]]; then
	IFS=$'\n'
	REDIS=($(redis-cli get "${uuid}:${mode}")) || exit 1
	ENTRY="${REDIS[0]}"
	alias="${REDIS[*]:1}"
	[[ $alias == $ENTRY ]] && alias=""

	if [[ -z $ENTRY ]]; then
		if [[ $RIPPLE == "true" ]]; then
			echo $'You haven\'t set a username yet! Set one with `~osuset --ripple [username]`!'
			exit
		else
			echo $'You haven\'t set a username yet! Set one with `~osuset [username]`!'
			exit
		fi
	fi
	username="$ENTRY"
else
	username="$command"
fi

if [[ $RIPPLE == "true" ]]; then
	URLname=$(urlencode "$username")
	CURL=$(curl -s "ripple.moe/api/get_user_recent?u=$URLname&m=$gamemode&limit=$OFFSET" | sh JSON.sh -b) || {
		echo "Error! API fetching failed!"
		exit 1
	}
else
	URLname=$(urlencode "$username")
	CURL=$(curl -s "osu.ppy.sh/api/get_user_recent?k=$API&u=$URLname&m=$gamemode&limit=$OFFSET" | sh JSON.sh -b) || {
		echo "Error! API fetching failed!"
		exit 1
	}
fi

if [[ $CURL == "[]" ]]; then
	echo Invalid username!
	exit
fi

if [[ $(echo "$CURL" | grep -F '[') == "" ]]; then
	echo "No recent is found, user is most likely inactive..."
	#echo "u=$URLname&m=$gamemode&limit=$OFFSET"
	exit 0
fi

OFFSETarray=$((OFFSET - 1))

# CURL variables
CURLbmid=$(jsonparse "$CURL" '['$OFFSETarray',"beatmap_id"]')
CURLdate=$(jsonparse "$CURL" '['$OFFSETarray',"date"]')
CURLscore=$(jsonparse "$CURL" '['$OFFSETarray',"score"]' | thousandths)
CURLrank=$(jsonparse "$CURL" '['$OFFSETarray',"rank"]')
CURLrank="${CURLrank//H/+}"
CURLuid=$(jsonparse "$CURL" '['$OFFSETarray',"user_id"]')
CURLcombo=$(jsonparse "$CURL" '['$OFFSETarray',"maxcombo"]')
CURL50=$(jsonparse "$CURL" '['$OFFSETarray',"count50"]')
CURL100=$(jsonparse "$CURL" '['$OFFSETarray',"count100"]')
CURL300=$(jsonparse "$CURL" '['$OFFSETarray',"count300"]')
CURLmiss=$(jsonparse "$CURL" '['$OFFSETarray',"countmiss"]')
CURLkatu=$(jsonparse "$CURL" '['$OFFSETarray',"countkatu"]')
CURLgeki=$(jsonparse "$CURL" '['$OFFSETarray',"countgeki"]')
CURLmods=$(jsonparse "$CURL" '['$OFFSETarray',"enabled_mods"]')

[[ "$CALCmode" == true ]] && {
	osu/filter-pp.sh https://osu.ppy.sh/b/$CURLbmid $CALC
	exit
}

## DATE FUNCTION
	TIMEepoch=$(command date -d"${CURLdate}" -u +%s)
	NOWepoch=$(printf "%(%s)T")
	if [[ $RIPPLE = "true" ]]; then
		DIFF=$(( $NOWepoch - ( $TIMEepoch - 28800 + 21600) ))
	else
		DIFF=$(( NOWepoch - TIMEepoch ))
	fi
	TIMEAGO=$(displaytime ${DIFF})
	HAPPENED="$TIMEAGO ago"

#HAPPENED=$(displaytime $DIFF)

case $CURLmods in
0) mods="" ;;
*) mods="+$(bitmod "$CURLmods") " ;;
esac

CIRCLECOUNTS=$(circlecounts "${CURL300}" "${CURL100}" "${CURL50}" "${CURLmiss}")
#OSUCURL=$(curl -s "https://osu.ppy.sh/osu/$CURLbmid")
OSUCURL=$(fetchBeatmap "$CURLbmid") || {
	echo "osu! is downed (or at least the bot can't do it right...)"
	ACCURACY=$(acc)
	echo "$gametype / ${username} / ${CURLrank} / https://osu.ppy.sh/b/${CURLbmid} ${mods}${CURLcombo}x ${ACCURACY}"
	echo $CURLscore / $CIRCLECOUNTS
	echo
	exit 0
}

oppaiOUTPUT=$(echo "$OSUCURL" | ./osu/oppai - $mods ${CURLcombo}x ${CURL50}x50 ${CURL100}x100 ${CURLmiss}xmiss -m${gamemode} -ojson | sh JSON.sh)

BMSID=$(echo "$OSUCURL" | grep -F 'BeatmapSetID' | cut -d':' -f2 | sed 's/[^0-9]*//g')
LINE=$(($(echo "$OSUCURL" | grep -n -F '[HitObjects]' | cut -d':' -f1) + 1))
HITOBJECTS=$(echo "$OSUCURL" | tail -n +${LINE} | sed '/^\s*$/d')
TOTALOBJECTS=$(echo "$OSUCURL" | tail -n +${LINE} | sed '/^\s*$/d' | lineCount)

ACCURACY=$(acc)
oppaiACCURACY="$ACCURACY"

if [[ "$(jsonparse "$oppaiOUTPUT" $'["errstr"]\t')" != "no error" ]]; then
	#if [[ $OUTPUTerrstr = "requested a feature that isn't implemented" ]]; then
	echo "$gametype / ${username} / ${CURLrank} / https://osu.ppy.sh/b/${CURLbmid} ${mods}${CURLcombo}x ${ACCURACY}"
	#else
	#	echo "ERROR! AN UNFORTUNATE ERROR HAS HAPPENED :("
	#	echo "$oppaiOUTPUT"
	#	echo "$OSUCURL" &> tmp/taikomap
	#fi

	exit
fi

OUTPUTtitle=$(jsonparse "$oppaiOUTPUT" '["title"]')
OUTPUTartist=$(jsonparse "$oppaiOUTPUT" '["artist"]')
#OUTPUTmapper=$(jsonparse "$oppaiOUTPUT"	 '["creator"]'	)
OUTPUTdiffname=$(jsonparse "$oppaiOUTPUT" '["version"]')
OUTPUTpp=$(jsonparse "$oppaiOUTPUT" '["pp"]' | roundpipe 2)
OUTPUTstars=$(jsonparse "$oppaiOUTPUT" '["stars"]' | roundpipe 2)
OUTPUTmods=$(jsonparse "$oppaiOUTPUT" '["mods_str"]')
OUTPUTcombo=$(jsonparse "$oppaiOUTPUT" '["combo"]')
OUTPUTcombomax=$(jsonparse "$oppaiOUTPUT" '["max_combo"]')

## COMPLETION FUNCTION
LASTLINE=$(echo "$HITOBJECTS" | head -${TOTALOBJECTS} | tail -1)
TOTALTIME=$(echo "$LASTLINE" | cut -d',' -f3)
if [[ $OUTPUTmods =~ "DT" ]]; then
	TOTALintime=$(convertsecs $(echo "( ${TOTALTIME} / 1000 / 1.5 )" | bc -l | printf "%.0f" $(cat)))
else
	TOTALintime=$(convertsecs $(echo "( ${TOTALTIME} / 1000 )" | bc -l | printf "%.0f" $(cat)))
fi

if [[ $CURLrank == "F" ]]; then
	SOFAR=$(($CURL50 + $CURL100 + $CURL300 + $CURLmiss - 1))
	# v maybe -1 this
	THATLINE=$(echo "$HITOBJECTS" | head -${SOFAR} | tail -1)
	TIMESOFAR=$(echo "$THATLINE" | cut -d',' -f3)
	#STARTINGTIME=$(echo "$HITOBJECTS" | head -1 | tail -1 | cut -d',' -f3)
	COMPLETION=$(echo "( $TIMESOFAR / ( $TOTALTIME ) * 100 )" | bc -l | roundpipe 2)
	if [[ $OUTPUTmods =~ "DT" ]]; then
		THATPOINTintime=$(convertsecs $(echo "( ${TIMESOFAR} / 1000 / 1.5 )" | bc -l | printf "%.0f" $(cat)))
	else
		THATPOINTintime=$(convertsecs $(echo "( ${TIMESOFAR} / 1000 )" | bc -l | printf "%.0f" $(cat)))
	fi
fi

if [[ ! -z $OUTPUTmods ]]; then
	if [[ $OUTPUTmods =~ "NC" && $OUTPUTmods =~ "DT" ]]; then
		OUTPUTmods="+${OUTPUTmods//DT/} "
	else
		OUTPUTmods="+$OUTPUTmods "
	fi
else
	OUTPUTmods=""
fi

[[ $anglerework == true ]] && {
	rework=$(echo "${OSUCURL}" | osu/goppai-rework -c "${CURLcombo}" -n50 "${CURL50}" -n100 "${CURL100}" -nmiss "${CURLmiss}" -m "$mods") && {
		readarray data <<<"$rework"
		# line 1 pp; line 2 accuracy; line 3 mod string
		reworkStr=" / ~~∠R ${data[0]//$'\n'/} @${data[1]//$'\n'/}~~"
	}
}

if [[ $MINI == "true" ]]; then
	[[ ! -z "$alias" ]] && username="${username} (${alias})"
	[[ $CURLrank == "F" ]] && OUTPUTpp="0pp" || OUTPUTpp="**${OUTPUTpp}**pp"
	echo "${username} / ${CURLrank} / ${OUTPUTpp} / https://osu.ppy.sh/beatmaps/${CURLbmid} [${OUTPUTdiffname}] ${OUTPUTmods}${CURLcombo}/${OUTPUTcombomax}x ${ACCURACY}"

	exit 0
fi

echo "$OUTPUTartist"' - '"$OUTPUTtitle"' ['"$OUTPUTdiffname"'] '"$OUTPUTmods"'/ '"$OUTPUTstars"'★'
echo 'https://osu.ppy.sh/b/'"$CURLbmid"
echo 'https://a.ppy.sh/'"${CURLuid}"'?query='"$((RANDOM % 100))"
echo 'https://b.ppy.sh/thumb/'"${BMSID}"'l.jpg'

[[ $RIPPLE == "true" ]] && server="Ripple" || server="$gametype"
[[ ! -z "$alias" ]] && username="${username} (${alias})"
echo "${username}"' / '"$HAPPENED on $server servers"

oppaiOUTPUTfc=$(echo "$OSUCURL" | ./osu/oppai - $mods ${CURL50}x50 ${CURL100}x100 -m${gamemode} -ojson | sh JSON.sh -b)
OUTPUTppFC=$(jsonparse "$oppaiOUTPUTfc" '["pp"]' | roundpipe 2)
ACCURACYFC="?"

if [[ $CURLrank == "F" ]]; then
	#if ! [[ ${CURLmods} = "" ]]; then
	#	mods='+'"${CURLmods}"
	#fi

	echo "$(rank-to-emoji "$CURLrank") ▷"'**'"$CURLscore"'** '$'\n~~'"${OUTPUTpp}pp ${OUTPUTcombo}x ${ACCURACY}, $OUTPUTppFC"'pp FC @'"$ACCURACYFC"'~~'"$reworkStr"
	echo '**'"$ACCURACY"'** / **'"$OUTPUTcombo"'**/'"$OUTPUTcombomax"'x / '$CIRCLECOUNTS
	echo $'Completed **'"$COMPLETION"'%** ('"$THATPOINTintime"'/'"$TOTALintime"')'
else
	#if ! [[ ${CURLmods} = "" ]]; then
	#	mods='+'"${CURLmods}"
	#fi

	if [[ "$OUTPUTcombomax" != "$OUTPUTcombo" ]]; then
		PP='**'"$OUTPUTpp"'**pp '$'\n''~~'"$OUTPUTppFC"'pp FC @'"$ACCURACYFC"'~~'"$reworkStr"
	elif [[ $oppai_ACCURACY != "100"* ]]; then
		oppaiOUTPUT100=$(echo "$OSUCURL" | ./osu/oppai - $mods -m${gamemode} -ojson | sh JSON.sh -b)
		OUTPUTpp100=$(jsonparse "$oppaiOUTPUT100" '["pp"]' | roundpipe 2)
		PP='**'"$OUTPUTpp"'**pp '$'\n''~~'"$OUTPUTpp100"'pp FC @100%~~'"$reworkStr"
	else
		PP='**'"$OUTPUTpp"'**pp'$'\n'"$reworkStr"
	fi

	echo "$(rank-to-emoji "$CURLrank") ▷"'**'"$CURLscore"'** '"$PP"
	echo '**'$oppaiACCURACY'** / **'"$OUTPUTcombo"'**/'"$OUTPUTcombomax"'x / '$CIRCLECOUNTS
	echo $'Completed **100%** ('"$TOTALintime"')'
fi
