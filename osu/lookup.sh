#!/usr/bin/env bash
round() {
	echo $(printf %.$1f $(echo "scale=$1;(((10^$1)*$2)+0.5)/(10^$1)" | bc))
};

htmlparser() {
	read -r v
	printf "$v" | sed -e 's/>/>\n/g' -e 's/</\n</g' | grep -v '<' | tr -d '\n'
}

displaytime() {
	local T=$1
	local D=$((T/60/60/24))
	local H=$((T/60/60%24))
	local M=$((T/60%60))
	local S=$((T%60))
	(( $D > 0 )) && printf '%d days ' $D
	(( $H > 0 )) && printf '%d hours ' $H
	(( $M > 0 )) && printf '%d minutes ' $M
	(( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
	printf '%d seconds\n' $S
};

timeago() {
	read -r CURLdate
	local YEAR=$(echo "$CURLdate" | awk '{print $1}' | awk -F '-' '{print $1'})
	local MONTH=$(echo "$CURLdate" | awk '{print $1}' | awk -F '-' '{print $2'})
	local DAY=$(echo "$CURLdate" | awk '{print $1}' | awk -F '-' '{print $3'})
	local TIME=$(echo "$CURLdate" | awk '{print $2}')
	local FULLTIME="${MONTH}/${DAY}/${YEAR} ${TIME}"
	local TIMEepoch=$(command date -d"${FULLTIME}" -u +%s)
	local NOWepoch=$(TZ=Australia/Perth date +%s)
	if [[ $RIPPLE = "true" ]]; then
		local DIFF=$(( $NOWepoch - ( $TIMEepoch - 28800 + 21600) ))
	else
		local DIFF=$(( $NOWepoch - ( $TIMEepoch ) ))
	fi
	echo "$(displaytime ${DIFF}) ago"
}

if [[ $1 = "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
elif [[ $1 = "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
elif [[ $1 = "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi
export uuid=$2
. config
. bin/funcs.bash
#array=( $3 )

args=( ${@:3} )
set -- "${args[@]}"

command=()

josn-parse() {
	JSONsh="$1"
	KEY="${@:2}"
	LINE=$(grep -F "$KEY" <<< "$JSONsh")
	STRING="${LINE#*	}"
	STRING="${STRING/#\"/}"
	STRING="${STRING//\\/}"
	printf "${STRING/%\"/}"
}

until [[ $# = 0 ]]; do
	case "$1" in 
		'--ripple')
			RIPPLE="true"
			mode="ripple"
			shift
			;;
		'--details'|'-d')
			DETAILS="true"
			shift
			;;
		*)
			command+=( "$1" )
			shift
			;;
	esac
done

set -- "${command[@]}"

#command="$@"

input="$@"

command="$input"
[ -z $mode ] && mode="osu"

if [[ $command = "" || $command = " " ]]; then
	IFS=$'\n'
	REDIS=( $(redis-cli get "${uuid}:${mode}") ) || exit 1
	ENTRY="${REDIS[0]}"
	alias="${REDIS[*]:1}"
	[[ $alias = $ENTRY ]] && alias=""

	if [[ -z $ENTRY ]]; then
		if [[ $RIPPLE = "true" ]]; then
			echo $'You haven\'t set a username yet! Set one with `~osuset --ripple [username]`!'
			exit 
		else
			echo $'You haven\'t set a username yet! Set one with `~osuset [username]`!'
			exit 
		fi
	fi
	username="$ENTRY"
else
	username="$command"
fi

URLname=$(urlencode "$username")
CURL_args="u=$URLname&m=$gamemode"
[[ $DETAILS = "true" ]] && CURL_args="u=$URLname&m=$gamemode&event_days=31"

if [[ $RIPPLE = "true" ]]; then
	[ -z $MODE ] && export MODE="ripple"
	CURL=$(curl -s "ripple.moe/api/get_user?${CURL_args}" | sh JSON.sh -b) || { echo "Error! API fetching failed!" ; exit 1; }
else
	[ -z $MODE ] && export MODE="osu"
	CURL=$(curl -s "osu.ppy.sh/api/get_user?k=$API&${CURL_args}" | sh JSON.sh -b) || { echo "Error! API fetching failed!" ; exit 1; }
fi

{
	if [[ $CURL = "[]" ]]; then
		echo Invalid username! 
		exit
	fi

	if [[ $(echo "$CURL" | grep -F '[') = "" ]]; then
		exit 1
	fi
}

# {
#	echo "$CURL" # > tmp/lookup
#}

export CURLusername=$(json-parse "$CURL" 		'[0,"username"]'		)
export CURLuser_id=$(json-parse "$CURL"			'[0,"user_id"]'			)
export CURLcountry=$(json-parse "$CURL"			'[0,"country"]'			)
export CURLpp_rank=$(json-parse "$CURL"			'[0,"pp_rank"]'			)
export CURLpp_country_rank=$(json-parse "$CURL" '[0,"pp_country_rank"]'	)
export CURLplaycount=$(json-parse "$CURL" 		'[0,"playcount"]'		) # v Rounding up numbers, second decimal points
export CURLpp_raw=$(json-parse "$CURL" 			'[0,"pp_raw"]'   | round 2 `cat`)
export CURLaccuracy=$(json-parse "$CURL" 		'[0,"accuracy"]' | round 2 `cat`)
export CURLlevel=$(json-parse "$CURL"			'[0,"level"]'    | round 2 `cat`)
export CURLcount_rank_ssh=$(json-parse "$CURL"	'[0,"count_rank_ssh"]'	)
export CURLcount_rank_sh=$(json-parse "$CURL"	'[0,"count_rank_sh"]'	)
export CURLcount_rank_ss=$(json-parse "$CURL"	'[0,"count_rank_ss"]'	)
export CURLcount_rank_s=$(json-parse "$CURL"	'[0,"count_rank_s"]'	)
export CURLcount_rank_a=$(json-parse "$CURL"	'[0,"count_rank_a"]'	)
export CURLtotal_score=$(json-parse "$CURL"		'[0,"total_score"]'		)
export CURLranked_score=$(json-parse "$CURL"	'[0,"ranked_score"]'	)
export CURLseconds_played=$(json-parse "$CURL"	'[0,"total_seconds_played"]')

[[ $uuid = "170132746042081280" ]] && :

nozero=("CURLcount_rank_ssh" "CURLcount_rank_sh" "CURLcount_rank_ss" "CURLcount_rank_s" "CURLcount_rank_a")
for nozerovar in "${nozero[@]}"; do
	[[ -z $(eval echo $(echo \$$nozerovar)) ]] && declare $(echo $nozerovar)=0
done

if [[ $DETAILS = "true" ]]; then
	for entry in {0..9}; do
		CURLevent_epic[$entry]=$(json-parse "$CURL" '[0,"events",'$entry',"epicfactor"]')
		[[ -z ${CURLevent_epic[$entry]} ]] && break
		CURLevent_name[$entry]=$(json-parse "$CURL" '[0,"events",'${entry}',"display_html"]' | htmlparser)
		CURLevent_happened[$entry]=$(json-parse "$CURL" '[0,"events",'$entry',"date"]' | timeago)
		CURLevent_bmid[$entry]=$(json-parse "$CURL" '[0,"events",'$entry',"beatmap_id"]')
		CURLevent_bmsid[$entry]=$(json-parse "$CURL" '[0,"events",'$entry',"beatmapset_id"]')
		if [[ ${CURLevent_bmid[$entry]} != 0 ]]; then CURLevent_url[$entry]="https://osu.ppy.sh/b/${CURLevent_bmid[$entry]}"
		else CURLevent_url[$entry]="https://osu.ppy.sh/s/${CURLevent_bmsid[$entry]}"
		fi
	done
fi

if [[ "$CURLpp_rank" = "null" ]]; then
	exit 1
fi

IFS=$'\n'
[[ $RIPPLE != true ]] && FLUCTUATION=( $(redis-cli --raw get "fluc:${uuid}:${CURLuser_id}:${MODE}") )
if [[ ${#FLUCTUATION} -gt 0 ]]; then
	for each in "${FLUCTUATION[@]}"; do
		eval "$each"
	done
else
	FLUCpp_rank=${CURLpp_rank}
	FLUCpp_country_rank=${CURLpp_country_rank}
	FLUCplaycount=${CURLplaycount}
	FLUCpp_raw=${CURLpp_raw}
	FLUCtotal_score=${CURLtotal_score}
	FLUCranked_score=${CURLranked_score}
	FLUCcount_rank_ssh=${CURLcount_rank_ssh}
	FLUCcount_rank_sh=${CURLcount_rank_sh}
	FLUCcount_rank_ss=${CURLcount_rank_ss}
	FLUCcount_rank_s=${CURLcount_rank_s}
	FLUCcount_rank_a=${CURLcount_rank_a}
	FLUClevel=${CURLlevel}
	FLUCaccuracy=${CURLaccuracy}
fi

changes=0

fluctuation=$( bc -l <<< "$CURLpp_raw - $FLUCpp_raw" )
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		pp_raw="**${CURLpp_raw}pp**";;
	'-'*)		pp_raw="**${CURLpp_raw}pp** ($fluctuation)";;
	*)		pp_raw="**${CURLpp_raw}pp** (+$fluctuation)";;
esac

fluctuation=$( bc -l <<< "$CURLpp_rank - $FLUCpp_rank" )
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		pp_rank="**#${CURLpp_rank}**";;
	'-'*)		fluctuation=${fluctuation#-}
			pp_rank="**#${CURLpp_rank}** (+$fluctuation)";;
	*)		pp_rank="**#${CURLpp_rank}** (-$fluctuation)";;
esac

fluctuation=$(( CURLpp_country_rank - FLUCpp_country_rank ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		pp_country_rank="**#${CURLpp_country_rank}**";;
	'-'*)		fluctuation=${fluctuation#-}
			pp_country_rank="**#${CURLpp_country_rank}** (+$fluctuation)";;
	*)		pp_country_rank="**#${CURLpp_country_rank}** (-$fluctuation)";;
esac

fluctuation=$(( CURLplaycount - FLUCplaycount ))
#echo 'fl'"$fluctuation"'+cu'"$CURLplaycount"'+fl'"$FLUCplaycount" &> tmp/fl
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		playcount="**$(printf "%s" ${CURLplaycount} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')**";;
	*)		playcount="**$(printf "%s" ${CURLplaycount} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')** (+$(printf "%s" $fluctuation | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'))";;
esac

fluctuation=$(( CURLtotal_score - FLUCtotal_score ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		total_score="**$(printf "%s" ${CURLtotal_score} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')**";;
	-*)		total_score="**$(printf "%s" ${CURLtotal_score} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')** ($(printf "%s" $fluctuation | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'))";;
	*)		total_score="**$(printf "%s" ${CURLtotal_score} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')** (+$(printf "%s" $fluctuation | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'))";;
esac

fluctuation=$(( CURLranked_score - FLUCranked_score ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		ranked_score="**$(printf "%s" ${CURLranked_score} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')**";;
	-*)		ranked_score="**$(printf "%s" ${CURLranked_score} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')** ($(printf "%s" $fluctuation | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'))";;
	*)		ranked_score="**$(printf "%s" ${CURLranked_score} | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')** (+$(printf "%s" $fluctuation | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'))";;
esac

fluctuation=$(( CURLcount_rank_ssh - FLUCcount_rank_ssh ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		count_rank_ssh="**${CURLcount_rank_ssh}**";;
	-*)		count_rank_ssh="**${CURLcount_rank_ssh}** ($fluctuation)";;
	*)		count_rank_ssh="**${CURLcount_rank_ssh}** (+$fluctuation)";;
esac

fluctuation=$(( CURLcount_rank_sh - FLUCcount_rank_sh ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		count_rank_sh="**${CURLcount_rank_sh}**";;
	-*)		count_rank_sh="**${CURLcount_rank_sh}** ($fluctuation)";;
	*)		count_rank_sh="**${CURLcount_rank_sh}** (+$fluctuation)";;
esac

fluctuation=$(( CURLcount_rank_ss - FLUCcount_rank_ss ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		count_rank_ss="**${CURLcount_rank_ss}**";;
	-*)		count_rank_ss="**${CURLcount_rank_ss}** ($fluctuation)";;
	*)		count_rank_ss="**${CURLcount_rank_ss}** (+$fluctuation)";;
esac

fluctuation=$(( CURLcount_rank_s - FLUCcount_rank_s ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		count_rank_s="**${CURLcount_rank_s}**";;
	-*)		count_rank_s="**${CURLcount_rank_s}** ($fluctuation)";;
	*)		count_rank_s="**${CURLcount_rank_s}** (+$fluctuation)";;
esac

fluctuation=$(( CURLcount_rank_a - FLUCcount_rank_a ))
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		count_rank_a="**${CURLcount_rank_a}**";;
	-*)		count_rank_a="**${CURLcount_rank_a}** ($fluctuation)";;
	*)		count_rank_a="**${CURLcount_rank_a}** (+$fluctuation)";;
esac

fluctuation=$( bc -l <<< "$CURLlevel - $FLUClevel" )
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		level="${CURLlevel}";;
	-*)		level="${CURLlevel} ($fluctuation)";;
	*)		level="${CURLlevel} (+$fluctuation)";;
esac

fluctuation=$( bc -l <<< "$CURLaccuracy - $FLUCaccuracy" )
[[ "$fluctuation" != 0 ]] && let "changes++"
case $fluctuation in  
	0)		accuracy="**${CURLaccuracy}%**";;
	-*)		accuracy="**${CURLaccuracy}%** ($fluctuation)";;
	*)		accuracy="**${CURLaccuracy}%** (+$fluctuation)";;
esac

[[ ! -z "$alias" ]] && username="${CURLusername} (${alias})" || username="$CURLusername"
echo "$gametype"' / '"$username"
if [[ $RIPPLE != true ]]; then
	echo 'https://osu.ppy.sh/u/'"$CURLuser_id"
	echo 'https://osu.ppy.sh/images/flags/'"$CURLcountry"'.png'
	echo 'https://a.ppy.sh/'"$CURLuser_id"'?query='"$((RANDOM%100))"
else
	echo 'https://ripple.moe/u/'"$CURLuser_id"
	echo 'https://osu.ppy.sh/images/flags/'"$CURLcountry"'.png'
	echo 'https://a.ripple.moe/'"$CURLuser_id"'?query='"$((RANDOM%100))"
fi

echo -n "Playtime: $(displaytime "$CURLseconds_played")"
if [[ ! -z ${FLUCtimestamp} ]] && [[ $changes = 0 ]]; then
	echo ' / Last updated: '"$(date -d"@${FLUCtimestamp}" +"%A, %B %d, %Y, %I:%M %p")"
else
	echo ' / Last updated: now '
fi

body=()
body+=( 'Ranks: worldwide '"${pp_rank}"' / '"$CURLcountry"' '"${pp_country_rank}" )
body+=( "**Lv.** ${level} / ""${pp_raw}"' / Accuracy: '"${accuracy}" )
body+=( "${count_rank_ssh}"' SS+ / '"${count_rank_sh}"' S+ / '"${count_rank_ss}"' SS / '"${count_rank_s}"' S / '"${count_rank_a}"' A' )
body+=( 'Total score: '"${total_score}" )
body+=( 'Ranked score: '"${ranked_score}" )
body+=( 'Plays: '"${playcount}" )

if [[ ${#FLUCTUATION} -eq 0 ]]; then
	osu/lookup_flucstore.sh </dev/null &>tmp/cache &
fi

if [[ $RIPPLE != true ]]; then
	if [[ $changes -gt 0 ]]; then
		if [[ $input = "" ]]; then
			osu/lookup_flucstore.sh </dev/null &>tmp/cache &
			redis-cli set "fluc2:${uuid}:${CURLuser_id}:${MODE}" "$(printf '%s\n' "${body[@]}")" &> /dev/null
			redis-cli expire "fluc2:${uuid}:${CURLuser_id}:${MODE}" "2629746" &> /dev/null
			printf '%s\n' "${body[@]}"
		else
			printf '%s\n' "${body[@]}"
			echo $'*Not writing to fluctuation logs, since the user isn\'t mentioned*'
		fi
	else
		GET=$(redis-cli --raw get "fluc2:${uuid}:${CURLuser_id}:${MODE}")

		if [[ -z "$GET" ]] || [[ ${#FLUCTUATION} -eq 0 ]]; then
			redis-cli set "fluc2:${uuid}:${CURLuser_id}:${MODE}" "$(printf '%s\n' "${body[@]}")" &> /dev/null
			redis-cli expire "fluc2:${uuid}:${CURLuser_id}:${MODE}" "2629746" &> /dev/null
			printf '%s\n' "${body[@]}"
		else
			echo "${GET}"
		fi
	fi
else
	body+=( $'*Not writing to fluctuation logs, as it\'s Ripple*' )
	printf '%s\n' "${body[@]}"
fi

#echo ${CURLpp_raw} / ${pp_raw} / $( bc -l <<< "$CURLpp_raw - $FLUCpp_raw" )
#echo ${FLUCTUATION[@]}

[[ $DETAILS != "true" ]] && exit

[[ -z "${CURLevent_name[*]}" ]] && echo $'\n*No recent activities*' && exit 0

echo $'//MOST_RECENT//'
for entry in {0..9}; do
	entry1=$(( entry  + 1 ))
	[[ -z ${CURLevent_name[$entry]} ]] && break
	echo "***${entry1}/** ${CURLevent_happened[$entry]}*"
	echo $"[${CURLevent_name[$entry]}](${CURLevent_url[$entry]}) ^${CURLevent_epic[$entry]}"
done
