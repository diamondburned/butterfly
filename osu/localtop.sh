#!/usr/bin/env bash
round() {
	echo $(printf %.$1f $(echo "scale=$1;(((10^$1)*$2)+0.5)/(10^$1)" | bc))
};

htmlparser() {
	read -r v
	printf "$v" | sed -e 's/>/>\n/g' -e 's/</\n</g' | grep -v '<' | tr -d '\n'
}

displaytime() {
	local T=$1
	local D=$((T/60/60/24))
	local H=$((T/60/60%24))
	local M=$((T/60%60))
	local S=$((T%60))
	(( $D > 0 )) && printf '%d days ' $D
	(( $H > 0 )) && printf '%d hours ' $H
	(( $M > 0 )) && printf '%d minutes ' $M
	(( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
	printf '%d seconds\n' $S
};

timeago() {
	read -r CURLdate
	local YEAR=$(echo "$CURLdate" | awk '{print $1}' | awk -F '-' '{print $1'})
	local MONTH=$(echo "$CURLdate" | awk '{print $1}' | awk -F '-' '{print $2'})
	local DAY=$(echo "$CURLdate" | awk '{print $1}' | awk -F '-' '{print $3'})
	local TIME=$(echo "$CURLdate" | awk '{print $2}')
	local FULLTIME="${MONTH}/${DAY}/${YEAR} ${TIME}"
	local TIMEepoch=$(command date -d"${FULLTIME}" -u +%s)
	local NOWepoch=$(TZ=Australia/Perth date +%s)
	if [[ $RIPPLE = "true" ]]; then
		local DIFF=$(( $NOWepoch - ( $TIMEepoch - 28800 + 21600) ))
	else
		local DIFF=$(( $NOWepoch - ( $TIMEepoch - 28800 ) ))
	fi
	echo "$(displaytime ${DIFF}) ago"
}

if [[ $1 = "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
elif [[ $1 = "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
elif [[ $1 = "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi

opts="$2"

. config
. bin/funcs.bash

josn-parse() {
	JSONsh="$1"
	KEY="${@:2}"
	LINE=$(grep -F "$KEY" <<< "$JSONsh")
	STRING="${LINE#*	}"
	STRING="${STRING/#\"/}"
	STRING="${STRING//\\/}"
	printf "${STRING/%\"/}"
}

declare -A users

for a in "${@:3}"; {
	# users[snowflake]=username
	users["${a%%:*}"]="${a#*:}"
}

# IFS="," read -d '' -ra snowflakes <<< "$3"
# IFS="," read -d '' -ra users <<< "${@:4}"

# echo "${@:4}" > /tmp/asd

if [[ "${#users[@]}" -eq 0 ]]; then
	echo "No players found"
	exit 0
fi

[ -z $mode ] && mode="osu"

usernames=()
aliases=()
discordname=()

for snowflake in "${!users[@]}"; do
	IFS=$'\n' REDIS=( $(redis-cli get "${snowflake}:${mode}") ) || exit 1
	ENTRY="${REDIS[0]}"
	alias="${REDIS[@]:1}"
	[[ $alias = $ENTRY ]] && alias=""

	if [[ $ENTRY ]]; then
		usernames+=( "$ENTRY" )
		aliases+=( "$alias" )
		discordname+=( "${users["$snowflake"]}" )
	fi
done

CURLusernames=()
CURLcountries=()
CURLpp_ranks=()
CURLpp_raws=()
CURLaccuracies=()

for username in "${usernames[@]}"; do
	URLname=$(urlencode "$username")
	CURL=$(curl -s "osu.ppy.sh/api/get_user?k=$API&u=$URLname&m=$gamemode" | sh JSON.sh -b) || {
		echo "Error! API fetching failed!"
		exit 1
	}

	CURLusernames+=(	"$(json-parse "$CURL" '[0,"username"]'	)" )
	CURLcountries+=(	"$(json-parse "$CURL" '[0,"country"]'	)" )
	CURLpp_ranks+=(		"$(json-parse "$CURL" '[0,"pp_rank"]'	)" )
	CURLpp_raws+=(		"$(json-parse "$CURL" '[0,"pp_raw"]'    )" )
	CURLaccuracies+=(	"$(json-parse "$CURL" '[0,"accuracy"]'  )" )
done

output=( "0 Username | Global Rank | pp | Accuracy (%)" )

for (( i=0; i<${#CURLusernames[@]}; i++)); do
	printf -v entry "%s [%s] %s (%s) | #%s | %.2fpp | %.2f%%" \
		"${CURLpp_ranks[$i]}" \
		"${CURLcountries[$i]}" \
		"${discordname[$i]:0:12}" \
		"${CURLusernames[$i]}" \
		"${CURLpp_ranks[$i]}" \
		"${CURLpp_raws[$i]}" \
		"${CURLaccuracies[$i]}"

	output+=( "$entry" )
done

#printf "%s\n" ${output[@]} | sort -n -t'' | cut -d'' -f2- 
printf "%s\n" "${output[@]}" | sort -n -t'' | cut -d'' -f2- | column -t -s''
