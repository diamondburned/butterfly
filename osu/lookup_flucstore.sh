#!/usr/bin/env bash
TIMESTAMP=$(date +%s)
FLUCTUATION="
FLUCpp_rank=${CURLpp_rank}
FLUCpp_country_rank=${CURLpp_country_rank}
FLUCplaycount=${CURLplaycount}
FLUCpp_raw=${CURLpp_raw}
FLUCtotal_score=${CURLtotal_score}
FLUCranked_score=${CURLranked_score}
FLUCcount_rank_ssh=${CURLcount_rank_ssh}
FLUCcount_rank_sh=${CURLcount_rank_sh}
FLUCcount_rank_ss=${CURLcount_rank_ss}
FLUCcount_rank_s=${CURLcount_rank_s}
FLUCcount_rank_a=${CURLcount_rank_a}
FLUClevel=${CURLlevel}
FLUCaccuracy=${CURLaccuracy}
FLUCtimestamp=${TIMESTAMP}
"

#echo "fluc_${uuid}:${MODE}"
redis-cli set "fluc:${uuid}:${CURLuser_id}:${MODE}" "$FLUCTUATION"
redis-cli expire "fluc:${uuid}:${CURLuser_id}:${MODE}" "2629746"

echo "$FLUCTUATION" >> /tmp/fluc

#	pp_rank
#	pp_country_rank
#	pp_raw
#	total_score 
#	ranked_score
#	count_rank_ssh
#	count_rank_sh
#	count_rank_ss
#	count_rank_s
#	count_rank_a
#	level
#	accuracy
