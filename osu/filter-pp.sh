#!/usr/bin/env bash
. config
source bin/funcs.bash

args=( $@ )
set -- "${args[@]}"
string="$1"
shift
IFS=$'\n'
#set -x

gamemode="${string#*?m=}"
if [[ $gamemode = "3" ]]; then
	gametype="osu! Mania"
elif [[ $gamemode = "1" ]]; then
	gametype="osu! Taiko"
elif [[ $gamemode = "2" ]]; then
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi

if [[ $string = *".ppy.sh/b/"* || $string = *".ppy.sh/beatmaps/"* ]] || [[ $string = *"#"* && $string = "https://osu.ppy.sh/beatmapsets/"* ]]; then
	id=$(echo "${string##*/}" | sed 's/?.*//')
	gamemode=$(osugm "$(echo "${string##*\#}" | cut -d/ -f1)")
	[[ -z "$gamemode" ]] && gamemode=0
	bmid=($id)
elif [[ $string = *".ppy.sh/s/"* ]]; then
	bmsid=$(echo $string | cut -d'/' -f5 ) 
	FILE=$(curl -s "osu.ppy.sh/api/get_beatmaps?k=$API&s=$bmsid&m=${gamemode}" | sh JSON.sh -b) || { echo "Unknown link"; exit 1; }
	bmid=( $(jsonparse "$FILE" '"beatmap_id"]	') )
elif [[ $string = *".ppy.sh/beatmapsets"* ]]; then
	bmsid=$(echo $string | cut -d'/' -f5 | sed 's~#osu~~g')
	FILE=$(curl -s "osu.ppy.sh/api/get_beatmaps?k=$API&s=$bmsid&m=${gamemode}" | sh JSON.sh -b) || { echo "Unknown link"; exit 1; }
	bmid=( $(jsonparse "$FILE" '"beatmap_id"]	') )
else
	echo Unknown link
	exit
fi

echo "$FILE" > tmp/filter

[[ $FILE = "[]" ]] && { echo "Data is not available, please try again later."; exit; } 

if [[ -z ${bmid[0]} ]]; then
	echo Unknown shit
	exit
else
	RESULTS=()
	OSUCURLarr=()

	for i in "${bmid[@]}"; do
		curl=$(fetchBeatmap "$i") || {
			echo "osu! is downed (or at least the bot can't do it right...)"
			exit 0
		}

		OSUCURLarr+=( "$curl" ) 
	done

	#echo "${OSUCURLarr[0]}" | grep -m 1 -F $'[Metadata]' -A 10

	general="$(echo "${OSUCURLarr[0]}" | grep -m 1 -F $'[General]' -A 10)"
	gamemode="$(parseBeatmapKey "$general" "Mode: ")"	

	IFS=$'\n'
	metadata="$(echo "${OSUCURLarr[0]}" | grep -m 1 -F $'[Metadata]' -A 10)"
	title="$(parseBeatmapKey "$metadata" "Title:")"	
	artist="$(parseBeatmapKey "$metadata" "Artist:")"
	creator="$(parseBeatmapKey "$metadata" "Creator:")"
	#echo "${OSUCURLarr[0]}"
	#tags="$(parseBeatmapKey "$metadata" "Tags:")"

	if [[ -z $(parseBeatmapKey "$metadata" "BeatmapID:") || -z "$gamemode" ]]; then
		FILE=$(curl -s "osu.ppy.sh/api/get_beatmaps?k=$API&b=${bmid[0]}&m=${gamemode}" | sh JSON.sh -b) || { echo "Unknown link"; exit 1; }
		bmsid=( $(jsonparse "$FILE" '"beatmapset_id"]	') )
		gamemode=$(jsonparse "$FILE" ',"mode"]'	)
		gametype=$(osugm "$gamemode")
	else
		bmsid=$(parseBeatmapKey "$metadata" "BeatmapSetID:")
	fi

	bpm=$(lastline "$(grep -m 1 -F $'[TimingPoints]' -A 1 <<< "${OSUCURLarr[0]/$'\r'}")" | fakecut ',' 2 | tr -dc '0-9.')
	BPM=$(echo '60000 / '${bpm} | bc -l | roundpipe 1)

	IFS=$'\n\t'

	[[ "${FILE}" ]] && {
		approved=$(jsonparse "$FILE" '[0,"approved"]')
	}

	case "$approved" in
		-2)
			STATUS="Status: Graveyard";;
		-1)
			STATUS="Status: Work in Progress";;
		0)
			STATUS="Status: Pending";;
		1)
			STATUS="Status: Ranked";;
		2)
			STATUS="Status: Approved";;
		3)
			STATUS="Status: Qualified";;
		4)
			STATUS="Status: Loved";;
		*)
			STATUS="Use /s/ links for ranking status";;
	esac

	POSITIONAL=()
	while [[ $# -gt 0 ]]; do
		case "$1" in
			'+'*)		
				mod="${1^^}"
				shift # past argument
			;;
			*'x')		
				combo="$1"
				if [[ $(echo ${combo:0:-1} | bc -l) != "${combo:0:-1}" ]]; then
					echo 'Your x (combo) argument is wrong!'
					exit 1
				fi
				shift
			;;
			*'%')		
				accuracy="$1"
				if [[ $(echo ${accuracy:0:-1} | bc -l) != "${accuracy:0:-1}" ]]; then
					echo 'Your % (accuracy) argument is wrong!'
					exit 1
				fi
				shift
			;;
			*'xmiss')	
				miss="${1}"
				if [[ $(echo ${miss/xmiss} | bc -l) != "${miss/xmiss}" ]]; then
					echo 'Your xmiss (miss count) argument is wrong!'
					exit 1
				fi
				shift
			;;
			'--objnerf')
				calcnerf=true
				shift
			;;
			'--angle')
				anglerework=true
				shift
			;;
			'--mode')
				case $2 in
					mania)
						gamemode="3";;
					taiko)
						gamemode="1";;
					ctb)
						echo "Feature not supported yet." && exit 0;;
					*)
						gamemode="0";;
				esac
				gametype="$(osugm $gamemode) (might not be accurate)"
				shift
				shift
			;;
			*)	POSITIONAL+=("$1") # save it in an array for later
				shift # past argument
			;;
		esac
	done

	# Hack to get total_length
		FirstBeatmap="${OSUCURLarr[0]}"

		Line=$(( $(echo "$FirstBeatmap" | grep -n -F '[HitObjects]' | cut -d':' -f1) + 1 ))
		HitObjects=$(echo "$FirstBeatmap" | tail -n +${Line} | sed '/^\s*$/d')
		TotalObjects=$(echo "$FirstBeatmap" | tail -n +${Line} | sed '/^\s*$/d' | wc -l)

		LastLine=$(echo "$HitObjects" | head "-${TotalObjects}" | tail -1)
		TotalTime=$(echo "$LastLine" | cut -d',' -f3)

		# No I'm not sorry
		[[ $mod = *"DT"* ]] || [[ $mod = *"NC"* ]] && { # if DT/NC
			TotalInTime=$(round 0 "$(bc -l <<< "$TotalTime / 1000 / 1.5")" )
			BPM="$(bc -l <<< "$BPM * 1.5" | roundpipe 1)"
		} || { # if not
			TotalInTime=$(( TotalTime / 1000 ))
		}

		Length=$(convertsecs "${TotalInTime}")

	echo $artist \- $title \[$creator\] \/ ${Length} \/ ${BPM} BPM ${mod}
	echo 'https://osu.ppy.sh/s/'${bmsid}
	echo 'https://b.ppy.sh/thumb/'"${bmsid}"'l.jpg'
	echo "$gametype / $STATUS"
	
	e=0
	until [[ $e -eq "${#OSUCURLarr[@]}" ]]; do
		if [[ "${OSUCURLarr[$e]}" ]]; then
			if [[ -z $FILE ]]; then
				bmid=$(parseBeatmapKey "$metadata" "BeatmapID:")
			else
				bmid=$(jsonparse "$FILE" "[${e},\"beatmap_id\"]")
			fi

			IFS=$'\n'
			metadata=$(echo "${OSUCURLarr[$e]}" | grep -m 1 -F $'[Metadata]' -A 10)
			version="$(parseBeatmapKey "$metadata" "Version:")"

			OUTPUT=$(echo "${OSUCURLarr[$e]}" | ./osu/oppai - $mod $accuracy $combo $miss -m${gamemode} -ojson | sh JSON.sh -b)
			[[ $calcnerf = true ]] && [[ $gamemode = 0 ]] && {
				OUTPUTnerf=$(echo "${OSUCURLarr[$e]}" | node osu/ojsama/index.js $mod $accuracy $combo $miss)
				PPnerf=$(jsonparse "$(printf "$(lastline "$OUTPUTnerf")" | sh JSON.sh -b)" '["pp"]	' | roundpipe 2)
			}

			[[ $anglerework = true ]] && {
				arg=""
				[[ "$combo"    ]] && arg+="-c ${combo} "
				[[ "$miss"     ]] && arg+="-nmiss ${miss} "
				[[ "$mod"      ]] && arg+="-m ${mod} "
				[[ "$accuracy" ]] && arg+="-a ${accuracy} "
				rework=$(echo "${OSUCURLarr[$e]}" | osu/goppai-rework $arg) && {
					readarray data <<< "$rework"
					# line 1 pp; line 2 accuracy; line 3 mod string
					reworkStr=" / ~~∠R ${data[0]//$'\n'} @${data[1]//$'\n'}~~"
				}
			}

			if [[ "$OUTPUT" = *"requested a feature that isn't implemented" ]]; then
				RESULTS+=( "[${version}](https://osu.ppy.sh/b/${bmid[$e]})" )
			else
				OPTIONS="**/** "

				O_mods="$(jsonparse "$OUTPUT" '["mods_str"]	')"
					[[ $O_mods ]] && OPTIONS+="+${O_mods} "

				O_accuracy="$accuracy"
					[[ $O_accuracy != "100%" ]] && OPTIONS+="${O_accuracy} "

				O_combo="$(jsonparse "$OUTPUT" '["combo"]	')"; O_max_combo="$(jsonparse "$OUTPUT" '["max_combo"]	')"
					[[ "$O_combo" -ne "$O_max_combo" ]] && OPTIONS+="${O_combo}/${O_max_combo}x " || OPTIONS+="${O_max_combo}x FC"

				O_misses="$(jsonparse "$OUTPUT" '["misses"]	')"
					[[ $O_misses -ne 0 ]] && OPTIONS+="${O_misses}xmiss "
				
				ARODCSHP=""
					ARODCSHP+="AR$(jsonparse "$OUTPUT" '["ar"]	' | roundpipe 2) "
					ARODCSHP+="OD$(jsonparse "$OUTPUT" '["od"]	' | roundpipe 2) "
					ARODCSHP+="CS$(jsonparse "$OUTPUT" '["cs"]	' | roundpipe 2) "
					ARODCSHP+="HP$(jsonparse "$OUTPUT" '["hp"]	' | roundpipe 2) "

				STARS=$(jsonparse "$OUTPUT" '["stars"]	' | roundpipe 2)

				PP="$(jsonparse "$OUTPUT" '["pp"]	' | roundpipe 2)pp"

				[[ $PPnerf ]] && { # not redundant, it's to keep spaces equal
					PP+=" $(ppnerfcheck "$PPnerf" "$PP") "
				}

				PP+="$reworkStr"

				PRINT='['${version}']'"(https://osu.ppy.sh/b/${bmid[$e]}) **/** ${STARS}★ \\\n""${PP}"' **/** '"$ARODCSHP$OPTIONS"
				RESULTS+=( "$STARS$PRINT" )
			fi
		fi
		((e++))
	done
	IFS=$'\n'
	OUTPUT=$(printf '%s\n' "${RESULTS[@]}" | sort -n -t\ | cut -f2- -d'' | sed 's~\\\\n~\n~g')
	echo -n "$OUTPUT"

	#echo https://b.ppy.sh/preview/${bmsid}.mp3
	#echo 'Beatmapset ID: '$bmsid
fi
