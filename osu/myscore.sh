#!/usr/bin/env bash
bitmod() {
	local modvalue="$1"
	local e=0
	local bits=(0 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 16777216 33554432 67108864 134217728 268435456)
	local mods=("" "NF" "EZ" "" "HD" "HR" "SD" "DT" "RX" "HT" "NC" "FL" "Auto" "SO" "AP" "Perfect" "Key4" "Key5" "Key6" "Key7" "Key8" "FadeIn" "Random" "LastMod" "Key9" "Key10" "Key1" "Key3" "Key2")
	local OUTPUT=()
	if [[ $modvalue -ne "0" ]]; then
		until [[ $e == ${#bits[@]} ]]; do
			local BITINCLUDE=$(($modvalue & ${bits[$e]}))
			if [[ $BITINCLUDE -ne 0 ]]; then
				OUTPUT+=("${mods[$e]}")
			fi
			((e++))
		done
		printf '%s' "${OUTPUT[@]}"
	else
		echo -n ""
	fi
}

displaytime() {
	local T=$1
	local D=$((T / 60 / 60 / 24))
	local H=$((T / 60 / 60 % 24))
	local M=$((T / 60 % 60))
	local S=$((T % 60))
	(($D > 0)) && printf '%d days ' $D
	(($H > 0)) && printf '%d hours ' $H
	(($M > 0)) && printf '%d minutes ' $M
	(($D > 0 || $H > 0 || $M > 0)) && printf 'and '
	printf '%d seconds\n' $S
}

convertsecs() {
	secs=$1
	TIMESTAMP=$(printf '%02d:%02d:%02d\n' $(($secs / 3600)) $(($secs % 3600 / 60)) $(($secs % 60)))
	if [[ $TIMESTAMP == "00:"* ]]; then
		echo "$TIMESTAMP" | cut -c 4-
	else
		echo "$TIMESTAMP"
	fi
}

urlencode() {
	local length="${#1}"
	for ((i = 0; i < length; i++)); do
		local c="${1:i:1}"
		case $c in
		[a-zA-Z0-9.~_-]) printf "$c" ;;
		*) printf "$c" | xxd -p -c1 | while read x; do printf "%%%s" "$x"; done ;;
		esac
	done
}

time_to_ago() {
	epoch=$(command date -d"${*}" -u +%s)
	now_epoch=$(printf "%(%s)T")

	if [[ $RIPPLE == "true" ]]; then
		local difference=$(($now_epoch - ($epoch - 28800 + 21600)))
	else
		local difference=$(($now_epoch - ($epoch)))
	fi

	local TIMEAGO=$(displaytime ${difference})
	echo "$TIMEAGO ago"
}

. config
source bin/funcs.bash

if [[ $1 == "mania" ]]; then
	gamemode="3"
	gametype="osu! Mania"
elif [[ $1 == "taiko" ]]; then
	gamemode="1"
	gametype="osu! Taiko"
elif [[ $1 == "ctb" ]]; then
	gamemode="2"
	gametype="osu! Catch the Beat"
else
	gamemode="0"
	gametype="osu!"
fi
uuid=$2

# READ ME: if the cmd is ./myscore.sh [osu] [snowflake] [args] then it parses the snowflake
set -- "${@:3}"

LIMIT=1

STRING=()
until [[ $# == 0 ]]; do
	key="$1"
	case $key in
	--ripple | -r)
		RIPPLE="true"
		mode="ripple"
		shift
		;;
	--list | -l)
		LIMIT=500
		shift
		;;
	--global | -g)
		global=true
		shift
		;;
	--objnerf)
		calcnerf=true
		shift
		;;
	--angle)
		anglerework=true
		shift
		;;
	*)
		STRING+=("$1")
		shift
		;;
	esac
done

set -- "${STRING[@]}"

[ -z $mode ] && mode="osu"

string="$1"

if [[ $string == *".ppy.sh/b/"* || $string == *".ppy.sh/beatmaps/"* ]] || [[ $string == *"#"* && $string == "https://osu.ppy.sh/beatmapsets/"* ]]; then
	id=$(echo "${string##*/}" | sed 's/?.*//')
else
	echo Unknown link
	exit
fi

command="${@:2}"

if [[ $command == "" ]]; then
	IFS=$'\n'
	REDIS=($(redis-cli get "${uuid}:${mode}")) || exit 1
	ENTRY="${REDIS[0]}"
	alias="${REDIS[*]:1}"
	[[ $alias == $ENTRY ]] && alias=""

	if [[ -z $ENTRY ]]; then
		if [[ $RIPPLE == "true" ]]; then
			echo $'You haven\'t set a username yet! Set one with `~osuset --ripple [username]`!'
			exit
		else
			echo $'You haven\'t set a username yet! Set one with `~osuset [username]`!'
			exit
		fi
	fi
	username="$ENTRY"
else
	username="$command"
fi

input="$1"
DATE=$(printf "%(%s)T")
IFS=$'\n\t'

[[ $global == true ]] && username=""

case $RIPPLE in
"true")
	echo "RIPPLE is WIP rn, bye!"
	exit 1
	#URLname=$(urlencode "$username")
	#CURL=$(curl -s "ripple.moe/api/get_user_recent?u=$URLname&m=$gamemode&limit=1" | sh JSON.sh -b) || { echo "Error! API fetching failed!" ; exit 1; }
	;;
*)
	URLname=$(urlencode "$username")
	CURL=$(curl -s "https://osu.ppy.sh/api/get_scores?k=${API}&m=${gamemode}&b=${id}&u=${URLname}&limit=${LIMIT}" | sh JSON.sh -b) || {
		echo "Error! API fetching failed!"
		exit 1
	}
	;;
esac

[[ -z "$CURL" ]] && echo "No plays found, user most likely have not passed the map yet..." && exit 0

if [[ $CURL == "[]" ]]; then
	echo Invalid username!
	exit 0
fi

if [[ $(echo "$CURL" | grep -F '[') == "" ]]; then
	echo "No plays found, user most likely have not passed the map yet..."
	exit 0
fi

CURL_bmdata=$(fetchosu "https://osu.ppy.sh/osu/$id") || {
	echo "osu! is downed (or at least the bot can't do it right...)"
	exit 0
}

BMSID=$(echo "$CURL_bmdata" | grep -F 'BeatmapSetID' | cut -d':' -f2 | sed 's/[^0-9]*//g')

#set -xe

RESULTScount=$(echo "$CURL" | grep -F ',"score"]	' | wc -l)

ENTRY=0
until [[ $ENTRY == $(($RESULTScount)) ]]; do
	CURL_score=$(json-parse "$CURL" '['$ENTRY',"score"]' | thousandths)
	CURL_username=$(json-parse "$CURL" '['$ENTRY',"username"]')
	CURL_maxcombo=$(json-parse "$CURL" '['$ENTRY',"maxcombo"]')
	CURL_count50=$(json-parse "$CURL" '['$ENTRY',"count50"]')
	CURL_count100=$(json-parse "$CURL" '['$ENTRY',"count100"]')
	CURL_count300=$(json-parse "$CURL" '['$ENTRY',"count300"]')
	CURL_countmiss=$(json-parse "$CURL" '['$ENTRY',"countmiss"]')
	CURL_countkatu=$(json-parse "$CURL" '['$ENTRY',"countkatu"]')
	CURL_countgeki=$(json-parse "$CURL" '['$ENTRY',"countgeki"]')
	CURL_perfect=$(json-parse "$CURL" '['$ENTRY',"perfect"]')
	CURL_enabled_mods=$(json-parse "$CURL" '['$ENTRY',"enabled_mods"]')
	CURL_user_id=$(json-parse "$CURL" '['$ENTRY',"user_id"]')
	CURL_date=$(json-parse "$CURL" '['$ENTRY',"date"]')
	CURL_rank=$(json-parse "$CURL" '['$ENTRY',"rank"]')
	CURL_pp=$(json-parse "$CURL" '['$ENTRY',"pp"]')

	case $CURL_enabled_mods in
	0) MODS="" ;;
	*) MODS="+$(bitmod $CURL_enabled_mods)" ;;
	esac

	HAPPENED=$(time_to_ago "$CURL_date")

	CIRCLECOUNTS=$(circlecounts "${CURL_count300}" "${CURL_count100}" "${CURL_count50}" "${CURL_countmiss}")

	oppai_OUTPUT=$(echo "${CURL_bmdata}" | ./osu/oppai - ${MODS} ${CURL_maxcombo}x ${CURL_count50}x50 ${CURL_count100}x100 ${CURL_countmiss}xm -m${gamemode} -ojson | sh JSON.sh -b)

	#ACCURACY="$(jsonparse "$oppai_OUTPUT" '["accuracy"]	' | roundpipe 2)%"
	ACCURACY=$(bin/acc.zsh ${gamemode} ${CURL_count50} ${CURL_count100} ${CURL_count300} ${CURL_countmiss} ${CURL_countkatu} ${CURL_countgeki})

	oppai_ACCURACY=$ACCURACY
	oppaiFC_OUTPUT=$(echo "${CURL_bmdata}" | ./osu/oppai - ${MODS} ${CURL_count50}x50 ${CURL_count100}x100 -m${gamemode} -ojson | sh JSON.sh -b)

	# error handling
	oppai_err=$(json-parse "$oppai_OUTPUT" '["errstr"]	')
	if [[ $oppai_err != "no error" ]]; then echo 'Something went wrong with oppai... Do ~report to report this.'"$oppai_err" && exit; fi

	if [[ $mapinfo_parsed != "true" ]]; then
		oppai_title=$(json-parse "$oppai_OUTPUT" '["title"]')
		oppai_artist=$(json-parse "$oppai_OUTPUT" '["artist"]')
		oppai_mapper=$(json-parse "$oppai_OUTPUT" '["creator"]')
		oppai_diffname=$(json-parse "$oppai_OUTPUT" '["version"]')
		oppai_combomax=$(json-parse "$oppai_OUTPUT" '["max_combo"]')
		mapinfo_parsed="true"
	fi # efficiency, pal

	oppai_pp=$(json-parse "$oppai_OUTPUT" '["pp"]' | round 2 $(cat)) # FC pp
	oppaiFC_pp=$(json-parse "$oppaiFC_OUTPUT" '["pp"]' | round 2 $(cat))
	oppai_stars=$(json-parse "$oppai_OUTPUT" '["stars"]' | roundpipe 2) # stars
	oppai_mods=$(json-parse "$oppai_OUTPUT" '["mods_str"]')             # mods
	#oppai_combo=$(json-parse	"$oppai_OUTPUT"	'["combo"]') prob dont need, use $CURL_maxcombo

	case $oppai_mods in
	"") oppai_mods="" ;;
	*) if [[ $oppai_mods =~ "NC" && $oppai_mods =~ "DT" ]]; then
		oppai_mods="+$(echo $oppai_mods | sed 's~DT~~g') "
	else
		oppai_mods="+$oppai_mods "
	fi ;;
	esac

	[[ $calcnerf == true ]] && [[ $gamemode == 0 ]] && {
		OUTPUTnerf=$(echo "${CURL_bmdata}" | node osu/ojsama/index.js "$oppai_mods" "$oppai_ACCURACY" "${CURL_maxcombo}x" "${CURL_countmiss}xmiss")
		PPnerf=$(jsonparse "$(printf "$(lastline "$OUTPUTnerf")" | sh JSON.sh -b)" '["pp"]	' | roundpipe 2)
	}

	[[ $anglerework == true ]] && {
		rework=$(echo "${CURL_bmdata}" | osu/goppai-rework -c "${CURL_maxcombo}" -n50 "${CURL_count50}" -n100 "${CURL_count100}" -nmiss "${CURL_countmiss}" -m "$MODS") && {
			readarray data <<<"$rework"
			# line 1 pp; line 2 accuracy; line 3 mod string
			reworkStr=" / ~~∠R ${data[0]//$'\n'/} @${data[1]//$'\n'/}~~"
		}
	}

	if [[ $LIMIT == 1 ]]; then
		echo "$oppai_artist"' - '"$oppai_title"' ['"$oppai_diffname"'] '"$oppai_mods"'/ '"$oppai_stars"'★'
		echo 'https://osu.ppy.sh/b/'"$id"
		echo 'https://a.ppy.sh/'"${CURL_user_id}"'?query='"$RANDOM"
		echo 'https://b.ppy.sh/thumb/'"${BMSID}"'l.jpg'

		[[ $RIPPLE == "true" ]] && server="Ripple" || server="osu!"
		[[ ! -z "$alias" ]] && username="${CURL_username} (${alias})" || username="$CURL_username"
		echo "${username}"' / '"$HAPPENED on $server servers"

		#      v combo max          v beatmap combo
		if [[ $CURL_pp != "null" ]]; then
			if [[ "$oppai_combomax" != "$CURL_maxcombo" ]]; then
				PP='**'"$CURL_pp"'**pp'$'\n''~~'"$oppaiFC_pp"'pp FC @'"$oppai_ACCURACY"'~~'
			else
				if [[ $oppai_ACCURACY != "100"* ]]; then
					oppaiOUTPUT100=$(echo "$CURL_bmdata" | ./osu/oppai - $MODS -m${gamemode} -ojson | sh JSON.sh -b)
					OUTPUTpp100=$(jsonparse "$oppaiOUTPUT100" '["pp"]' | roundpipe 2)
					PP='**'"$CURL_pp"'**pp'$'\n''~~'"$OUTPUTpp100"'pp FC @100%~~'
				else
					PP='**'"$CURL_pp"'**pp'
				fi
			fi
		else
			PP="~~${oppai_pp}pp if ranked~~"
		fi

		[[ $PPnerf ]] && PP+=" $(ppnerfcheck "$PPnerf" "$PP")"

		echo "$(rank-to-emoji "$CURL_rank")"' ▷**'"$CURL_score"'** / '"$PP""$reworkStr"
		echo '**'$oppai_ACCURACY'** / **'"$CURL_maxcombo"'**/'"$oppai_combomax"'x / '$CIRCLECOUNTS
		break # end loop, showing only one
	else
		if [[ $TITLE_shown -ne 1 ]]; then
			echo "Showing $RESULTScount results"
			echo ''
			echo 'https://a.ppy.sh/'"${CURL_user_id}"'?query='"$RANDOM"
			echo 'https://b.ppy.sh/thumb/'"${BMSID}"'l.jpg'
			echo ''
			TITLE_shown=1
		fi

		if [[ $RIPPLE == "true" ]]; then
			echo "▷ $CURL_username"' / '"$HAPPENED on Ripple servers"
		else
			echo "▷ $CURL_username"' / '"$HAPPENED on osu! servers"
		fi

		echo "[$oppai_artist"' - '"$oppai_title"' ['"$oppai_diffname"']](https://osu.ppy.sh/b/'"$id"') '"$oppai_mods"'/ '"$oppai_stars"'★'

		#      v combo max          v beatmap combo
		if [[ $CURL_pp != "null" ]]; then
			if [[ "$oppai_combomax" != "$CURL_maxcombo" ]]; then
				PP='**'"$CURL_pp"'**pp ~~['"$oppaiFC_pp"'pp FC @'"$oppai_ACCURACY"']~~'
			else
				if [[ $oppai_ACCURACY != "100"* ]]; then
					oppaiOUTPUT100=$(echo "$CURL_bmdata" | ./osu/oppai - $MODS -m${gamemode} -ojson | sh JSON.sh -b)
					OUTPUTpp100=$(jsonparse "$oppaiOUTPUT100" '["pp"]' | roundpipe 2)
					PP='**'"$CURL_pp"'**pp ~~['"$OUTPUTpp100"'pp FC @100%]~~'
				else
					PP='**'"$CURL_pp"'**pp'
				fi
			fi
		else
			PP="~~${oppai_pp}pp if ranked~~"
		fi

		[[ $PPnerf ]] && PP+=" $(ppnerfcheck "$PPnerf" "$PP")"

		echo "$(rank-to-emoji "$CURL_rank")"' ▷**'"$CURL_score"'** / '"$PP"
		echo '**'$oppai_ACCURACY'** / **'"$CURL_maxcombo"'**/'"$oppai_combomax"'x / '$CIRCLECOUNTS
		if [[ $ENTRY != $(($RESULTScount - 1)) ]]; then echo ""; fi
	fi
	ENTRY=$(($ENTRY + 1))
done
