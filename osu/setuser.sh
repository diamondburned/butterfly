#!/usr/bin/env bash
uuid=$1

shift
args=( $@ )
set -- "${args[@]}"


STRING=()
until [[ $# = 0 ]]; do
	key="$1"
	case $key in
		-n|--nickname)
			nickname="${*:2}"
			nickname="${nickname//-r}"
			nickname="${nickname//--ripple}"
			shift
			shift
			;;
		-r|--ripple)
			MODE="ripple"
			shift
			;;
		--reset)
			RESET="true"
			shift
			;;
		*)
			STRING+=( "$1" )
			shift
			;;
	esac
done

set -- "${STRING[@]}"

input="$*"

[[ -z $MODE ]] && MODE="osu"

. config
. bin/funcs.bash
#database file is data/database.csv

if [[ $RESET = "true" ]]; then
	{ redis-cli del "${uuid}:osu" "${uuid}:ripple" || exit 1; } &> /dev/null
	echo "Wiped osu! usernames from the database."
	exit 0
fi

if [[ ! -z $nickname ]]; then
	chkVotes=$(redis-cli get "${uuid}:discordbot")
	if [[ -z "$chkVotes" ]]; then
		echo "Please vote at \`~hasVoted\` to set a nickname! $uuid:discordbot"
		exit
	fi

	[[ ${#nickname} -gt 100 ]] && {
		echo "Nicknames should be shorter than 100 characters!"
		exit 0
	}

	REDIS=( $(redis-cli get "${uuid}:${MODE}") ) || exit 1
	ENTRY="${REDIS[0]}"
	alias="${REDIS[*]:1}"

	if [[ "$nickname" = "$alias" ]]; then
		echo "Nickname not changed."
		exit
	else
		push=$(echo -en "${ENTRY}\n${nickname//\\/\\\\}")
		redis-cli set "${uuid}:${MODE}" "${push}" &> /dev/null || exit 1
	fi

	echo 'Nickname set to `'"$nickname"'`'

	exit 0
fi

if [[ $input = '' || $input = '--ripple' ]]; then
	echo 'Invalid argument! Usage: `~osuset [username]`'
fi

##echo 'Snowflake: '$uuid
##echo 'Input: '"$input"
userinput="$input"

if [[ $MODE = "ripple" ]]; then
	CURL=$(curl -s ripple.moe/api/get_user -d "k=$API" -d "u=$userinput" -d "m=0")
else
	CURL=$(curl -s osu.ppy.sh/api/get_user -d "k=$API" -d "u=$userinput" -d "m=0")
fi

if [[ $CURL = "[]" ]]; then
	echo osu! username is invalid!
	exit
fi	

#if [ ! -e "data/database.csv" ] ; then
#	touch "data/database.csv"
#fi

REDIS=( $(redis-cli get "${uuid}:${MODE}") ) || exit 1
ENTRY="${REDIS[0]}"
alias="${REDIS[*]:1}"

JSON=$(sh JSON.sh -b <<< "$CURL")
userinput=$(json-parse "$JSON" '[0,"username"]')

redis-cli set "${uuid}:${MODE}" "${userinput}" &> /dev/null || exit 1
echo 'Username set to `'"$userinput"'`, nickname has been reset.'
