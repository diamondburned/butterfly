#!/usr/bin/env bash
set -fo pipefail
set -- $@
. bin/funcs.bash
. config

if [[ -z "$*" ]]; then
	err "ERR:INV_ARG"
	exit 1
fi

SHIFT=0

limit="1"
manga="false"

STRING=()
until [[ $# = 0 ]]; do
	case "$1" in
		"--search")
			limit="5"
			shift
			;;
		"-s"|"--shift")
			SHIFT="$2"
			shift
			shift
			;;
		"-m"|"--manga")
			manga="true"
			shift
			;;
		*)
			STRING+=( "$1" )
			shift
			;;
	esac
done

[[ $limit -eq 5 ]] && SHIFT=$(( SHIFT * 5 ))

string=$(printf "%s " "${STRING[@]}")
string=$(urlencode "${string:0:-1}")

endpoint="https://kitsu.io/api/edge"
useragent="Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"

if [[ $manga = "true" ]]; then
	type="manga"
	fields="fields[manga]=id,titles,canonicalTitle,slug,posterImage,status,averageRating,chapterCount,volumeCount,synopsis&include=genres&"
else
	type="anime"
	fields="fields[anime]=id,titles,canonicalTitle,slug,posterImage,status,averageRating,episodeCount,episodeLength,synopsis&include=genres&"
fi

searchurl="${endpoint}/${type}?${fields}page[limit]=${limit}&page[offset]=${SHIFT}&filter[text]=${string}"

#echo "$searchurl" && exit

CURL=$(curl -sg -A "$useragent" \
			-H 'Accept: application/vnd.api+json' \
			-H 'Content-Type: application/vnd.api+json' \
			"$searchurl" | sh JSON.sh -b) || {
				echo "Something went wrong..."
				exit 1
			}


if [[ $CURL =~ $'["meta","count"]\t0' ]]; then
	echo Nothing found
	exit 0
fi

entries=$(echo "$CURL" | grep '\["data",.*,"type"\]' | lineCount)

main() {
	type=$1
	e="${@:2}"

	CURLid=$(jsonparse "$CURL"				'["data",'$e',"id"]'						 		)
	CURLslug=$(jsonparse "$CURL"			'["data",'$e',"attributes","slug"]'		 			)
	CURLromaji=$(jsonparse "$CURL"			'["data",'$e',"attributes","titles","en_jp"]' 		)
	CURLjapanese=$(jsonparse "$CURL"		'["data",'$e',"attributes","titles","ja_jp"]' 		)
	CURLenglish=$(jsonparse "$CURL"			'["data",'$e',"attributes","titles","en"]' 			)
	CURLtype=$(jsonparse "$CURL"			'["data",'$e',"type"]' 								)

	[[ "$type" != "search" ]] && {
		CURLstatus=$(jsonparse "$CURL"			'["data",'$e',"attributes","status"]' 				)
		CURLrating=$(jsonparse "$CURL"			'["data",'$e',"attributes","averageRating"]' 		)
		CURLepisodes=$(jsonparse "$CURL"		'["data",'$e',"attributes","episodeCount"]' 		)
		CURLlength=$(jsonparse "$CURL"			'["data",'$e',"attributes","episodeLength"]' 		)
		CURLchapter=$(jsonparse "$CURL"			'["data",'$e',"attributes","chapterCount"]' 		)
		CURLvolumes=$(jsonparse "$CURL"			'["data",'$e',"attributes","volumeCount"]' 			)
		CURLsynopsis=$(jsonparse "$CURL"		'["data",'$e',"attributes","synopsis"]' 			)
		CURLcanonicaltitle=$(jsonparse "$CURL"	'["data",'$e',"attributes","canonicalTitle"]' 		)
		CURLthumbnail=$(jsonparse "$CURL"		'["data",'$e',"attributes","posterImage","original"]')

		genres=()
		includeCount="$(echo "$CURL" | grep -E '\["included",.*,"type"\]' | getnthkey 2)"
		for ((i=0; i<$includeCount; i++)) {
			inclType="$(jsonparse "$CURL" '["included",'$i',"type"]')"
			case "$inclType" in
			"genres")
				genres+=( "$(jsonparse "$CURL" '["included",'$i',"attributes","name"]' )" )
				;;
			# todo: add more
			esac
		}

		genres_string=$(printf "%s, " "${genres[@]}")

		CURLsynopsis="${CURLsynopsis%%$'\n'*}"
		[[ $CURLsynopsis = *"." ]] && CURLsynopsis="${CURLsynopsis/$'\r'}.."

		# case "$CURLsynopsis" in
		# 	*"\r\n"*)	CURLsynopsis="$(echo $CURLsynopsis | sed 's/\\r\\n.*/.../')";;
		# 	*"\n"*)		;;
		# 	*)		CURLsynopsis="$CURLsynopsis";;
		# esac
	}

	readarray CURLnull <<< "$(declare -p | startsWithP "declare -- CURL" | containsP "null")"
	for i in "${CURLnull[@]}"; do
		[[ -z "$i" || "$i" = $'\n' ]] && continue

		: "${i//declare -- /}"
		declare "${_%%=*}=?"
	done
	
	case "$CURLstatus" in
		tba)		CURLstatus="Unreleased" ;;
		finished)	CURLstatus="Finished" ;;
		current)	CURLstatus="Ongoing" ;;
		*)			: ;;
	esac
	
	if [[ $CURLenglish != "?" ]]; then
		name="$CURLenglish"
	elif [[ $CURLcanonicaltitle != "?" ]]; then
		name="$CURLcanonicaltitle"
	else 
		name="$CURLromaji"
	fi

	[[ $CURLjapanese ]] && \
		name+=' 「'"$(echo -e $CURLjapanese)"'」'

	case $type in
		search)	
			[[ -z $CURLslug ]] && return

			echo "$(($e+1)) / $CURLtype"' / ['"$name"'](https://kitsu.io/'"$CURLtype"'/'"$CURLslug"')'
			;;
		*)	
			echo '/'"$CURLtype"'/ '"$name"
			echo 'https://kitsu.io/'"$CURLtype"'/'"$CURLslug"
			echo "$CURLthumbnail"
			[[ "${genres_string:0:-2}" ]] && {
				echo "Genres: ${genres_string:0:-2}"
			} || {
				echo "Data fetched from Kitsu"
			}

			[[ $CURLvolumes ]] && {
				echo '**'"$CURLrating"'**/₁₀₀ / **'"$CURLstatus"'** / '"$CURLvolumes"' volumes, '"$CURLchapter"' chapters'
			} || {
				echo '**'"$CURLrating"'**/₁₀₀ / **'"$CURLstatus"'** / '"$CURLepisodes"' episodes, '"$CURLlength"' minutes each'
			}

			echo '>>   '"$CURLsynopsis"
			;;
	esac
}

if [[ $limit -eq 5 ]]; then
	echo '/'"$type"'/ Showing '$entries' results:'
	echo ''
	echo ''
	echo 'Add -s [n] to show the nth page'
	ENTRY=0
	until [[ $ENTRY = $entries ]]; do
		main "search" "$ENTRY"
		ENTRY=$(( $ENTRY + 1 ))
	done
else #
	main "asd" "0"
fi
