#!/usr/bin/env bash
set -- $1

if [[ $1 = "--shift" || $1 = "-s" ]]; then
	shift=$(sed 's/[^0-9]*//g' <<< "$2")
	[[ $shift = "0" ]] && shift=""
	shift
	shift
fi

urlencode() {
	# https://gist.github.com/cdown/1163649
	old_lc_collate=$LC_COLLATE
	local LC_COLLATE=C
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
			*) printf '%%%02X' "'$c" ;;
		esac
	done
}

string="$@"

if [[ $string = "" || $string = " " ]]; then
	echo Invalid args!
	exit 1
fi

string=$(urlencode "$string")

endpoint="https://nyaa.si/"
options="?f=0&c=0_0&s=seeders&q=$string"

# Raw HTML
CURL=$(curl -sg "${endpoint}${options}")
#CURL=$(cat tmp/nyaa)

URLTITLE=$(grep -F '<a href="/view/' <<< "$CURL")

IFS=$'\n'

URLs=( $(cut -d'"' -f2 <<< "$URLTITLE") )
TITLEs=( $(cut -d'"' -f4 <<< "$URLTITLE") )
CATEGORIES=( $(echo "$CURL" | grep -F '<a href="/?c=' | cut -d'"' -f4) )
SIZE=( $(echo "$CURL" | grep -F '<td class="text-center">' | grep -F 'iB' | cut -d'>' -f2 | cut -d'<' -f1) )
SEEDERS=( $(echo "$CURL" | grep -F '<td class="text-center" style="color: green;">' | cut -d'>' -f2 | cut -d'<' -f1) )
LEECHERS=( $(echo "$CURL" | grep -F '<td class="text-center" style="color: red;">' | cut -d'>' -f2 | cut -d'<' -f1) )

shiftrule="{${shift}0..${shift}9}"

n="${shift}0"
started=$n
entryend="${shift}9"

[[ "${TITLEs[$n]}" = "" ]] && echo "Nothing here." && exit 0

while true; do
	[[ "${TITLEs[$n]}" = "" ]] && ended=$(( $n - 1 )) && break
	print+=( "**$(( $n + 1))/** ${SIZE[$n]} / \🔼**${SEEDERS[$n]}** \🔽**${LEECHERS[$n]}** / **$(sed 's/ -/** -/' <<< ${CATEGORIES[$n]})"$'\n['"${TITLEs[$n]}"'](https://nyaa.si'"${URLs[$n]}"')' )
	#print+=(  )
	if [[ $n -lt $entryend ]]; then
		n=$(( $n + 1 ))
	else
		ended=$n
		break
	fi
done

lines=$(( $(printf '%s\n' "${print[@]}" | wc -l) / 2 ))
echo "Showing $lines entries, from $(( $started + 1 )) to $(( $ended + 1 ))" # L1, Author title
echo "${endpoint}${options}" # L2, Author URL
printf '%s\n' "${print[@]}" # L*, Body