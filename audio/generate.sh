#!/usr/bin/env bash
mkdir -p tmp/

LINK="$@"

DATE=$(date +%s)

wget "${LINK}" -q -O - | ffmpeg -i /dev/stdin -preset ultrafast "tmp/${DATE}.wav" &> /dev/null

./waveform/waveform -g 1200x360 -F 0x00000000 -B 0x00FFFFFF -G 0x00000000 -O 0 "tmp/${DATE}.wav" "tmp/${DATE}-waveform.png"

echo -n $(pwd)'/tmp/'${DATE}'-waveform.png'

rm "tmp/${DATE}.wav" &> /dev/null