#!/usr/bin/env bash
mkdir -p tmp/

LINK="$@"

FILENAME=$(echo "$LINK" | awk -F '/' '{print $NF}')

DATE=$(date +%s)

wget "${LINK}" -q -O - | ffmpeg -i /dev/stdin -preset ultrafast -acodec libopus -b:a 64k "tmp/${FILENAME}.ogg" &> /dev/null

echo -n $(pwd)'/tmp/'${FILENAME}'.ogg'
