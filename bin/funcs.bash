#!/usr/bin/env bash
set -fo pipefail
#set -f

displaytime() {
	local T=$1
	local D=$((T / 60 / 60 / 24))
	local H=$((T / 60 / 60 % 24))
	local M=$((T / 60 % 60))
	local S=$((T % 60))
	(($D > 0)) && printf '%d days ' $D
	(($H > 0)) && printf '%d hours ' $H
	(($M > 0)) && printf '%d minutes ' $M
	(($D > 0 || $H > 0 || $M > 0)) && printf 'and '
	printf '%d seconds\n' $S
}

jsonparse() {
	JSONsh="$1"
	KEY="${@:2}"
	IFS=$'\n'
	LINE=($(contains "$JSONsh" "$KEY"))
	for line in "${LINE[@]}"; do
		STRING="${line#*	}"
		STRING="${STRING/#\"/}"
		STRING="${STRING//\\\//\/}"
		echo -e "${STRING/%\"/}"
	done
}

getnthkey() {
	key=$(($1 - 1))
	#echo "$key"
	read -d"" -r input

	input=$(lastline "$input")
	input="${input//\,/$'\n'}"
	input="${input%]*}"

	IFS=$'\n' readarray input_array <<<"${input:1}"
	echo -en ${input_array["$key"]}
}

thousandths() {
	read -d "" -r pipe
	echo "$pipe" | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta'
}

timeago() {
	local CURLdate="$@"
	local TIMEepoch=$(/bin/date -d"${CURLdate}" -u +%s)
	local NOWepoch=$(printf "%(%s)T")
	local DIFF=$(($NOWepoch - $TIMEepoch))
	echo "$(displaytime ${DIFF}) ago"
}

split() {
	IFS=$'\n' read -d "" -ra arr <<<"${1//$2/$'\n'}"
	printf '%s\n' "${arr[@]}"
}

urlencode() {
    for ((i=0;i<${#1};i++)) {
        s=${1:i:1}

        if [[ $s == [a-zA-Z0-9.~_-] ]]; then
            printf %s "$s"

        else
            printf '%%%02X' "'$s"
        fi
    }

    printf '\n'
}

convertsecs() {
	secs=$1
	TIMESTAMP=$(printf '%02d:%02d:%02d' $((secs / 3600)) $((secs % 3600 / 60)) $((secs % 60)))
	if [[ $TIMESTAMP == "00:"* ]]; then
		echo "${TIMESTAMP#*:}"
	else
		echo "${TIMESTAMP}"
	fi

	#[[ ! $1 ]] && echo 0 && exit
	#date -d@"$1" +%H:%M
}

round() {
	rounded=$(printf "%.$1f" "$2")
	[[ ${rounded} == *"0" ]] && rounded=${rounded%.*}
	printf $rounded
}

roundpipe() {
	IFS=$'\n' read -d "" -ra array

	for int in "${array[@]}"; do
		rounded=$(printf "%.$1f" "${int}")
		trailings=${rounded#*.}
		((${trailings} == 0)) && rounded="${rounded%.*}"
		printf $rounded
	done
}

lastline() {
	readarray line <<<"$*"
	[[ ${#line[@]} -gt 0 ]] && echo -e "${line[-1]}"
}

lastlineP() {
	read -d "" -ra line
	[[ ${#line[@]} -gt 0 ]] && echo -e "${line[-1]}"
}

firstline() {
	readarray line <<<"$*"
	[[ ${#line[@]} -gt 0 ]] && echo -e "${line[0]}"
}

ppnerfcheck() {
	PPnerf=$1
	PP=$2
	if [[ ${PP/pp/} == $PPnerf ]]; then
		echo '(not nerfed)'
	else
		echo "~~($(round 2 $PPnerf)pp nerfed)~~"
	fi
}

fakecut() {
	# Usage: fakecut "delimiter" "entry"
	read -r line
	local IFS=$'\n'
	entry=$2
	entry=$((entry - 1))
	arr=(${line//$1/$'\n'})
	printf '%s\n' "${arr[$entry]}"
}

startsWith() {
	readarray lines <<<"$1"

	for line in "${lines[@]}"; do
		[[ "$line" == "$2"* ]] && printf "%s" "${line/$'\r'/}"
	done
}

startsWithP() {
	readarray lines

	for line in "${lines[@]}"; do
		[[ "$line" == "$1"* ]] && printf "%s" "${line/$'\r'/}"
	done
}

contains() {
	readarray lines <<<"$1"

	for line in "${lines[@]}"; do
		[[ "$line" == *"$2"* ]] && printf "%s" "${line/$'\r'/}"
	done
}

containsP() {
	readarray lines

	for line in "${lines[@]}"; do
		[[ "$line" == *"$1"* ]] && printf "%s" "${line/$'\r'/}"
	done
}

bashgrep() {
	read -d "" -ra lines

	for line in "${lines[@]}"; do
		[[ "$line" == *"$2"* ]] && printf "%s" "${line/$'\r'/}"
	done
}

parseBeatmapKey() {
	string=$(startsWith "$1" "$2")

	echo -n "${string#*:}"
}

lineCount() {
	readarray lines
	echo "${#lines[@]}"
}

lineCountP() {
	IFS=$'\n' readarray lines
	echo "${#lines[@]}"
}

wcl() {
	IFS=$'\n' read -d "" -ra lines
	printf "${#lines}"
}

splitString() {
	# Usage: split "string" "delimiter"
	IFS=$'\n' read -d "" -ra arr <<<"${1//$2/$'\n'}"
	printf '%s\n' "${arr[@]}"
}

headfn() {
	# Usage: head "n" "whatever"
	mapfile -tn "$1" line <<<"$2"
	printf '%s\n' "${line[@]}"
}

floor() {
	printf ${1%.*}
}

osugm() {
	case $1 in
	*0*) echo "osu!" ;;
	*1*) echo "osu! Taiko" ;;
	*2*) echo "osu! Catch the Beat" ;;
	*3*) echo "osu! Mania" ;;
	osu)
		echo "0"
		;;
	taiko)
		echo "1"
		;;
	ctb)
		echo "2"
		;;
	mania)
		echo "3"
		;;
	*)
		echo "0"
		;;
	esac

	exit 0
}

getTime() {
	export TZ="$1"
	printf "%(%A, %B %d, %Y, %I:%M %p)T"
}

circlecounts() {
	CIRCLECOUNTS=($1 $2 $3 $4)
	CIRCLEDEFINE=(300 100 50 miss)
	CIRCLESHIFT=0
	for e in ${CIRCLEDEFINE[@]}; do
		if ! [[ $e == "" ]]; then
			__circles=$(thousandths <<<"${CIRCLECOUNTS[$CIRCLESHIFT]}")
			echo '**'"${__circles}"'**x'"${CIRCLEDEFINE[$CIRCLESHIFT]}"
		fi
		((CIRCLESHIFT++))
	done
}

statuscode() {
	STATUScode="$1"
	case $STATUScode in
	-2)
		RANKED="⚰"
		;;
	-1)
		RANKED="🚧"
		;;
	0)
		RANKED="⏳"
		;;
	1)
		RANKED="⏫"
		;;
	2)
		RANKED="✅"
		;;
	3)
		RANKED="✅"
		;;
	4)
		RANKED="❤"
		;;
	esac

	echo -n "$RANKED"
}

plot() { # plot "bmid" (returns tmp/$bmid}.png)
	plot_header="
set terminal pngcairo size 1600,600 font 'Sans,13' transparent truecolor;
set output 'tmp/${1}.png';
set timefmt '%M:%S';
set xdata time;
set format y '%.2f';
set border lw 3 lc rgb '#7289DA';
set xtics textcolor rgb '#7289DA';
set ytics textcolor rgb '#7289DA';
set multiplot layout 1,1 rowsfirst;
set key font 'Sans,12';
set border back;
set tics nomirror;
plot '-' using 1:2 with line lw 2 lc rgb '#7289DA' title ' ★'
set encoding utf8;
0:00.000 0"

	stamp=$(printf "%(%s)T")
	echo "$plot_header" > "/tmp/$stamp.gp"

	averagePlot >> "/tmp/${stamp}.gp"

	gnuplot "/tmp/${stamp}.gp" > /dev/null || {
		rm "/tmp/${stamp}.gp"
		echo ""
		return
	}

	rm "/tmp/${stamp}.gp"

	echo "tmp/${1}.png"
}

bmlength() {
	read -d "" -r beatmap

	bpm=$(lastline "$(grep -m 1 -F $'[TimingPoints]' -A 1 <<<"${beatmap/$'\r'/}")" | fakecut ',' 2 | tr -dc '0-9.')
	BPM=$(echo '60000 / '${bpm} | bc -l | roundpipe 1)

	Line=$(($(echo "$beatmap" | grep -n -F '[HitObjects]' | cut -d':' -f1) + 1))
	HitObjects=$(echo "$beatmap" | tail -n +${Line} | sed '/^\s*$/d')
	TotalObjects=$(echo "$beatmap" | tail -n +${Line} | sed '/^\s*$/d' | wcl)

	LastLine=$(echo "$HitObjects" | head "-${TotalObjects}" | tail -1)
	TotalTime=$(echo "$LastLine" | cut -d',' -f3)

	# No I'm not sorry
	[[ $mod == *"DT"* ]] || [[ $mod == *"NC"* ]] && { # if DT/NC
		TotalInTime=$(round 0 "$(bc -l <<<"$TotalTime / 1000 / 1.5")")
		BPM="$(bc -l <<<"$BPM / 1.5" | roundpipe 1)"
	} || {
		TotalInTime=$((TotalTime / 1000))
	}

	Length=$(convertsecs "${TotalInTime}")
	printf "%s\n%s\n" "$BPM" "$Length"
}

escMD() {
	read -d "" -r string

	string="${string//\_/\\\_}"
	string="${string//\*/\\\*}"
	string="${string//\~/\\\~}"
	string="${string//\`/\\\`}"

	printf "$string"
}

fetchosu() {
	#curl=$(timeout 20s curl -s "localhost:12288?url=$1")
	#[[ "$curl" != *"osu file format"* ]] && {
		curl=$(curl -s "$1")
		[[ "$curl" != *"osu file format"* ]] && return 1
	#}

	echo "$curl"
}

averagePlot() {
	declare -A data

	local scan=0
	while read -d $'\n' -r line; do
	    [[ "$line" =~ "plot '-' with lines" ]] && {
	        let "scan++"
	        continue
	    }

	    [[ "$line" == e ]] && {
	        let "scan++"
	        continue
	    }

	    if [[ "$scan" == 1 ]]; then
	        IFS=" " read -r t s <<< "$line"
			data["$t"]=$(( ${s%%.*} ))
	    elif [[ "$scan" == 3 ]]; then
	        IFS=" " read -r t s <<< "$line"
			v=$(( ${s%%.*} ))
	        data["$t"]=$(( (${data["$t"]} + $v) / 2 ))
	    fi
	done

	graph=""

	for k in "${!data[@]}"; {
		((m=(k/1000)/60))
		((s=(k/1000)%60))
		subsec="${k: -3}"
		printf -v line '%d:%02d.%03d' "$m" "$s" "${subsec##*0}"

		graph+="$line ${data[$k]}"$'\n'
	}

	echo "$graph" | sort -n -t ' '
}

# arg beatmapID
fetchBeatmap() {
	# If exists in cache, return
	beatmap=$(redis-cli get "beatmaps:$1")
	[[ "$beatmap" ]] && {
		echo "$beatmap"
		return
	}

	# Else
	beatmap=$(fetchosu "https://osu.ppy.sh/osu/$1")
	redis-cli setex "beatmaps:$1" 1800 "$beatmap" &> /dev/null
	
	echo "$beatmap"
	return
}
