#!/usr/bin/env bash
. config

displaytime() {
	local T=$1
	local D=$((T/60/60/24))
	local H=$((T/60/60%24))
	local M=$((T/60%60))
	local S=$((T%60))
	(( $D > 0 )) && printf '%d days, ' $D
	(( $H > 0 )) && printf '%d hours, ' $H
	(( $M > 0 )) && printf '%d minutes ' $M
	(( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
	printf '%d seconds\n' $S
};

now=$(date +%s)

if [[ $1 = "--string" ]]; then
	inputstring="${@:2}"
	inputepoch=$(command date -d"$inputstring" +%s)
else
	inputepoch="$1"
	if [[ ${#inputepoch} != 10 ]]; then
		if [[ ${#inputepoch} = 13 ]]; then
			inputepoch=$(echo $inputepoch / 1000 | bc -l)
			#             floating point-v v-variable 
			inputepoch=$(printf "%.*f\n" 0 $epoch)
		else
			echo "Invalid epoch"
			exit 1
		fi
	fi
fi

epochago=$(( $now - $inputepoch ))

displaytime "$epochago"