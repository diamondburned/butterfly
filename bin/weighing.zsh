#!/usr/bin/env bash
ENTRY=$[ $1 - 1 ] # 1st, 2nd, 3rd, ...
PP=$2
printf "%.2f" "$(bc -l <<< "$PP * (0.95^${ENTRY})")"
