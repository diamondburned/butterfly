#!/bin/zsh
# command $CURLrank $HITOBJECTS $TOTALOBJECTS $CURL50 $CURL100 $CURL300 $CURLmiss $CURLmods
CURLrank=$1
HITOBJECTS=$2
TOTALOBJECTS=$3
CURL50=$4
CURL100=$5
CURL300=$6
CURLmiss=$7
CURLmods=$8

convertsecs() {
	secs=$1
	TIMESTAMP=$(printf '%02d:%02d:%02d\n' $(($secs/3600)) $(($secs%3600/60)) $(($secs%60)))
	if [[ $TIMESTAMP = "00:"* ]]; then
		echo "$TIMESTAMP" | cut -c 4-
	else
		echo "$TIMESTAMP"
	fi
}

round() {
	printf "%.$1f" "$2"
}

if [[ $CURLrank = "F" ]]; then
	SOFAR=$(( $CURL50 + $CURL100 + $CURL300 + $CURLmiss -1 ))
	# v maybe -1 this
	THATLINE=$(echo "$HITOBJECTS" | head -${SOFAR} | tail -1)
	TIMESOFAR=$(echo "$THATLINE" | cut -d',' -f3)
	LASTLINE=$(echo "$HITOBJECTS" | head -${TOTALOBJECTS} | tail -1)
	TOTALTIME=$(echo "$LASTLINE" | cut -d',' -f3)
	STARTINGTIME=$(echo "$HITOBJECTS" | head -1 | tail -1 | cut -d',' -f3)
	COMPLETION=$(round 2 `cat` <<< "$(( $TIMESOFAR / $TOTALTIME * 100 ))")
	if [[ $OUTPUTmods =~ "DT" ]]; then
		THATPOINTintime=$(convertsecs $(printf "%.0f" `cat` <<< "$(( ${TIMESOFAR} / 1000 / 1.5 ))"))
		TOTALintime=$(convertsecs $(printf "%.0f" `cat` <<< "$(( ${TOTALTIME} / 1000 / 1.5 ))"))
	else
		THATPOINTintime=$(convertsecs $(printf "%.0f" `cat` <<< "$(( ${TIMESOFAR} / 1000 ))"))
		TOTALintime=$(convertsecs $(printf "%.0f" `cat` <<< "$(( ${TOTALTIME} / 1000 ))"))
	fi
else
	COMPLETION=""
fi