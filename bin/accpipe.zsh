#!/bin/zsh
retMax() {
	if [ $1 -gt $2 ]; then
		echo $1
	else 
		echo $2
	fi
}

retMin() {
	if [ $1 -lt $2 ]; then
		echo $1
	else 
		echo $2
	fi
}

lineCount() {
	IFS=$'\n'
	lines=( $1 )
	printf "${#lines}"
}

splitString() {
	# Usage: split "string" "delimiter"
	IFS=$'\n' read -d "" -ra arr <<< "${1//$2/$'\n'}"
	printf '%s\n' "${arr[@]}"
}

headfn() {
    # Usage: head "n" "whatever"
    mapfile -tn "$1" line <<< "$2"
    printf '%s\n' "${line[@]}"
}

floor() {
	printf ${1%.*}
}

startsWith() {
    IFS=$'\n'
	lines=( $1 )

	for line in "${lines[@]}"; do
		[[ "$line" = "$2"* ]] && printf "%s" "${line/$'\r'}"
	done
}

acccalc() {
    gamemode="$1"
    CURL50="$2"
    CURL100="$3"
    CURL300="$4"
    CURLmiss="$5"
    katu="$6"
    geki="$7"

    if [[ $gamemode = "1" ]]; then
    	hitsNumber=$(( $CURL100 + $CURL300 + $CURLmiss ))
    	hitsPoint=$(( ($CURL100 * 0.5 + $CURL300 * 1.) * 300. ))
    elif [[ $gamemode = "2" ]]; then
    	hitsNumber=$(( $CURLmiss + $CURL50 + $CURL100 + $CURL300 + $katu ))
    	hitsPoint=$(( $CURL50 + $CURL100 + $CURL300 ))
    elif [[ $gamemode = "3" ]]; then
    	hitsNumber=$(( $CURLmiss + $katu + $geki + $CURL50 + $CURL100 + $CURL300 ))
    	hitsPoint=$(( $CURL50 * 50. + $CURL100 * 100. + $katu * 200. + $CURL300 * 300. + $geki * 300. ))
    else
    	hitsNumber=$(( $CURL50 + $CURL100 + $CURL300 + $CURLmiss ))
    	hitsPoint=$(( $CURL50 * 50.0 + $CURL100 * 100.0 + $CURL300 * 300.0 ))
    fi

    if [[ $gamemode = "2" ]]; then
    	ACCURACY=$(( $hitsPoint / $hitsNumber ))
    else
    	ACCURACY=$(( $hitsPoint / ( $hitsNumber * 3.0 ) ))
    fi

    echo -n "$ACCURACY"
}

beatmap="$1"
gamemode="$2"
CURL50="$3"
CURL100="$4"
CURL300="$5"
CURLmiss="$6"
katu="$7"
geki="$8"

spinners=$(lineCount "$(startsWith "$(echo "$beatmap" | grep -F '[HitObjects]' -A9999999999999999 | cut -d, -f4)" "8")")
sliders=$(lineCount "$(startsWith "$(echo "$beatmap" | grep -F '[HitObjects]' -A9999999999999999 | cut -d, -f4)" "2")")
objs=$(lineCount "$(echo "$beatmap" | grep -F '[HitObjects]' -A9999999999999999 | cut -d, -f4 | head -n-1)")

max300=$(( $objs - $CURLmiss ))
maxacc=$(acccalc ${gamemode} 0 0 "$max300" ${CURLmiss} 0 0)

Penalized300=$( retMax 0 "$(( $CURL300 - $spinners - $sliders ))" )

n50=0
n100=$(floor "$((-3.0 * (( maxacc * 0.01 - 1.0 ) * $objs + $CURLmiss) * 0.5))")

if (( $n100 > ($objs - $CURLmiss) )); then 
    n100=0
    n50=$(floor "$((-6.0 * (($maxacc * 0.01 - 1.0) * $objs + $CURLmiss) * 0.2))")
    n50=$(retMin $max300 $n50)
else 
    n100=$(retMin $max300 $n100)
fi

n300=$(( $objs - $n100 - $n50 - $CURLmiss ))

acccalc ${gamemode} $n50 $n100 $n300 ${CURLmiss} $katu $geki