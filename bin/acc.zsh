#!/usr/bin/env bash

# command gamemode 50 100 300 miss katu geki
gamemode="$1"
CURL50="$2"
CURL100="$3"
CURL300="$4"
CURLmiss="$5"
katu="$6"
geki="$7"

[[ "$8" ]] && hitsNumber="$8"

function retMax() {
	if [ $1 -lt $2 ]; then
		echo $1
	else
		echo $2
	fi
}

if [[ $gamemode == "1" ]]; then
	[[ -z "$hitsNumber" ]] && hitsNumber=$(($CURL100 + $CURL300 + $CURLmiss))
	hitsPoint=$(bc -l <<< "($CURL100 * 0.5 + $CURL300 * 1.) * 300.")
elif [[ $gamemode == "2" ]]; then
	[[ -z "$hitsNumber" ]] && hitsNumber=$(($CURLmiss + $CURL50 + $CURL100 + $CURL300 + $katu))
	hitsPoint=$(($CURL50 + $CURL100 + $CURL300))
elif [[ $gamemode == "3" ]]; then
	[[ -z "$hitsNumber" ]] && hitsNumber=$(($CURLmiss + $katu + $geki + $CURL50 + $CURL100 + $CURL300))
	hitsPoint=$(($CURL50 * 50 + $CURL100 * 100 + $katu * 200 + $CURL300 * 300 + $geki * 300))
else
	[[ -z "$hitsNumber" ]] && hitsNumber=$(($CURL50 + $CURL100 + $CURL300 + $CURLmiss))
	hitsPoint=$(($CURL50 * 50 + $CURL100 * 100 + $CURL300 * 300))
fi

if [[ $gamemode == "2" ]]; then
	ACCURACY=$(($hitsPoint / $hitsNumber))
else
	ACCURACY=$(bc -l <<< "$hitsPoint / ($hitsNumber * 3.0)")
fi

ACCURACY=$(printf "%.2f" "$ACCURACY")

case $ACCURACY in
*'.') ACCURACY="${ACCURACY:0:-1}%" ;;
*) ACCURACY="${ACCURACY}%" ;;
esac

echo -n "$ACCURACY"
