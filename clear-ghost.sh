#!/usr/bin/env bash
jsonexport() {
	prefix="$1" # jsonexport(prefix) :jerry:
	if [[ -z $prefix ]]; then echo "Prefix cannot be empty!" && exit 1; fi
	set -o posix
	IFS=$'\n'
	ENVS=( $(set | grep -i "^$prefix") )
	IFS=$'\n\t'
	JSON_bodyarray=()
	for JSON in "${ENVS[@]}"; do
		k=$"\"$(echo "$JSON" | cut -d= -f1 | sed "s/$prefix//")\""
		v=$"\"$(echo "$JSON" | cut -d= -f2-)\""
		if [[ "$v" = '"('* ]] && [[ "$v" = *')"' ]]; then
			v="${v:2}"
			v="${v:0:-2}"
			# [0]="a" [1]="s" [2]="d"
			#ARRAYS=( $(echo "$v" | sed 's/') )
			ENTRY=0
			THEREST="$v"
			THING=()
			until [[ $THEREST = "" ]]; do
				CURRENT=$(echo "$THEREST" | sed "s/\" \[$(( $ENTRY + 1 ))]=.*/\"/")
				CURRENT1=$(( ${#CURRENT} + 1 ))
				THEREST=$(echo "${THEREST:${CURRENT1}}")
				THING+=( "$(echo "$CURRENT" | sed "s/\[$ENTRY\]=//")" )
				ENTRY=$(( $ENTRY + 1 ))
			done
			body="$(printf '%s, ' "${THING[@]}")"
			body=$"[${body:0:-2}]"
			JSON_bodyarray+=( "$k:$body" )
		else
			JSON_bodyarray+=( "$k:$v" )
		fi
	done
	
	JSON_body=$(printf '%s,' "${JSON_bodyarray[@]}")
	echo '[{'"${JSON_body:0:-1}"'}]'
}


IFS=$'\n'
PID=( $(ps --no-headers -xo "%cpu,%mem,pid,command" --sort -%cpu | grep -F '/usr/bin/env bash -l -i' | grep -v 'grep') )
if [[ ${PID} = "" ]]; then exit 1; fi
if [[ -z $1 ]]; then
	for each in ${PID[@]}; do
		kill -9 $(echo $each | awk '{print $3}')
		echo killed $each
	done
else
	ENTRY=0
	for each in ${PID[@]}; do
		cpu=$(awk '{print $1}' <<< $each)
		mem=$(awk '{print $2}' <<< $each)
		pid=$(awk '{print $3}' <<< $each)
		cmd=$(awk '{$1=""; $2=""; $3=""; print}' <<< $each | cut -d ' ' -f4-)
		eval "JSON${ENTRY}=( \"$cpu\" \"$mem\" \"$pid\" \"$cmd\" )"
		ENTRY=$(( $ENTRY + 1 ))
	done
	jsonexport "JSON"
fi


