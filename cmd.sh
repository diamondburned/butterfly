
#!/usr/bin/env bash
logloc="$HOME/.logs/butterfly.log"
case $1 in
guild)
	log=$(< "$logloc")
	add=$(grep '/+\|/-' < "$logloc")

	add="${add//\/+/$'|\e[32m+\e[39m '}"
	add="${add//\,/|}"

	echo "Date| Time|C Members|Name|"$'\n'"${add//\/-/$'|\e[31m-\e[39m '}" | column -s '|' -t
	exit;;
log)
	tail -f "$logloc"
	exit;;
*)
	echo Unknown command
	exit;;
esac

