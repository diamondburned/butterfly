process.setMaxListeners(1000)
process.stdin.resume()

const { promisify } = require('util')

const redisdb = require('redis')
var redis = redisdb.createClient()
global.aget = promisify(redis.get).bind(redis)
const aget = promisify(redis.get).bind(redis)
const aset = promisify(redis.set).bind(redis)
const adel = promisify(redis.del).bind(redis)

//const cf_ws = require('./src/cf_ws')
//cf_ws.ListenCF("localhost", 12288)

const Discord = require('discord.js')

const fs = require('fs')
const ini = require('ini')
var config = ini.parse(fs.readFileSync('./config', 'utf-8'))

const shard = new Discord.ShardingManager('./shard.js', { 
	token: config.BOT_TOKEN,
	respawn: true,
})

shard.on('shardCreate', (Shard) => {
	console.log(`Shard ${Shard.id} [PID ${Shard.process}] up`)
})

function unhandler(reason, p) {
    timestamp = new Date().toLocaleString()
    console.log(`Handled: Unhandled Rejection on ${timestamp}, reason:\n`, reason)

	pmOwner(`Unhandled exception: \`\`\`js\n${reason}\`\`\ `)
}

process.on('uncaughtException', unhandler)
process.on('unhandledRejection', unhandler)

function pmOwner(string) {
	if (!config.ownerID) {
		return
	}

	const timestamp = new Date().getTime()
	const ownerID   = config.ownerID
	const sendover  = string

	aset(`ticket:${timestamp}`, sendover, 'EX', 300).catch(err => {
		return console.log(`Redis failed at ${timestamp}, error\n${err}`)
	})

	shard.broadcastEval(`
		if (this.shard.id === 0) {
			global.aget("ticket:${timestamp}").then(res => {
				this.users.fetch("${ownerID}").then(async (user) => {
					user.send(res).catch(err => {
						console.log("Failed to send PM to Owner: " + err)
					});
				}).catch(err => {
					console.log("Failed to send PM to Owner: " + err)
				});
			}).catch(err => {
				return console.log(err);
			})
		}
	`).catch(err => {
		console.log(`"ticket:${timestamp}"`)
		console.log(`Failed to send PM to Owner at ${timestamp}. Debug: ${ownerID}\n` + err)
	})
}

shard.spawn(1, 200, -1)
