#!/usr/bin/env bash
cd "$HOME/butterfly"
. config
PORT="$githubci_port"
TIMESTAMP=$(date +%s)

function 200header() {
	echo -e 'HTTP/1.1 200 OK\r\n'
	echo -e "\nButterfly bot webhook gate."
	echo -e "\nYou have found what you should not have. Beware."
}

function end() {
	echo END
	exit 1
}

trap end SIGINT

function pipeline_check() {
	local JSON="$json"
	local object_kind=$(echo "$JSON" | grep -F '["object_kind"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//')
	local status=$(echo "$JSON" | grep -F '["object_attributes","status"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//')
	local detailed_status=$(echo "$JSON" | grep -F '["object_attributes","detailed_status"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//')
	local builds_status=$(echo "$JSON" | grep -F '["builds",0,"status"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//')
	
	if [[ $object_kind != "pipeline" ]]; then
		echo "ERR: Not a pipeline."
		echo "Object Kind: $object_kind"
		echo "Status: $status - $detailed_status - $builds_status"
		return 1
	fi 

	if [[ $status = "success" && $detailed_status = "passed" && $builds_status = "success" ]]; then
		echo "OK: Passed."
		return 0
	else
		echo "ERR: Failed."
		echo "Object Kind: $object_kind"
		echo "Status: $status - $detailed_status - $builds_status"
		return 1
	fi
}

function checktoken() {
	local TOKEN="$1"
	if [[ -z $TOKEN ]]; then
		echo "ERR: No token"
		return 1
	fi

	if [[ -z $headers ]]; then
		echo "ERR: No headers"
		return 1
	fi

	local GLtoken=$(echo "$headers" | grep -F 'X-Gitlab-Token' | cut -d':' -f2 | tail -c +2)
	if [[ $GLtoken != $TOKEN ]]; then
		echo "ERR: Invalid token: $GLtoken"
		return 1
	else
		echo "OK: Valid token"
		return 0
	fi
}

touch -f /tmp/hookout

while true; do 
	hookout=$(200header | nc -lp "$PORT" -q 3)
	if [[ $hookout = "" ]]; then
		echo "$(date) - Failed"
		echo "GitLab Webhook returned nothing."
	else
		writetolog=$(echo "$hookout" | tee "/tmp/hook-${TIMESTAMP}.log")
		headers=$(echo "$hookout" | head -n -1)
		json=$(echo "$hookout" | tail -n 1 | sh JSON.sh -b)
		#echo "$headers" > headers
		#echo "$json" > json

		tokencheck=$(checktoken "$githubci_token")

		if [[ $? -eq 0 ]]; then
			echo "$(date) - Failed"
			echo "$tokencheck"
		else
			pipeline=$(pipeline_check)
			if [[ $? -eq 0 ]]; then
				echo "$(date) - Passed"
				git pull -q --ff origin -q 
				pm2 restart bot &> /dev/null
			else
				echo "$(date) - Failed"
				echo "$pipeline"
			fi
		fi
		echo ENDTURN
	fi
done
