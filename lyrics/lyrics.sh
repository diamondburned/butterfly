#!/usr/bin/env bash
. config
urlencode() {
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
		*) printf "$c" | xxd -p -c1 | while read x;do printf "%%%s" "$x";done
	esac
done
}

trap err ERR
set -- $1
if [[ $@ = "" || $@ = " " ]]; then
	echo 'Please insert keywords!'
	echo ''
	echo '`~lyrics [keywords]`'
	exit
fi

search_parse() {
	CURLthumb=$(echo "$CURL" | grep -F '["response","hits",0,"result","header_image_thumbnail_url"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//') 
	IFS=$'\n'
	CURLtitle=( $(echo "$CURL" | grep -F ',"result","full_title"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//') )
	CURLid=( $(echo "$CURL" | grep -F ',"result","id"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//') )
	CURLurl=( $(echo "$CURL" | grep -F ',"result","url"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//') )

	if (( ${#CURLid[@]} > 6 )); then
		RESULTS=6
	else
		RESULTS=${#CURLid[@]}
	fi

	echo "Showing $(( $RESULTS )) results"
	echo $CURLthumb

	INDEX=0
	until [[ $RESULTS = $INDEX ]]; do
		INDEX1=$(( $INDEX + 1 ))
		echo "**$INDEX1/** ${CURLid[$INDEX]} / [${CURLtitle[$INDEX]}](${CURLurl[$INDEX]})"
		INDEX=$INDEX1
	done
}

input="$@"
string=$(urlencode "$input")
CURL=$(curl -s "https://api.genius.com/search?access_token=$GENIUS_TOKEN&q=$string" | sh JSON.sh -b)
search_parse
#echo "$NODE" | cut -c1-50
