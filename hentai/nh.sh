#!/usr/bin/env bash
. bin/funcs.bash

input="$1"
IFS=$'\n'

set -e

galleryid=$(echo "$input" | cut -d/ -f5)
printf -v DATA "$(curl -s https://nhentai.net/g/"$galleryid"/ | \
	grep -oP 'var gallery = new N\.gallery\(\K(.*)(?=\);)' | sh JSON.sh -b)"

LINK="$input"

TITLEpretty=$(jsonparse "$DATA" '["title","pretty"]')
TITLEenglish=$(jsonparse "$DATA" '["title","english"]')
TITLEjapanese=$(jsonparse "$DATA" '["title","japanese"]')
mediaID=$(jsonparse "$DATA" '["media_id"]')

ENTRY=0
TAGCOUNT=$(echo "$DATA" | grep -E '["tags",\d+,"type"]' | wc -l)

declare -a TAGS; declare -a PARODY; declare -a LANGUAGE; declare -a GROUP; declare -a ARTIST; declare -a CHARACTERS; declare -a CATEGORY; declare -a LANGUAGE
until [[ $TAGCOUNT = $ENTRY ]]; do
	TAGNAME=( $(jsonparse "$DATA" '["tags",'$ENTRY',"name"]') )
	TAGTYPE=( $(jsonparse "$DATA" '["tags",'$ENTRY',"type"]') )
	if [[ ${TAGTYPE} = "tag" ]]; then
		TAGS+=( "$TAGNAME" )
	elif [[ ${TAGTYPE} = "parody" ]]; then
		PARODY+=( "$TAGNAME" )
	elif [[ ${TAGTYPE} = "group" ]]; then
		GROUP+=( "$TAGNAME" )
	elif [[ ${TAGTYPE} = "artist" ]]; then
		ARTIST+=( "$TAGNAME" )
	elif [[ ${TAGTYPE} = "character" ]]; then
		CHARACTERS+=( "$TAGNAME" )
	elif [[ ${TAGTYPE} = "category" ]]; then
		CATEGORY+=( "$TAGNAME" )
	elif [[ ${TAGTYPE} = "language" ]]; then
		LANGUAGE+=( "$TAGNAME" )
	fi
	ENTRY=$(( $ENTRY + 1 ))
done

CATEGORYlist="$(printf '%s, ' "${CATEGORY[@]}")"
CATEGORYlist="${CATEGORYlist%??}"
if ! [[ "$TITLEjapanese" = "" ]] && ! [[ "$TITLEenglish" = "" ]]; then
	NAME=$(echo '/'"$(tr '[:lower:]' '[:upper:]' <<< ${CATEGORYlist:0:1})""${CATEGORYlist:1}"'/ '"$TITLEenglish"' 「'"$TITLEjapanese"'」')
else
	NAME=$(echo '/'"$(tr '[:lower:]' '[:upper:]' <<< ${CATEGORYlist:0:1})""${CATEGORYlist:1}"'/ '"$TITLEpretty")
fi

if [[ ${#NAME} > 255 ]]; then
	echo "${NAME:0:253}"...
else
	echo "$NAME"
fi

echo "$input"
echo 'https://i.nhentai.net/galleries/'"$mediaID"'/1.jpg'

if ! [[ ${PARODY} = "" ]]; then
	PARODYlist="$(printf '%s, ' "${PARODY[@]}")"
	echo '**Parody:** '"${PARODYlist%??}"
fi

if ! [[ ${CHARACTERS} = "" ]]; then
	CHARACTERSlist="$(printf '%s, ' "${CHARACTERS[@]}")"
	echo '**Characters:** '"${CHARACTERSlist%??}"
fi

if ! [[ ${TAGS} = "" ]]; then
	TAGSlist="$(printf '%s, ' "${TAGS[@]}")"
	echo '**Tags:** '"${TAGSlist%??}"
fi

if ! [[ ${ARTIST} = "" ]]; then
	ARTISTlist="$(printf '%s, ' "${ARTIST[@]}")"
	echo '**Artist:** '"${ARTISTlist%??}"
fi

if ! [[ ${GROUP} = "" ]]; then
	GROUPlist="$(printf '%s, ' "${GROUP[@]}")"
	echo '**Group:** '"${GROUPlist%??}"
fi

if ! [[ ${LANGUAGE} = "" ]]; then
	LANGUAGElist="$(printf '%s, ' "${LANGUAGE[@]}")"
	echo '**Language:** '"${LANGUAGElist%??}"
fi

if ! [[ ${CATEGORY} = "" ]]; then
	CATEGORYlist="$(printf '%s, ' "${CATEGORY[@]}")"
	echo '**Category:** '"${CATEGORYlist%??}"
fi
