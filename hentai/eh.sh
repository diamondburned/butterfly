#!/usr/bin/env bash
input="$1"
IFS=$'\n'
string=$(echo "$input" | tr ' ' '\n' | grep -F "https://e-hentai.org/g/")
galleryid=$(echo "$string" | awk -F '/' '{print $5}')
gallerytoken=$(echo "$string" | awk -F '/' '{print $6}')

curldata="{\"method\": \"gdata\", \"gidlist\": [[${galleryid},\"${gallerytoken}\"]], \"namespace\": 1}"
DATARAW=$(curl -s -u application_name:application_password --data "$curldata" https://api.e-hentai.org/api.php --header 'Content-Type: application/json; charset=utf-8' --header 'Accept: application/json' | sh JSON.sh)
DATA=$(echo -e "$DATARAW")

TITLE=$(echo "$DATA" | grep -F '["gmetadata",0,"title"]' | sed 's~	"~~g' | sed 's~\["gmetadata",0,"title"\]~~g')
CATEGORY=$(echo "$DATA" | grep -F '["gmetadata",0,"category"]' | awk -F ']' '{print $2}' | sed 's~"~~g' | sed 's~\t~~g')
TAGSRAW=$(echo "$DATA" | grep -F '["gmetadata",0,"tags"]' | sed 's~\["gmetadata",0,"tags"\]\t~~g') 
THUMB=$(echo "$DATA" | grep -F '["gmetadata",0,"thumb"]' | sed 's~\["gmetadata",0,"thumb"\]\t~~g' | sed 's~"~~g' | sed 's~\\~~g') 
RATING=$(echo "$DATA" | grep -F '["gmetadata",0,"rating"]' | sed 's~\["gmetadata",0,"rating"\]\t~~g' | sed 's~"~~g') 
TAGSPROCESSED=$(echo "$TAGSRAW" | sed 's~\[~~g' | sed 's~\]~~g' | sed 's~\,~\n~g' | sed 's~\:~\: ~g' | sed 's~"~~g')

# TAGS
IFS=$'\n'
TAGSlanguage=$(echo "$TAGSPROCESSED" | grep -F language | sed 's~language: ~~g')
	TAGSlanguage=($TAGSlanguage)
	printf -v language "%s," "${TAGSlanguage[@]}"
TAGSparody=$(echo "$TAGSPROCESSED" | grep -F parody | sed 's~parody: ~~g')
	TAGSparody=($TAGSparody)
	printf -v parody "%s," "${TAGSparody[@]}"
TAGScharacter=$(echo "$TAGSPROCESSED" | grep -F character | sed 's~character: ~~g')
	TAGScharacter=($TAGScharacter)
	printf -v character "%s," "${TAGScharacter[@]}"
TAGSgroup=$(echo "$TAGSPROCESSED" | grep -F group | sed 's~group: ~~g')
	TAGSgroup=($TAGSgroup)
	printf -v group "%s," "${TAGSgroup[@]}"
TAGS=$(echo "$TAGSPROCESSED" | grep -v -E "parody:|character:|group:|artist:|language:")
	TAGS=($TAGS)
	printf -v tags "%s," "${TAGS[@]}"

echo '/'$CATEGORY'/ '${TITLE%?}
echo "$string "
echo "$THUMB"
echo '**Rating:** '"$RATING"'★'
if [[ ${language%?} = "" ]]; then
	:
else	
	echo '**Language:** '"${language%?}" | sed 's~,~, ~g'
fi

if [[ ${parody%?} = "" ]]; then
	:
else	
	echo '**Parody:** '"${parody%?}" | sed 's~,~, ~g'
fi

if [[ ${character%?} = "" ]]; then
	:
else	
	echo '**Characters:** '"${character%?}" | sed 's~,~, ~g'
fi

if [[ ${group%?} = "" ]]; then
	:
else	
	echo '**Group:** '"${group%?}" | sed 's~,~, ~g'
fi

echo '**Tags:** '"${tags%?}" | sed 's~,~, ~g'
