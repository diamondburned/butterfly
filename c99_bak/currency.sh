#!/usr/bin/env bash
set -eo pipefail

function err {
	echo "Error triggered, exiting"
}

trap err ERR

function round {
	printf "%.$1f" "$2"
};

. config

set -- $(echo "$1" | sed 's/\\"/"/g')

if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]; then
	exit 1
fi

amount="&amount=${1}"
from="&from=${2}"
to="&to=${3}"

endpoint="https://api.c99.nl/currency.php?key=${c99key}"

CURL=$(curl -s "${endpoint}${amount}${from}${to}") || { exit 1; }

if [[ $CURL =~ "A non-numeric value encountered" ]]; then
	echo "**Error!** Invalid integer at \`\$1\`!"
	exit 0
elif [[ $CURL =~ "Undefined property" ]]; then
	NOTICE=$(grep -F "<b>Notice</b>:" <<< "$CURL")

	ERRclass=$(echo $NOTICE | awk -F '::' '{print $2}' | cut -d' ' -f1)
	ERRarg=${ERRclass/\$/}

	{
		shopt -s nocasematch
		[[ $ERRarg = $1 ]] && after=" at \`\$1\`"
		[[ $ERRarg = $2 ]] && after=" at \`\$2\`"
		[[ $ERRarg = $3 ]] && after=" at \`\$3\`"
	}

	echo "**Error!** Unknown argument \`${ERRarg/\$/}\`${after}"
	exit 0
fi

if [ $(bc <<< "$CURL" 2> /dev/null) ]; then
	CURL=$(round 2 "$CURL")
	int="${CURL%%.*}" # 123
	dec="${CURL##*.}" # .456

	echo "$(sed ':a;s/\B[0-9]\{3\}\>/,&/;ta' <<< "$int").${dec} $(tr '[:lower:]' '[:upper:]' <<< "${3}")"
	exit 0
fi

echo "Unknown error. Logged."
echo "$CURL" | tee -a tmp/c99_currency.log
exit 1

exit 0