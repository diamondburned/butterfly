#!/usr/bin/env bash
set -eo pipefail

function err {
	echo "Error triggered, exiting"
}

trap err ERR

function round {
	printf "%.$1f" "$2"
}

function displaytime {
	local T=$1
	local D=$((T/60/60/24))
	local H=$((T/60/60%24))
	local M=$((T/60%60))
	local S=$((T%60))
	(( $D > 0 )) && printf '%dd/' $D
	(( $H > 0 )) && printf '%d:' $H
	(( $M > 0 )) && printf '%d:' $M
	#(( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
	printf '%d' $S
}

. config

set -- "$1"

if [ ! -z "$@" ] && [[ "$@" = "https://www.youtube.com/watch?v="* ]]; then
	videoid=${@/https\:\/\/www.youtube.com\/watch\?v\=/}
else
	exit 1
fi

arg="&videoid=${videoid}"

endpoint="http://api.c99.nl/youtubedetails.php?json&key=${c99key}"

CURL=$(curl -s "${endpoint}${arg}" | sh JSON.sh -b)
#CURL=$(cat tmp/ytinfo | sh JSON.sh -b)

success=$(json-parse "$CURL" '["success"]')
[[ $success != true ]] && exit 1

title=$(json-parse "$CURL" '["title"]')
uploader=$(json-parse "$CURL" '["uploader"]')
description=$(json-parse "$CURL" '["description"]')
duration=$(json-parse "$CURL" '["duration"]')
views=$(json-parse "$CURL" '["views"]' | tr '.' ','))
likes=$(json-parse "$CURL" '["likes"]' | tr '.' ',')
dislikes=$(json-parse "$CURL" '["dislikes"]' | tr '.' ','))
uploaded=$(json-parse "$CURL" '["uploaded"]')
category=$(json-parse "$CURL" '["category"]')
url=$(json-parse "$CURL" '["url"]')
thumbnail=$(json-parse "$CURL" '["thumbnail"]')

[[ ${#description} -gt 1024 ]] && \
	description="${description:0:1024}..."$'\n(Description was too long and was cut off)'

duration=$(displaytime "$duration")

echo "$title [$uploader] / $duration"
echo "$url"
echo "$thumbnail"
echo "\👍 ${likes} / \👎 ${dislikes} --- ${views} views ---"
echo "Uploaded: $(date -d"${uploaded}" +"%A, %B %d, %Y, %I:%M %p")"
echo $'Description: \n```'"${description}"$'\n```'