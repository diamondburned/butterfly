const fs = require("fs");
const ini = require("ini");
var config = ini.parse(fs.readFileSync("./config", "utf-8"));
var os = require("os");

process.stdin.resume();
process.setMaxListeners(1000);
function handle(signal) {
  timestamp = new Date().toLocaleString();
  console.log(`Received ${signal} on ${timestamp}`);
  pmOwner(`Received ${signal} on ${timestamp}`);
  process.exit(0);
}
process.on("SIGINT", handle);
process.on("SIGTERM", handle);
process.on("uncaughtException", (reason, p) => {
  timestamp = new Date().toLocaleString();
  pmOwner(
    "Unhandled exception on " +
      timestamp +
      "\n```json\n" +
      escMD(JSON.stringify(p)) +
      "```"
  );
  console.log(
    `Handled: Unhandled Rejection on ${timestamp} at:`,
    p,
    "\nReason:",
    reason
  );
});

process.on("unhandledRejection", (reason, p) => {
  timestamp = new Date().toLocaleString();
  pmOwner(
    "Unhandled rejection on " +
      timestamp +
      "\n```json\n" +
      escMD(JSON.stringify(p)) +
      "```"
  );
  console.log(
    `Handled: Unhandled Rejection on ${timestamp} at:`,
    p,
    "\nReason:",
    reason
  );
});

const Discord = require("discord.js");
const client = new Discord.Client({
  apiRequestMethod: "burst",
  messageCacheMaxSize: 2,
  restTimeOffset: 5000,
  retryLimit: 2
});

client.on("error", async err => {
  pmOwner(
    `Websocket disconnected, code ${err.code} \`\`\`js\n${err.reason}\`\`\ `
  );
  if (typeof err.message !== "undefined") {
    if (
      err.message.indexOf("ECONNRESET") !== -1 ||
      err.message.indexOf("socket hang up") !== -1
    ) {
      process.exit();
    }
  }
});

client.on("disconnect", async err => {
  pmOwner(
    `Websocket disconnected, code ${err.code} \`\`\`js\n${err.reason}\`\`\ `
  );
  login();
});

client.on("warn", ev => {
  pmOwner("Websocket warning: ```js\n" + ev.message + "```");
});

const influx = require("influx");
const { execFile } = require("child_process");
const { Throttle } = require("container-throttle");
const sourcequery = require("source-server-query");
const Lyricist = require("lyricist/node6");
const lyricist = new Lyricist(config.GENIUS_TOKEN);
const shell = require("@tehshrike/shell-escape-tag");
const { promisify } = require("util");
const redisdb = require("redis");
var redis = redisdb.createClient();
global.aget = promisify(redis.get).bind(redis);
const aget = promisify(redis.get).bind(redis);
const aset = promisify(redis.set).bind(redis);
const adel = promisify(redis.del).bind(redis);

redis.on("connect", () => {
  console.log("Redis client connected");
});

redis.on("error", err => {
  console.log("Error at redis! " + err);
});

function execRun(fields, callback, keep) {
  const file = fields.shift();

  let args = fields;

  if (!keep) {
    args = fields.join(" ").split(" ");
  }

  try {
    execFile(
      file,
      args,
      {
        cwd: "./",
        shell: false,
        timeout: 30000,
        maxBuffer: 4096
      },
      async (err, stdout, stderr) => {
        if (err || stderr) {
          console.log(err, stderr);
          if (err) return;
        }

        callback(err, stdout, stderr);
      }
    );
  } catch (err) {
    console.log("Error caught while exec", err);
  }
}

function escMD(input) {
  let mds = ["*", "_", "~", "`"];
  for (i = 0; i <= mds.length; ++i) {
    if (input.indexOf(mds[i]) > -1)
      input = input.replace(new RegExp(mds[i], "g"), `\\${mds[i]}`);
  }

  return input;
}

// START OF INFLUXBS
const Influx = new influx.InfluxDB({
  host: "127.0.0.1:8086",
  database: config.influxDATABASE,
  schema: [
    {
      measurement: "Guilds",
      fields: { count: influx.FieldType.INTEGER },
      tags: []
    },
    {
      measurement: "Users",
      fields: { count: influx.FieldType.INTEGER },
      tags: []
    },
    {
      measurement: "Commands",
      fields: { count: influx.FieldType.INTEGER },
      tags: []
    },
    {
      measurement: "Saucenao",
      fields: { count: influx.FieldType.INTEGER },
      tags: []
    },
    {
      measurement: "Latency",
      fields: { count: influx.FieldType.FLOAT },
      tags: []
    },
    {
      measurement: "la1m",
      fields: { count: influx.FieldType.FLOAT },
      tags: []
    },
    {
      measurement: "la5m",
      fields: { count: influx.FieldType.FLOAT },
      tags: []
    },
    {
      measurement: "la15m",
      fields: { count: influx.FieldType.FLOAT },
      tags: []
    },
    {
      measurement: "ghosts",
      fields: { count: influx.FieldType.INTEGER },
      tags: []
    }
  ]
});

Influx.getDatabaseNames()
  .then(names => {
    if (!names.includes(config.influxDATABASE)) {
      return Influx.createDatabase(config.influxDATABASE);
    }
  })
  .catch(error => console.log({ error }));

async function writepoints() {
  return;

  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  await client.shard
    .broadcastEval("global.commandfreq")
    .then(results => {
      global.sum_commandfreq = results.reduce(reducer);
    })
    .catch(console.log);
  await client.shard
    .broadcastEval("global.saucenaofinal")
    .then(results => {
      global.sum_saucenaofinal = results.reduce(reducer);
    })
    .catch(console.log);
  await client.shard
    .broadcastEval("this.guilds.size")
    .then(results => {
      global.sum_guilds_size = results.reduce(reducer);
    })
    .catch(console.log);
  await client.shard
    .broadcastEval("this.users.size")
    .then(results => {
      global.sum_users_size = results.reduce(reducer);
    })
    .catch(console.log);
  await client.shard
    .broadcastEval("this.ws.ping")
    .then(results => {
      ping = results.reduce(reducer);
      global.average_ping = ping / results.length;
    })
    .catch(console.log);

  if (client.shard.id == 0) {
    await cleanup();
    //console.log(`Writing to influx: ${os.loadavg()[0]}; ${os.loadavg()[1]}, ${os.loadavg()[2]}, ${global.ghostsfreq}, ${global.sum_guilds_size}, ${global.sum_users_size}, ${global.sum_commandfreq}, ${global.sum_saucenaofinal}, ${global.average_ping}`);
    Influx.writePoints([
      {
        measurement: "Guilds",
        fields: { count: global.sum_guilds_size },
        tags: []
      },
      {
        measurement: "Users",
        fields: { count: global.sum_users_size },
        tags: []
      },
      {
        measurement: "Commands",
        fields: { count: global.sum_commandfreq },
        tags: []
      },
      {
        measurement: "Saucenao",
        fields: { count: global.sum_saucenaofinal },
        tags: []
      },
      {
        measurement: "Latency",
        fields: { count: global.average_ping },
        tags: []
      },
      { measurement: "la1m", fields: { count: os.loadavg()[0] }, tags: [] },
      { measurement: "la5m", fields: { count: os.loadavg()[1] }, tags: [] },
      { measurement: "la15m", fields: { count: os.loadavg()[2] }, tags: [] },
      { measurement: "ghosts", fields: { count: global.ghostsfreq }, tags: [] }
    ]).catch(err => {
      console.log(`Can't write to influx: ${err}`);
    });
  }
}

function cleanup() {
  return;
}

function pmOwner(string) {
  return;

  if (!config.ownerID) {
    return;
  }

  var timestamp = new Date().getTime();
  var ownerID = config.ownerID;

  aset(`ticket:${timestamp}`, string, "EX", 300).catch(err => {
    return console.log(`Redis failed at ${timestamp}, error\n${err}`);
  });

  client.shard
    .broadcastEval(
      `
		if (this.shard.id === 0) {
			global.aget("ticket:${timestamp}").then(res => {
				this.users.fetch("${ownerID}").then(async (user) => {
					user.send(res).catch(err => {
						console.log("Failed to send PM to Owner: " + err)
					});
				}).catch(err => {
					console.log("Failed to send PM to Owner: " + err)
				});
			}).catch(err => {
				return console.log(err);
			})
		}
	`
    )
    .catch(err => {
      console.log(`"ticket:${timestamp}"`);
      console.log(
        `Failed to send PM to Owner at ${timestamp}. Debug: ${ownerID}\n` + err
      );
    });
}

global.ghostsfreq = 0;
global.commandfreq = 0;
global.after30 = 0;
global.saucenao = 0;
global.saucenao60 = 0;
async function reset() {
  if (global.after30 === 1) {
    await (global.saucenaofinal = global.saucenao60 + global.saucenao);
    await writepoints();
    global.commandfreq = 0;
    global.saucenao = 0;
    global.saucenao60 = 0;
    global.after30 = 0;
  } else {
    await (global.saucenao60 = global.saucenao);
    global.saucenao = 0;
    global.after30 = 1;
  }
}

function shiftstr(str) {
  var str = str + "";
  var a = str
    .split(" ")
    .splice(1)
    .join(" ");
  return a;
}

function stderr_handler(err, stderr, stdout) {
  printerr_arr = (stdout + "").split("\n");
  printerr = printerr_arr[printerr_arr.length];
  var entry = 0;
  while (!printerr) {
    entry++;
    printerr = printerr_arr[printerr_arr.length - entry];
  }
  timestamp = new Date().toLocaleString();
  pmOwner(
    `Shell errored out on ${timestamp}: \`\`\`bash\nerr---\n${err}\nstderr---\n${stderr +
      ""}\nstdout---\n${stdout + ""}\n\`\`\``
  );
  console.log(
    `Error at ${timestamp}! ` + err + "=== OUTPUT ===\n" + stderr + stdout
  );
  return (
    "ERROR at " +
    timestamp +
    "! Please report to `~report`! The error is: ```js\n" +
    printerr +
    "```"
  );
}

// setInterval(reset, 30000);

client.on("ready", () => {
  client.user.setActivity(`~help | butterfly.diamondb.xyz`);
  console.log(
    `Shard ${client.shard.id} has started on ${new Date().toString()}, with ${
      client.users.size
    } users, in ${client.channels.size} channels of ${
      client.guilds.size
    } guilds.`
  );
  reset();
  //execRun("bash notify-webhook.sh", {shell: 'bash', timeout: 60000}, async function(err, stdout, stderr) {
  //	console.log(err);
  //	console.log(stdout + '|' + stderr);
  //});
});

function cycleActivity() {
  if (!cycle) var cycle = 0;
  if (cycle === 0) client.user.setActivity(``);
}

let t = new Throttle();

client.on("guildCreate", async guild => {
  timestamp = new Date().toLocaleString();
  console.log(
    `${timestamp}/+MEMCOUNT:"${guild.memberCount}",NAME:"${guild.name}"`
  );
});

client.on("guildDelete", async guild => {
  timestamp = new Date().toLocaleString();
  console.log(
    `${timestamp}/-MEMCOUNT:"${guild.memberCount}",NAME:"${guild.name}"`
  );
});

function checkifAdmin(bitfield) {
  var test = 0;
  if (bitfield & Discord.Permissions.FLAGS.ADMINISTRATOR) test++;
  if (bitfield & Discord.Permissions.FLAGS.MANAGE_MESSAGES) test++;
  if (test > 0) return true;
  else return false;
}

//async function chechDBvote(author) {
//	if (!author)
//}

async function togglecheck(guild, command, disabledbydefault) {
  if (!command || !guild) return false;
  global.pass = false;
  await aget(`${guild}:${command}`).then(result => {
    //console.log('result for ' + command + ': ' + result);
    if (!result) {
      if (disabledbydefault == 1) global.pass = 0;
      else global.pass = 1;
    } else if (result == 1) {
      global.pass = 1;
    } else if (result == 0) {
      global.pass = 0;
    } else global.pass = 0;
  });
  //console.log('global pass is ' + global.pass);
  if (global.pass == 1) return true;
  if (global.pass == 0) return false;
}

async function Voted(id, site, username) {
  // Voted(message.author.id, "discordbot", message.author.username)
  if (site !== "discordbot") return "Unsupported~!";
  //                                 ^~v always discordbot
  let message;
  if (username) message = `${escMD(username)}, thanks for voting for the bot`;
  else message = `Thanks for voting for the bot`;
  try {
    var result = await aget(`${id}:${site}`);
  } catch (err) {
    return false;
  } finally {
    if (!result) return false;
    else {
      try {
        var json = JSON.parse(result);
        if (json.isWeekend == false) return `${message}!`;
        else if (json.isWeekend == true) return `${message} during weekend!`;
      } catch (e) {
        return `${message}!`;
      }
    }
  }
}

async function VotedFooter(id, site, username) {
  if (site !== "discordbot") return;
  if (await Voted(id, site, username)) {
    if (username) return `Thanks for voting, ${escMD(username)}!`;
    else return "Thanks for voting!";
  } else return;
}

function Limit(message, msgauthor, quiet) {
  if (
    t.throttle({ container: "limit", id: msgauthor, usages: 3, duration: 5 })
  ) {
    return true;
  } else {
    if (quiet == undefined) {
      if (CoolDownMessage.has(msgauthor)) return;
      message.reply("Please slow down...");
      CoolDownMessage.add(msgauthor);
      setTimeout(() => {
        CoolDownMessage.delete(msgauthor);
      }, 2000);
    }
    return false;
  }
}

function apiLimit(message, msgauthor, quiet) {
  if (t.throttle({ container: "api", id: msgauthor, usages: 3, duration: 5 })) {
    return true;
  } else {
    if (quiet == undefined) {
      if (CoolDownMessage.has(msgauthor)) return;
      message.reply("Please slow down...");
      CoolDownMessage.add(msgauthor);
      setTimeout(() => {
        CoolDownMessage.delete(msgauthor);
      }, 2000);
    }
    return false;
  }
}

function osuLimit(message, msgauthor, quiet) {
  if (t.throttle({ container: "osu", id: msgauthor, usages: 3, duration: 5 })) {
    return true;
  } else {
    if (quiet == undefined) {
      if (CoolDownMessage.has(msgauthor)) return;
      message.reply("Please slow down...");
      CoolDownMessage.add(msgauthor);
      setTimeout(() => {
        CoolDownMessage.delete(msgauthor);
      }, 2000);
    }
    return false;
  }
}

function rippleLimit(message, msgauthor, quiet) {
  if (
    t.throttle({ container: "ripple", id: msgauthor, usages: 1, duration: 5 })
  ) {
    return true;
  } else {
    if (quiet == undefined) {
      if (CoolDownMessage.has(msgauthor)) return;
      message.reply("Please slow down...");
      CoolDownMessage.add(msgauthor);
      setTimeout(() => {
        CoolDownMessage.delete(msgauthor);
      }, 2000);
    }
    return false;
  }
}

async function saucenaoLimit(message, msgauthor, quiet) {
  global.saucenao++;
  if (global.saucenao == 30) {
    message.channel.send(
      "Global API limit hit, please slow down! Consider donating to the bot for a potential API rate increase!"
    );
    return false;
  }

  if (await Voted(message.author.id, "discordbot", message.author.username)) {
    return true;
  } else {
    if (
      await t.throttle({
        container: "limit",
        id: msgauthor,
        usages: 1,
        duration: 60
      })
    ) {
      return true;
    } else {
      if (quiet == undefined) {
        if (await CoolDownMessage.has(msgauthor)) return;
        message.channel.send(
          "Seems like this server has hit Saucenao's mere limit of 1 query per minute. Sadly, this is all the bot's author can afford, so please wait."
        );
        CoolDownMessage.add(msgauthor);
        setTimeout(() => {
          CoolDownMessage.delete(msgauthor);
        }, 2000);
      }
      return false;
    }
  }
}

async function reportLimit(message) {
  var limit = await aget(`${message.author.id}:report`);
  if (limit == "true") {
    return false;
  }

  await aset(`${message.author.id}:report`, "true", "EX", 86400);
  return true;
}

function ifStartsWithArray(array, input) {
  if (Array.isArray(array)) {
    for (i = 0; i < array.length; i++) {
      if (input.startsWith(array[i])) return true;
    }
  }

  return false;
}

function ifEqualsArray(array, input) {
  if (Array.isArray(array)) {
    for (i = 0; i < array.length; i++) {
      if (input == array[i]) return true;
    }
  }

  return false;
}

const prefix = "~";
const despacitoCoolDown = new Set();
const CoolDownMessage = new Set();

client.on("message", async message => {
  if (message.author.id == client.user.id) {
    global.commandfreq = global.commandfreq + 1;
  }

  if (message.author.bot) return;

  function AuthorGuildSwap() {
    if (message.guild) return message.guild.id;
    else return message.author.id;
  }

  if (message.content === "des" || message.content === "cito") {
    if (despacitoCoolDown.has(message.author.id)) return;
    if (await togglecheck(message.guild.id, "despacito", 1)) {
      despacitoCoolDown.add(message.author.id);
      setTimeout(() => {
        despacitoCoolDown.delete(message.author.id);
      }, 2000);
      if (message.content === "des") return message.channel.send("pa");
      if (message.content === "cito")
        return message.channel.send("Quiero respirar tu cuello despacito");
    }
  }

  if (
    message.content.includes("https://e-hentai.org/g/") &&
    message.channel.nsfw
  ) {
    if (await togglecheck(message.guild.id, "hentai", 0)) {
      if (await Limit(message, await AuthorGuildSwap(), "quiet")) {
        var splitmessage = message.content.split(" ");
        var links = [];
        for (var i = 0; i < splitmessage.length; i++) {
          if (splitmessage[i].indexOf("https://e-hentai.org/g/") > -1) {
            links.push(splitmessage[i]);
          }
        }
        for (var e = 0; e < links.length; e++) {
          var command = links[e];
          execRun(["hentai/eh.sh", command], async function(
            err,
            stdout,
            stderr
          ) {
            if (err) {
              return;
            } else {
              var lines = stdout.split("\n");
              var firstLine = lines.shift();
              var secondLine = lines.shift();
              var thirdLine = lines.shift();
              var everythingElse = lines.join("\n");
              const embed = new Discord.MessageEmbed()
                .setColor(14934225)
                .setTitle(firstLine)
                .setURL(secondLine)
                .setThumbnail(thirdLine)
                .setDescription(everythingElse);
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            }
          });
        }
      }
      return;
    }
  }

  if (
    message.content.includes("https://nhentai.net/g/") &&
    message.channel.nsfw
  ) {
    if (await togglecheck(message.guild.id, "hentai", 0)) {
      if (await Limit(message, await AuthorGuildSwap(), "quiet")) {
        var splitmessage = message.content.split(" ");
        var links = [];
        for (var i = 0; i < splitmessage.length; i++) {
          if (splitmessage[i].indexOf("https://nhentai.net/g/") > -1) {
            links.push(splitmessage[i]);
          }
        }
        for (var e = 0; e < links.length; e++) {
          var command = links[e];
          execRun(["hentai/nh.sh", command], async function(
            err,
            stdout,
            stderr
          ) {
            if (err) {
              return;
            } else {
              var lines = stdout.split("\n");
              var firstLine = lines.shift(); // pops off first element of array and store in firstLine
              var secondLine = lines.shift();
              var thirdLine = lines.shift();
              var everythingElse = lines.join("\n");
              const embed = new Discord.MessageEmbed()
                .setColor(15541587)
                .setTitle(firstLine)
                .setURL(secondLine)
                .setThumbnail(thirdLine)
                .setDescription(everythingElse);
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            }
          });
        }
      }
      return;
    }
  }

  if (message.content.includes("https://www.youtube.com/watch?v=")) {
    if (await togglecheck(message.guild.id, "youtube", 0)) {
      if (await Limit(message, await AuthorGuildSwap(), "quiet")) {
        var splitmessage = message.content.split(" ");
        var links = [];
        for (var i = 0; i < splitmessage.length; i++) {
          if (
            splitmessage[i].indexOf("https://www.youtube.com/watch?v=") > -1
          ) {
            links.push(splitmessage[i]);
          }
        }
        for (var e = 0; e < links.length; e++) {
          var command = links[e];
          execRun(["c99/youtube.sh", command], async function(
            err,
            stdout,
            stderr
          ) {
            if (err) {
              return;
            } else {
              var lines = stdout.split("\n");
              var firstLine = lines.shift(); // pops off first element of array and store in firstLine
              var secondLine = lines.shift();
              var thirdLine = lines.shift();
              var everythingElse = lines.join("\n");
              const embed = new Discord.MessageEmbed()
                .setColor(16711680)
                .setTitle(firstLine)
                .setURL(secondLine)
                .setThumbnail(thirdLine)
                .setDescription(everythingElse);
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            }
          });
        }
      }
      return;
    }
  }

  if (!message.content.startsWith(prefix)) {
    if (
      message.content.includes("https://osu.ppy.sh/b/") ||
      message.content.includes("https://osu.ppy.sh/beatmapsets") ||
      message.content.includes("https://osu.ppy.sh/s/")
    ) {
      if (
        (await osuLimit(message, await AuthorGuildSwap(), "quiet")) &&
        (await togglecheck(message.guild.id, "osulink", 0))
      ) {
        var splitmessage = message.content.split(" ");
        var links = [];
        for (var i = 0; i < splitmessage.length; i++) {
          if (splitmessage[i].indexOf("https://osu.ppy.sh/") > -1) {
            links.push(splitmessage[i]);
          }
        }

        for (var e = 0; e < links.length; e++) {
          var command = await links[e];
          execRun(
            [`osu/filter.sh`, message.author.id, command],
            async (err, stdout, stderr) => {
              if (err) {
                return message.channel.send("Invalid link?");
              } else {
                var lines = stdout.split("\n");
                var title = lines.shift(); // pops off first element of array and store in firstLine
                var link = lines.shift();
                var thumb = lines.shift();
                var creator = lines.shift();
                var creLink = lines.shift();
                var smallAva = lines.shift();
                var footer = lines.shift();
                var previewMP3 = lines.shift();
                var diffplot = lines.shift();
                var everythingElse = lines.join("\n");

                const embed = new Discord.MessageEmbed()
                  .setAuthor(creator, smallAva, creLink)
                  .setTitle(title)
                  .setURL(link)
                  .setThumbnail(thumb)
                  .setColor(3447003)
                  .setDescription(everythingElse)
                  .setFooter(`${footer}`);

                let votedMsg = await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                );

                let filesList = {};
                if (diffplot) {
                  filesList = [
                    previewMP3,
                    {
                      attachment: diffplot,
                      name: diffplot.split("/")[1]
                    }
                  ];
                  embed.setImage("attachment://" + diffplot.split("/")[1]);
                } else {
                  filesList = [previewMP3];
                }

                message.channel
                  .send(votedMsg, { embed, files: filesList })
                  .then(function() {
                    function check(file, err) {
                      if (err != null)
                        console.log(
                          "Failed to delete file",
                          file,
                          "reason: \n" + err
                        );
                    }

                    fs.unlink(previewMP3, err => check(previewMP3, err));
                    if (diffplot) {
                      fs.unlink(diffplot, err => check(diffplot, err));
                    }
                  });
              }
            }
          );
        }
      }
      return;
    }
  }

  //const prefixes = ['~', `<@${client.user.id}>`];
  //let prefix = false;
  //for (const thisPrefix of prefixes) {
  //	if (message.content.startsWith(thisPrefix)) {
  //		prefix = thisPrefix;
  //	}
  //}

  if (message.content.startsWith(prefix + "hasVoted")) {
    try {
      var res = await Voted(
        message.author.id,
        "discordbot",
        message.author.username
      );
    } finally {
      if (!res || res == false) {
        message.channel.send(
          "You haven't voted yet! Vote now at https://discordbots.org/bot/463221705628975104"
        );
      } else {
        message.channel.send(res);
      }
    }
  }

  if (message.content.startsWith(prefix + "np")) {
    if (await Limit(message, await AuthorGuildSwap())) {
      let mds = ["*", "_", "~"];
      if (message.mentions.users.size > 0) {
        var presenceJSON = message.mentions.users.first().presence.activity;
        authorname = message.mentions.users.first().username;
      } else {
        var presenceJSON = message.author.presence.activity;
        authorname = message.author.username;
      }
      for (i = 0; i <= mds.length; ++i) {
        if (authorname.indexOf(mds[i]) > -1)
          authorname = authorname.replace(
            new RegExp(mds[i], "g"),
            `\\${mds[i]}`
          );
      }
      if (presenceJSON) {
        if (presenceJSON.name.indexOf("osu!") >= 0 && presenceJSON.details) {
          if (await osuLimit(message, await AuthorGuildSwap())) {
            thing = presenceJSON.details.replace(".osu", "");
            var nowPlaying = thing.replace(/\'/g, "\\'").replace(/\\/g, "\\");
            message.channel
              .send(authorname + " is playing " + nowPlaying + "\n")
              .then(msg => {
                execRun(
                  ["osu/np.sh", presenceJSON.state, nowPlaying],
                  async function(err, stdout, stderr) {
                    if (err) {
                      return msg.edit(stderr_handler(err, stderr, stdout));
                    } else {
                      var lines = stdout.split("\n");
                      var firstLine = lines.shift(); // pops off first element of array and store in firstLine
                      var secondLine = lines.shift(); // pops off second element of array and store in secondLine
                      var everythingElse = lines.join("\n");
                      var everythingElseArray = everythingElse.split("\n");
                      let i,
                        j,
                        tarray,
                        chunk = 2,
                        field = [];
                      const embed = new Discord.MessageEmbed();
                      embed.setColor(3447003);
                      embed.setAuthor(firstLine, "", secondLine);
                      embed.setURL("https://osusearch.com");
                      for (
                        i = 0, j = everythingElseArray.length;
                        i < j;
                        i += chunk
                      ) {
                        tarray = everythingElseArray.slice(i, i + chunk);
                        if (tarray[0] && tarray[1]) {
                          embed.addField(tarray[0], tarray[1]);
                        }
                      }
                      embed.setFooter(
                        "Data fetched from https://osusearch.com <3",
                        "http://osusearch.com/static/img/logo.png"
                      );
                      if (everythingElse) {
                        msg.edit(
                          authorname + " is playing " + nowPlaying + "\n",
                          { embed }
                        );
                      } else {
                        msg.edit(
                          authorname +
                            " is playing " +
                            nowPlaying +
                            "\n" +
                            stdout
                        );
                      }
                    }
                  }
                );
              });
          }
        } else if (presenceJSON.type == "LISTENING") {
          if (await apiLimit(message, await AuthorGuildSwap())) {
            title = presenceJSON.details;
            artist = presenceJSON.state;
            album = presenceJSON.assets.largeText;
            appname = presenceJSON.name;
            if (appname == "Spotify") {
              trackid = presenceJSON.syncID;
              message.channel
                .send(
                  authorname +
                    " is listening to " +
                    title +
                    " by " +
                    artist +
                    " [" +
                    album +
                    "] on " +
                    appname
                )
                .then(msg => {
                  execRun(
                    ["spotify/fetch.sh", "trackid-url", trackid],
                    async function(err, stdout, stderr) {
                      if (err) {
                        msg.edit(stderr_handler(err, stderr, stdout));
                      } else {
                        msg.edit(
                          authorname +
                            " is listening to " +
                            title +
                            " by " +
                            artist +
                            " [" +
                            album +
                            "] on " +
                            appname +
                            "\n" +
                            stdout
                        );
                        //message.channel.send(authorname + ' is listening to ' + title + ' by ' + artist + ' [' + album + '] on ' + appname + "\n" + stdout);
                      }
                    }
                  );
                });
            }
          }
        } else {
          if (presenceJSON.details) {
            message.channel.send(
              authorname +
                ' is playing "' +
                presenceJSON.name +
                '" [' +
                presenceJSON.details +
                " / " +
                presenceJSON.state +
                "]"
            );
          } else {
            if (presenceJSON.name.indexOf("osu!") < 0) {
              message.channel.send(
                authorname + ' is playing "' + presenceJSON.name + '"'
              );
            } else {
              message.channel.send("You're not playing anything.");
            }
          }
        }
      } else {
        message.channel.send("You're not playing anything.");
      }
    }
    return;
  }

  if (message.content.startsWith(prefix + "twitch")) {
    let commandr = message.content;
    if (!commandr) {
      const embed = new Discord.MessageEmbed()
        .setTitle(`Invalid argument! Usage: \`~twitch [username]\``)
        .addField(`Example`, "`~twitch thepoon02`");
      message.channel.send(
        await VotedFooter(
          message.author.id,
          "discordbot",
          message.author.username
        ),
        { embed }
      );
    } else {
      var command = shiftstr(commandr);
      execRun(["twitch/fetchjson.sh", command], async (err, stdout, stderr) => {
        if (err)
          return message.channel.send(stderr_handler(err, stderr, stdout));
        var curl = JSON.parse(stdout);
        if (curl.stream == null) {
          return message.channel.send(
            "The streamer is not live, or you may have entered a wrong username."
          );
        } else {
          var game = curl.stream.game;
          var name = curl.stream.channel.display_name;
          var logo = curl.stream.channel.logo;
          var nsfw = curl.stream.channel.mature;
          var preview = curl.stream.preview.large;
          var timestamp = curl.stream.created_at;
          var title = curl.stream.channel.status;
          var type = curl.stream.stream_type;
          var url = curl.stream.channel.url;
          var viewers = curl.stream.viewers;

          if (nsfw == true && !message.channel.nsfw)
            return message.channel.send(
              "Mature content, please go to a NSFW channel!"
            );

          await execRun(
            [`bin/timeago.sh`, "--string", timestamp],
            async (err, stdout, stderr) => {
              if (err) return stderr_handler(err, stderr, stdout);
              const embed = new Discord.MessageEmbed()
                .setColor(4929148)
                .setDescription(
                  `Title: **${title}** / Viewers: ${viewers}\nGame: **${game}**`
                )
                .setFooter(`${stdout} ago`)
                .setAuthor(`${name} is ${type}!`, logo, url)
                .setThumbnail(preview);
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            }
          );
        }
      });
    }
  }

  if (message.content.startsWith(prefix + "pp")) {
    if (await osuLimit(message, await AuthorGuildSwap())) {
      if (message.content === "~pp") {
        message.channel.send({
          embed: {
            color: 3447003,
            title: "Invalid argument! Usage: `~pp [beatmap link] [options]`",
            description: "Refer to `~help pp` for more details."
          }
        });
        return;
      } else {
        command = shiftstr(message.content);
        console.log(command);
        message.channel.send("Fetching data and calculating...").then(msg => {
          execRun(["osu/filter-pp.sh", command], async function(
            err,
            stdout,
            stderr
          ) {
            if (err) {
              msg.edit(
                "Bad link, most likely. If you think this is a mistake, please report at `~report`."
              );
              return;
            } else {
              var lines = stdout.split("\n");
              var firstLine = lines.shift(); // pops off first element of array and store in firstLine
              var secondLine = lines.shift();
              var thirdLine = lines.shift();
              var fourthLine = lines.shift();
              var everythingElse = lines.join("\n");
              const embed = new Discord.MessageEmbed()
                .setTitle(firstLine)
                .setColor(3447003)
                .setURL(secondLine)
                .setThumbnail(thirdLine)
                .setDescription(everythingElse)
                .setFooter(`${fourthLine}`);
              msg.edit({ embed });
              return;
            }
          });
        });
      }
    }
    return;
  }

  if (message.content.startsWith(prefix + "report")) {
    let args = shiftstr(message.content);
    if (await reportLimit(message)) {
      let timestamp = new Date().toLocaleString();
      let log = "";
      log += `LOGS_[${timestamp}]_START\n`;
      log += `+++++++++++++++\n`;
      log += `LOGS_[${timestamp}]_REASON: ${args}\n`;
      log += `+++++++++++++++\n`;

      message.channel.messages
        .fetch({ limit: 35 })
        .then(async messages => {
          for (const val of messages.values()) {
            if (val.author.id === client.user.id) {
              if (val.content) log += `[BOT ] CONTENT___\n${val.content}\n`;
              if (val.embeds) log += `[BOT ] EMBEDS____\n${val.embeds}\n`;
            } else if (val.author.id === message.author.id) {
              if (val.content) log += `[USER] CONTENT___\n${val.content}\n`;
            }
          }
        })
        .catch(err => {
          message.channel.send(
            "Couldn't fetch messages. Contact diamondburned instead."
          );
          console.log(err);
        });

      log += `+++++++++++++++\n`;
      log += `LOGS_[${timestamp}]_END\n`;

      console.log(log);
      pmOwner(
        `User ${escMD(message.author.tag)} (${
          message.author.id
        }) reported, reason: ${escMD(args)}\nCode: \`${timestamp}\``
      );
      message.channel.send(
        "Bug and dump reported with the following reason: " + args
      );
    } else {
      let embed = new Discord.MessageEmbed();
      embed.setAuthor("Quota reached!");
      embed.addField(
        "Quota reached!",
        "You've reached the quota for reporting!"
      );
      embed.addField(
        "Website (along with the support server URL)",
        "https://butterfly.diamondb.xyz"
      );
      embed.addField(
        "DiscordBots page",
        "https://discordbots.org/bot/463221705628975104"
      );
      message.channel.send(
        await VotedFooter(
          message.author.id,
          "discordbot",
          message.author.username
        ),
        { embed }
      );
    }

    return;
  }

  if (message.content === prefix + "about") {
    if (await Limit(message, await AuthorGuildSwap())) {
      let embed = new Discord.MessageEmbed();
      embed.setColor(16777215);
      embed.setAuthor(
        "Despacito Bot, made by diamondburned/Gyrfalcon7",
        "http://u.cubeupload.com/diamondburned/1877jx.png"
      );
      embed.setURL("https://despacito-bot.diamondb.xyz");
      embed.addField("Languages", "Bash and JS");
      embed.addField("Helps", "Fishy, PooN and OMK for providing JS stuff");
      embed.setDescription(
        ">PP calculator from [Francesco149/oppai-ng](https://github.com/Francesco149/oppai-ng). \n>E-Hentai crawler provided by E-Hentai APIs"
      );
      embed.setFooter(
        "Bot made by _diamondburned_#4507",
        "http://u.cubeupload.com/diamondburned/23p4rz.png"
      );
      message.channel.send(
        await VotedFooter(
          message.author.id,
          "discordbot",
          message.author.username
        ),
        { embed }
      );
    }
    return;
  }

  if (message.content === prefix + "sotarks " + "truth") {
    message.channel.send("https://www.youtube.com/watch?v=cJdpHz1X7bQ");
  }

  if (message.content.startsWith(prefix + "sourcequery")) {
    if (await Limit(message, await AuthorGuildSwap())) {
      let command = message.content;
      async function sourcequery_invalidsyntax() {
        const embed = new Discord.MessageEmbed()
          .setColor(11211288)
          .setTitle("Invalid argument! Usage: `~sourcequery server:port`")
          .addField("Example", "`~sourcequery 96.44.130.137:27025`");
        message.channel.send(
          await VotedFooter(
            message.author.id,
            "discordbot",
            message.author.username
          ),
          { embed }
        );
      }
      if (command == `${prefix}sourcequery`) {
        return sourcequery_invalidsyntax();
      } else {
        inputs = command.split(" ", 2);
        address = inputs[1].split(":", 2);
        ip = address[0];
        port = parseInt(address[1]);
        if (!ip || !port) {
          return sourcequery_invalidsyntax();
        }
        message.channel.send("Querying data...").then(msg => {
          sourcequery
            .info(ip, port, 5000)
            .then(async output => {
              if (!output.game) return msg.edit("Nothing found.");
              if (output.vac === 1) {
                var color = 1211307;
              } else {
                var color = 11211288;
              }
              //console.log(output);
              const embed = new Discord.MessageEmbed()
                .setColor(color)
                .setTitle(`/${output.folder}/ ${output.name}`)
                .setDescription(
                  `Address: **${ip}:${port}**\nMap: **${output.map}** / Game: **${output.game}**\nPlayer: **${output.playersnum}**/${output.maxplayers} (Bots: ${output.botsnum})`
                );
              if (
                await Voted(
                  message.author.id,
                  "discordbot",
                  message.author.username
                )
              ) {
                sourcequery.players(ip, port, 5000).then(output2 => {
                  if (output2.length > 0) {
                    let Player = new Array();
                    output2.forEach(val => {
                      let Time = new Date(val.duration * 1000)
                        .toISOString()
                        .substr(11, 8);
                      Player[
                        val.score
                      ] = `${val.name} / ${val.score} / ${Time}`;
                    });
                    embed.addField("Players", Player.reverse().filter(String));
                  }
                  msg.edit({ embed });
                  message.author.send(
                    "JSON data for your server: ```json\n" +
                      JSON.stringify(output) +
                      "```\n```json\n" +
                      JSON.stringify(output2) +
                      "```"
                  );
                });
              } else {
                msg.edit({ embed });
              }
            })
            .catch(error => {
              msg.edit("Timed out. Nothing's probably there.");
              console.log(error);
            });
        });
      }
    }
    return;
  }

  if (message.content.startsWith(prefix + "time")) {
    if (await Limit(message, await AuthorGuildSwap())) {
      let commandraw = message.content;
      if (!message.mentions.users.size == 0) {
        snowflake = message.mentions.users.first().id;
        let s = commandraw.split(" "),
          index;
        if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
          s.splice(index, 1);
          commandraw = s.join(" ");
        }
      } else {
        snowflake = message.author.id;
      }
      command = shiftstr(commandraw);
      execRun(["datetime/datetime.sh", snowflake, command], async function(
        err,
        stdout,
        stderr
      ) {
        if (err) {
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 11211288,
                title: "Please enter a valid city!",
                fields: [
                  {
                    name: "Usage",
                    value: "`~time [city or location]`"
                  },
                  {
                    name: "Example",
                    value: "`~time Perth`"
                  },
                  {
                    name: "Setting time",
                    value: "`~time -s [Linux timezone]`"
                  }
                ]
              }
            }
          );
          return;
        } else {
          var lines = stdout.split("\n");
          var firstLine = lines.shift(); // pops off first element of array and store in firstLine
          var everythingElse = lines.join("\n");
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 3447003,
                title: firstLine,
                description: everythingElse
              }
            }
          );
        }
      });
      //// Thanks OMKelderman
    }
    return;
  }

  if (message.content.startsWith(prefix + "lyrics")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      var command = shiftstr(message.content);
      if (message.content == `${prefix}lyrics`) {
        const embed = new Discord.MessageEmbed()
          .setTitle("Invalid argument! Usage: `~lyrics`")
          .addField("`~lyrics [keywords]`", "Searches Genius for IDs")
          .addField("`~lyrics id [id]`", "Queries Genius for Lyrics from ID");
        message.channel.send(
          await VotedFooter(
            message.author.id,
            "discordbot",
            message.author.username
          ),
          { embed }
        );
        return;
      }

      if (message.content.startsWith(prefix + "lyrics id")) {
        let id = parseInt(shiftstr(command));
        message.channel.send("Querying data...").then(msg => {
          lyricist
            .song(id, { fetchLyrics: true })
            .then(lyrics => {
              const embed = new Discord.MessageEmbed();
              embed.setTitle(lyrics.full_title);
              embed.setURL(lyrics.url);
              embed.setThumbnail(lyrics.header_image_thumbnail_url);
              if (lyrics.lyrics.length > 2000) {
                embed.setDescription(`${lyrics.lyrics.slice(0, 2000)}...`);
              } else {
                embed.setDescription(`${lyrics.lyrics.slice(0, 2000)}`);
              }
              msg.edit({ embed });
            })
            .catch(err => {
              msg.edit("Invalid ID: `" + id + "`");
              return;
            });
        });
      } else {
        command = command;
        message.channel.send("Querying data...").then(msg => {
          execRun(["lyrics/lyrics.sh", command], async function(
            err,
            stdout,
            stderr
          ) {
            if (err) {
              message.channel.send(stderr_handler(err, stderr, stdout));
            }
            var lines = stdout.split("\n");
            var firstLine = lines.shift(); // pops off first element of array and store in firstLine
            var secondLine = lines.shift();
            var everythingElse = lines.join("\n");
            const embed = new Discord.MessageEmbed()
              .setColor(3447003)
              .setTitle(firstLine)
              .setURL(secondLine)
              .setDescription(everythingElse)
              .setFooter("`~lyrics id [id] to print full lyrics`");
            msg.edit({ embed });
          });
        });
      }
    }
    return;
  }

  if (message.content.startsWith(prefix + "aur")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      command = shiftstr(message.content);
      function aur_invalidargs() {
        message.channel.send({
          embed: {
            color: 1545169,
            title: "Invalid argument!",
            fields: [
              {
                name: "Usage",
                value: "`~aur [keywords]`"
              }
            ]
          }
        });
      }
      if (command == `${prefix}aur`) {
        return aur_invalidargs();
      }
      execRun(["utils/aur.sh", command], async function(err, stdout, stderr) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        }
        if (stdout == "Invalid args!") {
          return aur_invalidargs();
        } else if (stdout.indexOf("Showing 0 results") >= 0) {
          message.channel.send("Nothing found :(");
        } else {
          var lines = stdout.split("\n");
          var firstLine = lines.shift(); // pops off first element of array and store in firstLine
          var secondLine = lines.shift(); // pops off second element of array and store in secondLine
          if (!secondLine) {
            return message.channel.send(stdout);
          }

          var everythingElse = lines.join("\n");
          var everythingElseArray = everythingElse.split("\n");
          let i,
            j,
            tarray,
            chunk = 2,
            field = [];
          for (i = 0, j = everythingElseArray.length; i < j; i += chunk) {
            tarray = everythingElseArray.slice(i, i + chunk);
            field.push({
              name: tarray[0],
              value: tarray[1]
            });
          }
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 3447003,
                title: firstLine,
                url: secondLine,
                fields: field,
                footer: {
                  icon_url: "https://aur.archlinux.org/images/favicon.ico",
                  text: "Data fetched from https://aur.archlinux.org <3"
                }
              }
            }
          );
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "packages")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      let commandr = shiftstr(message.content);
      command = shiftstr(message.content);
      function packages_invalidargs() {
        message.channel.send({
          embed: {
            color: 1545169,
            title: "Invalid argument!",
            fields: [
              {
                name: "Usage",
                value:
                  "`~packages [keywords]`\n`~packages exact [package name]`"
              }
            ]
          }
        });
      }
      if (command == `${prefix}packages`) {
        return packages_invalidargs();
      }
      execRun(["utils/packages.sh", command], async function(
        err,
        stdout,
        stderr
      ) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        }
        if (stdout == "Invalid args!") {
          return packages_invalidargs();
        } else if (stdout.indexOf("Showing 0 results") >= 0) {
          message.channel.send("Nothing found :(");
        } else {
          var lines = stdout.split("\n");
          var firstLine = lines.shift(); // pops off first element of array and store in firstLine
          var secondLine = lines.shift(); // pops off second element of array and store in secondLine
          var everythingElse = lines.join("\n");
          var everythingElseArray = everythingElse.split("\n");
          let i,
            j,
            tarray,
            chunk = 2,
            field = [];
          for (i = 0, j = everythingElseArray.length; i < j; i += chunk) {
            tarray = everythingElseArray.slice(i, i + chunk);
            field.push({
              name: tarray[0],
              value: tarray[1]
            });
          }
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 3447003,
                title: firstLine,
                url: secondLine,
                fields: field,
                footer: {
                  icon_url: "https://aur.archlinux.org/images/favicon.ico",
                  text:
                    "Data fetched from https://www.archlinux.org/packages <3"
                }
              }
            }
          );
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "kitsu")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      let commandr = shiftstr(message.content);
      let command = shiftstr(message.content);
      execRun(["anime/kitsu.sh", command], async function(err, stdout, stderr) {
        if (stderr.indexOf("ERR:INV_ARG") > -1) {
          return message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 4206399,
                title: "Invalid arguments!",
                fields: [
                  {
                    name: "Usage",
                    value: "`~kitsu [anime|manga] [keywords]`"
                  },
                  {
                    name: "Search",
                    value: "`~kitsu --search [anime|manga] [keywords]`"
                  }
                ]
              }
            }
          );
        }

        if (err) {
          return message.channel.send(stdout);
        }

        if (stdout.indexOf("Nothing found") >= 0) {
          message.channel.send("Nothing found.");
        } else {
          var lines = stdout.split("\n");
          var firstLine = lines.shift(); // pops off first element of array and store in firstLine
          var secondLine = lines.shift(); // pops off second element of array and store in secondLine
          var thirdLine = lines.shift();
          var fourthLine = lines.shift();
          var everythingElse = lines.join("\n");
          const embed = new Discord.MessageEmbed()
            .setColor(4206399)
            .setTitle(firstLine)
            .setURL(secondLine)
            .setThumbnail(thirdLine)
            .setDescription(everythingElse)
            .setFooter(
              fourthLine,
              "https://kitsu.io/android-chrome-192x192-6b1404d91a423ea12340f41fc320c149.png"
            );
          return message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            { embed }
          );
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "calc")) {
    if (await Limit(message, await AuthorGuildSwap())) {
      let command = shiftstr(message.content);
      if (command == `${prefix}calc`) {
        return message.channel.send({
          embed: {
            color: 3447003,
            title: "Invalid arguments!",
            fields: [
              {
                name: "Usage",
                value: "`~calc [input 1] | [input 2] | ...[input]`"
              },
              {
                name: "Example",
                value: "`~calc 1+1 | sqrt(2)`"
              }
            ]
          }
        });
      }

      execRun(["utils/calc.sh", command], async function(err, stdout, stderr) {
        console.log(command);
        if (err || stderr) {
          message.channel.send(stderr);
        } else {
          const embed = new Discord.MessageEmbed().setColor(3447003);
          let results = stdout.split("\n");
          for (i = 0; i < results.length; i += 2) {
            if (results[i] && results[i + 1])
              embed.addField(results[i], results[i + 1]);
          }

          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            { embed }
          );
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "saucenao")) {
    //let command = shiftstr(message.content)
    let command = message.content.substring(
      message.content.indexOf(" ") + 1,
      message.content.length
    );
    var attachs = message.attachments.array();

    async function saucenao_invalidargs() {
      const embed = new Discord.MessageEmbed()
        .setTitle("Invalid argument! Usage: `~saucenao [URL/embed]`")
        .setDescription(
          `Notice:\n\t>URL has to be a __\`cdn.discordapp.com\`__ URL that ends with a supported format/container\n\t>The rate limit is 1 query / minute.`
        );
      message.channel.send(
        await VotedFooter(
          message.author.id,
          "discordbot",
          message.author.username
        ),
        { embed }
      );
      const errnao = 1;
    }

    if (attachs.length > 0) {
      var link = attachs[0].url;
    } else if (
      [".gif", ".jpg", ".png", ".bmp", ".svg", ".webp"].some(
        container => command.indexOf(container) >= 0
      )
    ) {
      var link = command.split(" ", 1).join("");
    } else {
      saucenao_invalidargs();
      return;
    }

    if (link.includes("https://media.discordapp.net/attachments/") == false) {
      if (link.includes("https://cdn.discordapp.com") == false) {
        saucenao_invalidargs();
        return;
      }
    }

    if (await saucenaoLimit(message, await AuthorGuildSwap())) {
      message.channel.send(`Querying API...`).then(msg => {
        var esc = link;
        execRun(["utils/saucenao.sh", esc], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            msg.edit(stderr_handler(err, stderr, stdout));
          }
          var lines = stdout.split("\n");
          var firstLine = lines.shift();
          var secondLine = lines.shift();
          var everythingElse = lines.join("\n");
          const embed = new Discord.MessageEmbed()
            .setColor(1907997)
            .setTitle(firstLine)
            .setThumbnail(secondLine)
            .setDescription(everythingElse);
          msg.edit(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            { embed }
          );
        });
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "wttr")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      let command = shiftstr(message.content);
      execRun(["utils/wttr.sh", command], async function(err, stdout, stderr) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        }
        if (stdout.indexOf("ERR_NO_STRING") >= 0) {
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 3447003,
                title: "No string(s) specified!",
                fields: [
                  {
                    name: "Usage",
                    value: "`~wttr [-c|-f] [city or location]`"
                  },
                  {
                    name: "Example for Celcius (metric unit)",
                    value: "`~wttr -c Perth`"
                  },
                  {
                    name: "Example for Fahrenheit (imperial/US unit)",
                    value: "`~wttr -f Seattle`"
                  }
                ]
              }
            }
          );
          return;
        } else if (stdout.indexOf("ERR_UNKNOWN_LOCATION") >= 0) {
          message.channel.send("Unknown location!");
        } else {
          message.channel.send(
            (await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            )) + stdout
          );
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "currency")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      let commandr = shiftstr(message.content);
      command = shiftstr(message.content);
      execRun(["c99/currency.sh", command], async function(
        err,
        stdout,
        stderr
      ) {
        if (err) {
          const embed = new Discord.MessageEmbed()
            .setTitle(
              "Invalid argument! Usage: `~currency [value, integer] [from] [to]`"
            )
            .addField("Example", "`~currency 1 USD JPY`")
            .setColor(3750201);
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            { embed }
          );
          return;
        } else {
          message.channel.send(
            (await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            )) +
              "\n" +
              stdout
          );
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "nyaa")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      let commandr = shiftstr(message.content);
      command = shiftstr(message.content);
      execRun(["anime/nyaa.sh", command], async function(err, stdout, stderr) {
        if (err) {
          message.channel.send({
            embed: {
              color: 14855658,
              title: "Invalid argument! Usage: `~nyaa [options] [keywords]`",
              fields: [
                {
                  name: "Options",
                  value: "`[--shift|-s] [n]` shifts 10+`n` pages"
                },
                {
                  name: "Example",
                  value: "`~nyaa -s 7 Wandering Son`"
                }
              ]
            }
          });
          return;
        }

        var lines = stdout.split("\n");
        let firstLine,
          secondLine = false;
        firstLine = lines.shift(); // pops off first element of array and store in firstLine
        secondLine = lines.shift(); // pops off second element of array and store in secondLine
        if (!secondLine) {
          message.channel.send(stdout);
          return;
        }

        const embed = new Discord.MessageEmbed()
          .setTitle(firstLine)
          .setURL(secondLine)
          .setColor(14855658);

        var everythingElse = lines.join("\n");
        var everythingElseArray = everythingElse.split("\n");
        var val,
          odd = 0;
        everythingElseArray.forEach(item => {
          if (odd == 0) {
            val = item;
            odd = 1;
          } else {
            odd = 0;
            embed.addField(val, item);
          }
        });

        //embed.setDescription(everythingElse);

        //for (i = 0, j = everythingElseArray.length; i < j; i + 2) {
        //	tarray = everythingElseArray.slice(i, i+1);
        //	if (tarray[1]) {
        //		embed.addField(tarray[0], tarray[1]);
        //		console.log(tarray[0], tarray[1])
        //	}
        //}
        message.channel.send(
          await VotedFooter(
            message.author.id,
            "discordbot",
            message.author.username
          ),
          { embed }
        );
      });
    }
  }

  if (message.content.startsWith(prefix + "mapsearch")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      function mapsearch(command) {
        execRun(["osu/mapsearch.sh", command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Not found.");
          }
          if (stdout == "Invalid args!") {
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              {
                embed: {
                  color: 13119355,
                  title: "Invalid argument!",
                  fields: [
                    {
                      name: "Usage",
                      value:
                        "`~mapsearch title MAPTITLE | artist MAPARTIST | mapper MAPPER | diffname DIFFICULTY_NAME | [ exact | ranked | qualified | loved]`"
                    },
                    {
                      name: "Example",
                      value:
                        "`~mapsearch title Jitter Doll | mapper -Laura- | exact`"
                    }
                  ]
                }
              }
            );
            return;
          } else if (stdout.indexOf("Showing 0 results") >= 0) {
            message.channel.send("Nothing found :(");
          } else {
            var lines = stdout.split("\n");
            var firstLine = lines.shift(); // pops off first element of array and store in firstLine
            var secondLine = lines.shift(); // pops off second element of array and store in secondLine
            if (!secondLine) {
              message.channel.send(stdout);
              return;
            }

            var everythingElse = lines.join("\n");
            var everythingElseArray = everythingElse.split("\n");
            let i,
              j,
              tarray,
              chunk = 2,
              field = [];
            for (i = 0, j = everythingElseArray.length; i < j; i += chunk) {
              tarray = everythingElseArray.slice(i, i + chunk);
              if (tarray[1]) {
                field.push({
                  name: tarray[0],
                  value: tarray[1]
                });
              }
            }
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              {
                embed: {
                  color: 3447003,
                  title: firstLine,
                  url: secondLine,
                  fields: field,
                  footer: {
                    icon_url: "http://osusearch.com/static/img/logo.png",
                    text: "Data fetched from https://osusearch.com <3"
                  }
                }
              }
            );
          }
        });
      }
      if (message.content === prefix + "mapsearch") {
        message.channel
          .send(
            "The proper syntax for `~mapsearch` is ``~mapsearch title MAPTITLE | artist MAPARTIST | mapper MAPPER | diffname DIFFICULTY_NAME | [ exact | ranked | qualified | loved]`.\nPlease type `search` if you want to continue using the wizard."
          )
          .then(() => {
            message.channel
              .awaitMessages(response => response.content, {
                max: 1,
                time: 30000,
                errors: ["time"]
              })
              .then(collected => {
                if (collected.first().content == "search") {
                  collected
                    .send("Please type the title for your beatmap.")
                    .then(() => {
                      message.channel
                        .awaitMessages(response2 => response2.content, {
                          time: 30000,
                          errors: ["time"]
                        })
                        .then(collected2 => {
                          mapsearch(`title ${collected2.first().content}`);
                        })
                        .catch(() => {
                          message.channel.send("Timed out");
                        });
                    });
                }
              })
              .catch(() => {
                message.channel.send("Timed out.");
              });
          });
      } else {
        let command = shiftstr(message.content);
        console.log("mapsearch debug/ ", command);
        mapsearch(command);
      }
    }
    return;
  }

  if (message.content.startsWith(prefix + "spotify")) {
    if (await apiLimit(message, await AuthorGuildSwap())) {
      let commandr = shiftstr(message.content);
      command = shiftstr(message.content);
      execRun(["spotify/fetch.sh", command], async function(
        err,
        stdout,
        stderr
      ) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        }
        if (stdout == "Invalid args!") {
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 1947988,
                title: "Invalid argument!",
                fields: [
                  {
                    name: "Usage",
                    value: "`~spotify [track|artist|album] [keywords]`"
                  },
                  {
                    name: "Example",
                    value: "`~spotify track November Rain`"
                  }
                ]
              }
            }
          );
          return;
        } else if (stdout.indexOf("Nothing found") >= 0) {
          message.channel.send("Nothing found :(");
        } else {
          var lines = stdout.split("\n");
          var first = lines.shift(); // pops off first element of array and store in firstLine
          var everythingElse = lines.join("\n");
          if (first.indexOf("Showing 5 results") >= 0) {
            const embed = new Discord.MessageEmbed()
              .setColor(1947988)
              .setTitle(first)
              .setDescription(everythingElse);
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              { embed }
            );
          } else {
            message.channel.send(stdout);
          }
        }
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "choose")) {
    if (await Limit(message, await AuthorGuildSwap())) {
      let commandr = shiftstr(message.content);
      command = shiftstr(message.content);
      execRun(["fun/choose.sh", command], async function(err, stdout, stderr) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        }
        if (stdout == "Invalid args!") {
          message.channel.send(
            await VotedFooter(
              message.author.id,
              "discordbot",
              message.author.username
            ),
            {
              embed: {
                color: 3447003,
                title: "Invalid argument!",
                fields: [
                  {
                    name: "Usage",
                    value: "`~choose a or b or c`"
                  }
                ]
              }
            }
          );
          return;
        } else {
          message.channel.send(stdout);
        }
      });
    }
    return;
  }

  if (message.content.includes("--ripple")) {
    if (!(await rippleLimit(message, await AuthorGuildSwap()))) {
      return;
    }
  }

  if (message.content.startsWith(prefix + "osuset")) {
    if (await osuLimit(message, await AuthorGuildSwap())) {
      let command = shiftstr(message.content);
      console.log(command);
      if (
        message.content == prefix + "osuset " ||
        message.content == prefix + "osuset"
      ) {
        message.channel.send(
          "Please enter username! Example: `~osuset cookiezi`"
        );
      } else {
        execRun(["osu/setuser.sh", message.author.id, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send(stderr_handler(err, stderr, stdout));
          } else {
            message.channel.send(stdout);
          }
        });
      }
    }
    return;
  }

  if (message.content.startsWith(prefix + "compare")) {
    message.channel.messages
      .fetch({ limit: 35 })
      .then(async messages => {
        let set = {};
        for (const val of messages.values()) {
          if (val.author.id === client.user.id) {
            if (val.embeds[0]) {
              if (val.embeds[0].author) {
                if (val.embeds[0].author.url) {
                  if (
                    val.embeds[0].author.url.includes("https://osu.ppy.sh/b/")
                  ) {
                    var mapid = val.embeds[0].author.url;
                    var mapname = val.embeds[0].author.name;
                    var description = val.embeds[0].description;
                    var username = val.embeds[0].footer.text.split(" / ", 1)[0];

                    if (hasOwnProperty.call(set, mapid)) {
                      for (const val in set) {
                        for (const field of set[val]) {
                          if (field.description == description) {
                            var fail = 1;
                          }
                        }
                      }

                      if (!fail) {
                        var fail = 0;
                      }

                      if (fail === 0) {
                        set[mapid].push({
                          mapname,
                          username,
                          description
                        });
                      }
                    } else {
                      set[mapid] = [
                        {
                          mapname,
                          username,
                          description
                        }
                      ];
                    }
                  }
                }
              }
            }
          }
        }
        const embed = new Discord.MessageEmbed();
        embed.setTitle("Comparison for the latest 50 messages");
        let count = 0;
        for (const val in set) {
          var desc = "";
          if (set[val].length <= 1) {
            continue;
          }

          for (const field of set[val]) {
            desc += `**${field.username} / ${field.mapname}**\n${field.description}\n\n`;
          }

          count++;

          embed.addField(val, desc);
        }

        if (count !== 0) {
          message.channel
            .send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              { embed }
            )
            .catch(console.error);
        } else {
          message.channel.send("No multiple plays of the same maps are found.");
        }
      })
      .catch(err => {
        message.channel.send(`Error: \n${err}`);
        console.log(err);
      });

    return;
  }

  if (
    (await ifStartsWithArray(
      [prefix + "recent", prefix + "r ", prefix + "R "],
      message.content
    )) ||
    (await ifEqualsArray([prefix + "r", prefix + "R"], message.content))
  ) {
    if (await osuLimit(message, await AuthorGuildSwap())) {
      let commandraw = message.content;
      if (!message.mentions.users.size == 0) {
        snowflake = message.mentions.users.first().id;
        let s = commandraw.split(" "),
          index;
        if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
          s.splice(index, 1);
          commandraw = s.join(" ");
        }
      } else {
        snowflake = message.author.id;
      }
      command = shiftstr(commandraw);
      message.channel.send("Fetching data and calculating...").then(msg => {
        execRun(["osu/recent.sh", "osu", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            msg.edit(stderr_handler(err, stderr, stdout));
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var fourth = lines.shift();
            var fifth = lines.shift();
            var everythingElse = lines.join("\n");
            if (fifth) {
              const embed = new Discord.MessageEmbed()
                .setColor(13119355)
                .setAuthor(first, "", second)
                .setThumbnail(fourth)
                .setFooter(fifth, third, third)
                .setDescription(everythingElse);
              msg
                .edit(
                  await VotedFooter(
                    message.author.id,
                    "discordbot",
                    message.author.username
                  ),
                  { embed }
                )
                .catch(async () => {
                  var lines = stdout.split("\n");
                  var firstLine = lines.shift(); // pops off first element of array and store in firstLine
                  var secondLine = lines.shift();
                  var thirdLine = lines.shift();
                  var fourthLine = lines.shift();
                  var everythingElse = lines.join("\n");
                  const embed = new Discord.MessageEmbed()
                    .setTitle(firstLine)
                    .setColor(3447003)
                    .setURL(secondLine)
                    .setThumbnail(thirdLine)
                    .setDescription(everythingElse)
                    .setFooter(`${fourthLine}`);
                  msg
                    .edit(
                      await VotedFooter(
                        message.author.id,
                        "discordbot",
                        message.author.username
                      ),
                      { embed }
                    )
                    .catch(() => {
                      msg.edit("Error occured. Please `~report` this.");
                    });
                });
            } else {
              msg.edit(stdout);
            }
          }
        });
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "score")) {
    if (await osuLimit(message, await AuthorGuildSwap())) {
      let commandraw = message.content;
      if (!message.mentions.users.size == 0) {
        snowflake = message.mentions.users.first().id;
        let s = commandraw.split(" "),
          index;
        if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
          s.splice(index, 1);
          commandraw = s.join(" ");
        }
      } else {
        snowflake = message.author.id;
      }
      command = shiftstr(commandraw);
      message.channel.send("Fetching data and calculating...").then(msg => {
        execRun(["osu/myscore.sh", "osu", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            const embed = new Discord.MessageEmbed()
              .setColor(11211288)
              .setTitle(
                `Invalid argument! Usage: \`~score [beatmap URL] [username|mention(optional)]\``
              )
              .addField(`Example`, "`~score https://osu.ppy.sh/b/1642274`")
              .addField(
                `Notes`,
                `- The link has to be a beatmap link (usually has \`/b/\`), not a beatmap**set** link.\n- If you think there's an error, please report at \`~report\` along with the beatmap link.`
              );
            msg.edit({ embed });
            timestamp = new Date().toLocaleString();
            console.log(
              `Error at ${timestamp}!` +
                err +
                "\n" +
                "std: " +
                stderr +
                "\n" +
                stdout
            );
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var fourth = lines.shift();
            var fifth = lines.shift();
            var everythingElse = lines.join("\n");

            const embed = new Discord.MessageEmbed()
              .setColor(13119355)
              .setAuthor(first, "", second)
              .setThumbnail(fourth)
              .setFooter(fifth, third, third);

            if (!second) {
              var everythingElseArray = everythingElse.split("\n");
              let i,
                j,
                chunk = 5;
              for (i = 0, j = everythingElseArray.length; i < j; i += chunk) {
                tarray = everythingElseArray.slice(i, i + chunk);
                if (tarray[0] && tarray[1]) {
                  embed.addField(
                    tarray[0],
                    `${tarray[1]}\n${tarray[2]}\n${tarray[3]}\n${tarray[4]}`
                  );
                }
              }
            } else {
              console.log(second);
              embed.setDescription(everythingElse);
            }

            if (fourth) {
              msg.edit(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            } else {
              msg.edit(stdout);
            }
          }
        });
      });
    }
    return;
  }

  if (message.content.startsWith(prefix + "localtop")) {
    var users = new Array();
    var opts = "";

    message.guild.members.forEach(member => {
      users.push(`${member.id}:${member.displayName}`);
    });

    args = [
      `osu/localtop.sh`,
      message.content == "" ? "osu" : message.content,
      opts
    ];
    args.push(...users);

    console.log(args);

    execRun(
      args,
      async function(err, stdout, stderr) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        }

        message.channel.send("```" + stdout + "```");
      },
      true
    ); // keep args array how it is
  }

  if (
    (message.content.startsWith(prefix + "osu") ||
      message.content.startsWith(prefix + "mania") ||
      message.content.startsWith(prefix + "taiko") ||
      message.content.startsWith(prefix + "ctb") ||
      message.content.includes("https://osu.ppy.sh/u/") ||
      message.content.includes("https://osu.ppy.sh/users/")) &&
    !message.content.startsWith(prefix + "osuset")
  ) {
    if (await osuLimit(message, await AuthorGuildSwap())) {
      if (
        message.content.startsWith(prefix + "osu ") ||
        message.content === prefix + "osu" ||
        message.content.includes("https://osu.ppy.sh/u/") ||
        message.content.includes("https://osu.ppy.sh/users/")
      ) {
        try {
          var invalid = false;
          var snowflake = false;
          var links = false;
          if (
            message.content.startsWith(prefix + "osu ") ||
            message.content === prefix + "osu"
          ) {
            let commandraw = message.content;
            if (!message.mentions.users.size == 0) {
              snowflake = message.mentions.users.first().id;
              let s = commandraw.split(" "),
                index;
              if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
                s.splice(index, 1);
                commandraw = s.join(" ");
              }
            } else snowflake = message.author.id;
            links = shiftstr(commandraw);
          } else if (
            message.content.includes("https://osu.ppy.sh/u/") ||
            message.content.includes("https://osu.ppy.sh/users/")
          ) {
            if (!(await togglecheck(message.guild.id, "osulink", 0)))
              return (invalid = true);
            var splitmessage = message.content.split(" ");
            var links = new Set();
            snowflake = message.author.id;
            for (var i = 0; i < splitmessage.length; i++) {
              if (
                splitmessage[i].indexOf("https://osu.ppy.sh/u/") > -1 ||
                splitmessage[i].indexOf("https://osu.ppy.sh/users/") > -1
              ) {
                let message_arr = splitmessage[i].split("/");
                var topush = message_arr[4];
                var push = topush.replace(/\'/g, "\\'").replace(/\\/g, "\\");
                links.add(push);
              }
            }
          }
        } finally {
          if (invalid === true) return;
          if (!snowflake) return message.channel.send("Error at snowflake.");
          if (links.constructor === Set) {
            for (let link of links) {
              osu(snowflake, link);
            }
          } else {
            osu(snowflake, links);
          }
          function osu(snowflake, command) {
            execRun(
              ["osu/lookup.sh", "osu", snowflake, command],
              async function(err, stdout, stderr) {
                if (err) {
                  message.channel.send("Profile not found, sadly...");
                  return;
                } else {
                  const embed = new Discord.MessageEmbed();
                  var lines = stdout.split("\n");
                  var first = lines.shift(); // pops off first element of array and store in firstLine
                  var second = lines.shift();
                  var third = lines.shift();
                  var fourth = lines.shift();
                  var fifth = lines.shift();
                  var everythingElse = lines.join("\n");
                  var recents = everythingElse.indexOf("//MOST_RECENT//");
                  if (recents !== -1) {
                    var events = everythingElse.substring(recents).split("\n");
                    everythingElse = everythingElse.substring(0, recents);
                    events.shift();
                    for (e = 0; e <= events.length; e += 2) {
                      if (events[e + 1]) {
                        embed.addField(events[e], events[e + 1]);
                      }
                    }
                  }
                  if (fourth) {
                    embed.setColor(13119355);
                    embed.setAuthor(first, third, second);
                    embed.setURL(second);
                    embed.setThumbnail(fourth, fourth);
                    embed.setDescription(everythingElse);
                    embed.setFooter(fifth);
                    message.channel.send(
                      await VotedFooter(
                        message.author.id,
                        "discordbot",
                        message.author.username
                      ),
                      { embed }
                    );
                  } else {
                    message.channel.send(stdout);
                  }
                }
              }
            );
          }
        }
        return;
      }

      if (
        message.content.startsWith(prefix + "mania ") ||
        message.content === prefix + "mania"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/lookup.sh", "mania", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var fourth = lines.shift();
            var everythingElse = lines.join("\n");
            if (fourth) {
              const embed = new Discord.MessageEmbed()
                .setColor(13119355)
                .setAuthor(first, third, second)
                .setURL(second)
                .setThumbnail(fourth, fourth)
                .setDescription(everythingElse)
                .setTimestamp();
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            } else {
              message.channel.send(stdout);
            }
          }
        });
        return;
      }

      if (
        message.content.startsWith(prefix + "taiko ") ||
        message.content === prefix + "taiko"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/lookup.sh", "taiko", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var fourth = lines.shift();
            var everythingElse = lines.join("\n");
            if (fourth) {
              const embed = new Discord.MessageEmbed()
                .setColor(13119355)
                .setAuthor(first, third, second)
                .setURL(second)
                .setThumbnail(fourth, fourth)
                .setDescription(everythingElse)
                .setTimestamp();
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            } else {
              message.channel.send(stdout);
            }
          }
        });
        return;
      }

      if (
        message.content.startsWith(prefix + "ctb ") ||
        message.content === prefix + "ctb"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/lookup.sh", "ctb", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var fourth = lines.shift();
            var everythingElse = lines.join("\n");
            if (fourth) {
              const embed = new Discord.MessageEmbed()
                .setColor(13119355)
                .setAuthor(first, third, second)
                .setURL(second)
                .setThumbnail(fourth, fourth)
                .setDescription(everythingElse)
                .setTimestamp();
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            } else {
              message.channel.send(stdout);
            }
          }
        });
        return;
      }

      if (
        message.content.startsWith(prefix + "osutop ") ||
        message.content === prefix + "osutop"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/top.sh", "osu", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var everythingElse = lines.join("\n");
            if (third) {
              const embed = new Discord.MessageEmbed()
                .setColor(13119355)
                .setAuthor(first, third, second)
                .setDescription(everythingElse);
              message.channel.send(
                await VotedFooter(
                  message.author.id,
                  "discordbot",
                  message.author.username
                ),
                { embed }
              );
            } else {
              message.channel.send(stdout);
            }
          }
        });
        return;
      }

      if (
        message.content.startsWith(prefix + "maniatop ") ||
        message.content === prefix + "maniatop"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/top.sh", "mania", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var everythingElse = lines.join("\n");
            const embed = new Discord.MessageEmbed()
              .setColor(13119355)
              .setAuthor(first, third, second)
              .setDescription(everythingElse);
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              { embed }
            );
          }
        });
        return;
      }

      if (
        message.content.startsWith(prefix + "taikotop ") ||
        message.content === prefix + "taikotop"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/top.sh", "taiko", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var everythingElse = lines.join("\n");
            const embed = new Discord.MessageEmbed()
              .setColor(13119355)
              .setAuthor(first, third, second)
              .setDescription(everythingElse);
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              { embed }
            );
          }
        });
        return;
      }

      if (
        message.content.startsWith(prefix + "ctbtop ") ||
        message.content === prefix + "ctbtop"
      ) {
        let commandraw = message.content;
        if (!message.mentions.users.size == 0) {
          snowflake = message.mentions.users.first().id;
          let s = commandraw.split(" "),
            index;
          if ((index = s.indexOf(`<@!${snowflake}>`)) >= "-1") {
            s.splice(index, 1);
            commandraw = s.join(" ");
          }
        } else {
          snowflake = message.author.id;
        }
        command = shiftstr(commandraw);
        execRun(["osu/top.sh", "ctb", snowflake, command], async function(
          err,
          stdout,
          stderr
        ) {
          if (err) {
            message.channel.send("Profile not found, sadly...");
            return;
          } else {
            var lines = stdout.split("\n");
            var first = lines.shift(); // pops off first element of array and store in firstLine
            var second = lines.shift();
            var third = lines.shift();
            var everythingElse = lines.join("\n");
            const embed = new Discord.MessageEmbed()
              .setColor(13119355)
              .setAuthor(first, third, second)
              .setDescription(everythingElse);
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              { embed }
            );
          }
        });
        return;
      }
    }
  }

  //if (message.content.startsWith(prefix + "frequency")) {
  //	message.channel.send('Commands ran so far: **' + commandfreq + '**');
  //}

  if (message.content.startsWith(prefix + "ping")) {
    if (await Limit(message, await AuthorGuildSwap())) {
      try {
        const m = await message.channel.send("Bash is the superior language.");
        m.edit(
          `\Latency is ${m.createdTimestamp -
            message.createdTimestamp}ms. API Latency is ${Math.round(
            client.ws.ping
          )}ms.`
        );
      } catch (err) {
        message.channel.send("Failed to ping.");
      }
    }
  }

  if (message.content.startsWith(prefix + "toggle")) {
    if (await checkifAdmin(message.member.permissions.bitfield)) {
      if (message.content === prefix + "toggle") {
        const embed = new Discord.MessageEmbed()
          .setColor(3447003)
          .setTitle("Invalid argument! Usage: `~toggle [feature]`")
          .addField("`despacito`", "Toggles a spammable Despacito response")
          .addField("`hentai`", "Toggles n/ehentai link parsing")
          .addField(
            "`osulink`",
            "Toggles osu! beatmap link parsing + previewing"
          )
          .addField("`youtube`", "Toggles YouTube link parsing");
        message.channel.send(
          await VotedFooter(
            message.author.id,
            "discordbot",
            message.author.username
          ),
          { embed }
        );
        return;
      } else {
        function toggle(guild, command) {
          if (!command || !guild) return;
          redis.get(`${guild}:${command}`, (err, result) => {
            if (err) {
              console.log(err);
              return;
            }
            if (result == 1) {
              redis.set(`${guild}:${command}`, 0);
              message.channel.send(`\`${command}\` disabled`);
            } else if (result == 0) {
              redis.set(`${guild}:${command}`, 1);
              message.channel.send(`\`${command}\` enabled`);
            } else {
              redis.set(`${guild}:${command}`, 1);
              message.channel.send(`\`${command}\` enabled`);
            }
          });
        }
        command = message.content.replace(prefix + "toggle ", "");
        if (
          command === "despacito" ||
          command === "hentai" ||
          command === "osulink" ||
          command === "youtube"
        )
          toggle(message.guild.id, command);
        else message.channel.send(`Unknown toggle: \`${command}\``);
      }
    } else {
      message.channel.send(
        "You have to have the Administrator or Manage Messages role to access this command."
      );
    }
  }

  if (
    message.content.startsWith(prefix + "help") ||
    message.content.startsWith(prefix + "man")
  ) {
    if (await Limit(message, await AuthorGuildSwap())) {
      command = shiftstr(message.content);
      execRun(["utils/help", command], async function(err, stdout, stderr) {
        if (err) {
          message.channel.send(stderr_handler(err, stderr, stdout));
        } else {
          var lines = stdout.split("\n");
          var firstLine = lines.shift(); // pops off first element of array and store in firstLine
          var everythingElse = lines.join("\n");
          if (command === "") command = "main page";

          const embed = new Discord.MessageEmbed()
            .setColor(3447003)
            .setTitle(firstLine + ": " + command)
            .setDescription(everythingElse);
          message.author
            .send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              embed
            )
            .then(() => {
              message.reply("Check your Direct Messages.");
            })
            .catch(err => {
              message.channel.send("", embed);
            });
        }
      });
    }

    return;
  }

  if (!message.guild) return;
  if (
    (await checkifAdmin(message.member.permissions.bitfield)) ||
    message.author.id === "170132746042081280"
  ) {
    if (message.content.startsWith(prefix + "say")) {
      if (await Limit(message, await AuthorGuildSwap())) {
        sayMessage = message.content.replace(`${prefix}say`, "");
        // Then we delete the command message (sneaky, right?). The catch just ignores the error with a cute smiley thing.
        message.delete().catch(O_o => {});
        // And we get the bot to say the thing:
        message.channel.send(sayMessage);
      }
    }

    if (message.content.startsWith(prefix + "sudo")) {
      if (await Limit(message, await AuthorGuildSwap())) {
        var messageCommand = message.content
          .split(" ")
          .splice(1)
          .join(" ");
        function err(string) {
          message.channel.send(`**Error:** ${string}`);
          return;
        }

        if (messageCommand.startsWith("warn")) {
          var args = messageCommand
            .split(" ")
            .splice(1)
            .join(" ");
          if (!args) {
            return err("Invalid args! Usage: `~warn @User Reason`");
          }

          if (args.startsWith("init")) {
            var Channel = message.mentions.channels.first();
            if (!Channel) {
              return err("No channels inputted!");
            }

            if (
              Channel.permissionsFor(
                message.guild.roles.find(r => r.name === "@everyone")
              ).has("SEND_MESSAGES") == true
            ) {
              return err("Don't allow write access for role `@everyone`.");
            }

            if (!Channel.permissionsFor(client.user.id).has("ADMINISTRATOR")) {
              return err("The bot requires permission `ADMINISTRATOR`.");
            }

            await aset(`${message.guild.id}:warnchannel`, Channel.id);

            message.channel.send(
              "Channel successfully set to " + Channel.toString()
            );

            return;
          }

          var reason = messageCommand
            .split(" ")
            .splice(2)
            .join(" ");
          if (!reason) {
            var reason = "No reason.";
          }

          if (message.mentions.users.size > 0) {
            var targetUser = message.mentions.users.first();
          } else {
            message.channel.send("Error: No mentions found!");
            return;
          }

          var targetChannelID = await aget(`${message.guild.id}:warnchannel`);
          if (!targetChannelID) {
            return err(
              "Warn isn't yet set up! Please run `~warn init #channel`"
            );
          }

          var targetObj = {
            avatar: targetUser.avatarURL({ size: 64 }),
            snowflake: targetUser.id,
            tag: targetUser.tag,
            warnedby: message.author.tag,
            totalwarns: 0,
            channel: client.channels.get(targetChannelID)
          };

          var warnMessages = await targetObj.channel.messages
            .fetch()
            .then(messages => messages.filter(msg => msg.author.bot));
          warnMessages.forEach(message => {
            var userID = parseInt(message.embeds[0].title.split(" ")[2]);
            if ((userID = targetObj.snowflake)) {
              targetObj.totalwarns++;
            }
          });

          let embed = new Discord.MessageEmbed()
            .setTitle("/warn/ on " + targetObj.snowflake)
            .setThumbnail(targetObj.avatar)
            .addField("Warned by", "`" + targetObj.warnedby + "`")
            .addField("Target", "`" + targetObj.tag + "`")
            .addField("Reason", reason);

          var confJSON = '{"kick": 3, "ban15": 4, "permban": -1}';
          var parseConf = JSON.parse(confJSON);
          try {
            parseConf = JSON.parse(targetObj.topic);
          } catch (err) {
            // Well, whatever
          }

          if (targetObj.totalwarns == parseConf.kick) {
            targetUser
              .kick()
              .then(() => {
                embed.addField("Actions taken", "Kick");
              })
              .catch(() => {
                embed.addField("Actions taken", "None, kick failed.");
              });
          } else if (targetObj.totalwarns == parseConf.ban15) {
            // eh
          } else if (targetObj.totalwarns == parseConf.permban) {
            // eh
          }

          targetObj.channel.send(embed);
        }

        if (messageCommand.startsWith("purge ")) {
          message.delete().catch(O_o => {});
          let deleteCount = parseInt(shiftstr(messageCommand));
          deleteCount++;
          if (!deleteCount || deleteCount <= 1 || deleteCount >= 99)
            return message.channel.send("Number has to be `[1..99]`.");

          const fetched = await message.channel.messages.fetch({
            limit: deleteCount
          });
          message.channel.bulkDelete(fetched).catch(error => {
            message.channel.send(
              `Couldn't delete messages because of: ${error}`
            );
          });
        }

        if (message.author.id === "170132746042081280") {
          if (messageCommand.startsWith("vote")) {
            let args = shiftstr(messageCommand);
            message.delete().catch(O_o => {});
            if (message.mentions.users.first().id) {
              let snowflake = message.mentions.users.first().id;

              if (args.startsWith("remove")) {
                await adel(`${snowflake}:discordbot`)
                  .then(() => {
                    message.channel.send(
                      `Removed <@${snowflake}> from votelist!`
                    );
                  })
                  .catch(err => {
                    timestamp = new Date().toLocaleString();
                    message.channel.send(
                      `Failed to remove Redis at ${timestamp}! Error: \`\`\`\n${err}\`\`\``
                    );
                    console.log(
                      `Failed removing vote Redis at ${timestamp}: \n${err}`
                    );
                  });
              } else {
                let JSON = `{"bot":"${client.user.id}","user":"${snowflake}","type":"upvote","query":"","isWeekend":true}`;

                // Might wanna change me in the future (discordbot)
                await aset(`${snowflake}:discordbot`, JSON, "EX", 86400)
                  .then(() => {
                    message.channel.send(
                      `Added <@${snowflake}> into votelist!`
                    );
                  })
                  .catch(err => {
                    timestamp = new Date().toLocaleString();
                    message.channel.send(
                      `Failed to set Redis at ${timestamp}: \`${err}\`! JSON value: \`\`\`json\n${JSON}\`\`\``
                    );
                    console.log(
                      `Failed setting vote Redis at ${timestamp}: ${err} \n${JSON}`
                    );
                  });
              }
            } else {
              message.channel.send(
                "Missing mention! `~sudo vote [|remove] <mention>`"
              );
              return;
            }
          }

          if (messageCommand.startsWith("eval ")) {
            const clean = text => {
              if (typeof text === "string")
                return text
                  .replace(/`/g, "`" + String.fromCharCode(8203))
                  .replace(/@/g, "@" + String.fromCharCode(8203));
              else return text;
            };

            timestamp = new Date().toLocaleString();
            command = shiftstr(messageCommand);
            if (command.startsWith("--broadcast ")) {
              try {
                command = shiftstr(command);
                var results = await client.shard.broadcastEval(command);
                message.channel.send(
                  `Completed at ${timestamp}. Check console for logs.`
                );
                console.log(
                  `stdout for broadcastEval ${command} at ${timestamp}:\n` +
                    results
                );
              } catch (err) {
                console.log(timestamp + err);
                message.channel.send(`Error at ${timestamp}`);
              }
            } else {
              try {
                var results = eval(command);
                if (typeof results !== "string") {
                  results = require("util").inspect(results);
                }
                message.channel.send(
                  `Completed at ${timestamp}. Check console for logs.`
                );
                console.log(
                  `stdout for eval at ${timestamp}:\n` + clean(results)
                );
              } catch (err) {
                message.channel.send(`Error at ${timestamp}`);
                console.log(timestamp + err);
              }
            }

            return;
          }

          if (messageCommand.startsWith("activity ")) {
            messageCommand = shiftstr(messageCommand);
            client.user.setActivity(messageCommand);
            return;
          }

          if (message.content === prefix + "sudo stats") {
            message.delete().catch(O_o => {});
            execRun(["./bot-usage"], function(err, stdout, stderr) {
              if (err) {
                message.channel.send(stderr_handler(err, stderr, stdout));
              } else {
                var lines = stdout.split("\n");
                var first = lines.shift();
                var everythingElse = lines.join("\n");
                const embed = new Discord.MessageEmbed();
                embed.setColor(3447003);
                embed.setTitle(`${first}`);
                embed.setDescription(`${everythingElse}`);
                message.channel.send({ embed });
              }
            });

            return;
          }

          if (messageCommand === "shards") {
            message.delete().catch(O_o => {});
            var sudoshards = {};
            const reducer = (accumulator, currentValue) =>
              accumulator + currentValue;
            await client.shard
              .broadcastEval("this.guilds.size")
              .then(results => {
                global.sudo_guilds_size = results;
              })
              .catch(console.log);
            await client.shard
              .broadcastEval("this.users.size")
              .then(results => {
                global.sudo_users_size = results;
              })
              .catch(console.log);
            await client.shard
              .broadcastEval("this.ws.ping")
              .then(results => {
                global.sudo_ping = results;
              })
              .catch(console.log);
            var embed = new Discord.MessageEmbed();
            embed.setTitle(`${client.shard.count} shards /`);
            embed.setColor(3447003);
            embed.setFooter(
              `Total guilds: ${global.sudo_guilds_size.reduce(
                reducer
              )} / Total users: ${global.sudo_users_size.reduce(reducer)}`
            );
            var entry = 0;
            global.sudo_guilds_size.forEach(() => {
              entry1 = entry + 1;
              embed.addField(
                `Shard ${entry1}`,
                "Guilds: " +
                  global.sudo_guilds_size[`${entry}`] +
                  "\n Users: " +
                  global.sudo_users_size[`${entry}`] +
                  "\n Ping: " +
                  global.sudo_ping[`${entry}`],
                true
              );
              entry = entry1;
            });
            message.channel.send(
              await VotedFooter(
                message.author.id,
                "discordbot",
                message.author.username
              ),
              { embed }
            );
            return;
          }

          if (messageCommand.startsWith("exec ")) {
            let command = shiftstr(messageCommand);
            if (messageCommand.startsWith("exec --raw ")) {
              command = shiftstr(command);
            }
            message.delete().catch(O_o => {});
            let output = "";
            exec(
              `source config && ${command}`,
              { shell: "bash", timeout: 60000 },
              async function(err, stdout, stderr) {
                if (stderr) {
                  output = stderr + "\n" + stdout;
                } else if (stdout) {
                  output = stdout;
                } else {
                  output = "Error!";
                }
                if (output.length > 2048) {
                  message.channel.send("Content longer than 2048 characters!");
                  console.log(output);
                } else {
                  if (message.content.startsWith(prefix + "sudo exec --raw ")) {
                    message.channel.send(output);
                  } else {
                    const embed = new Discord.MessageEmbed()
                      .setColor(3447003)
                      .setTitle("Output")
                      .addField("stdout", "```" + output + "```")
                      .addField("Length", output.length)
                      .setTimestamp();
                    message.channel.send(
                      await VotedFooter(
                        message.author.id,
                        "discordbot",
                        message.author.username
                      ),
                      { embed }
                    );
                  }
                }
              }
            );

            return;
          }

          if (messageCommand.startsWith("debugPresence")) {
            //let command = shiftstr(messageCommand);
            let mds = ["*", "_", "~"];
            if (message.mentions.users.size > 0) {
              var presenceJSON = message.mentions.users.first().presence
                .activity;
              authorname = message.mentions.users.first().username;
            } else {
              var presenceJSON = message.author.presence.activity;
              authorname = message.author.username;
            }

            for (i = 0; i <= mds.length; ++i) {
              if (authorname.indexOf(mds[i]) > -1)
                authorname = authorname.replace(
                  new RegExp(mds[i], "g"),
                  `\\${mds[i]}`
                );
            }

            if (presenceJSON) {
              message.channel.send(
                authorname +
                  " ```json" +
                  `\n${JSON.stringify(presenceJSON)}\n` +
                  "```"
              );
            } else {
              message.channel.send("Empty");
            }
          }
        }
      }
    }
    return;
  }
});

client.login(config.BOT_TOKEN);
