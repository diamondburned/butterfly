# despacito-bot

A private bot in heavy development.

## Documentation

#### [butterfly.diamondb.xyz](https://butterfly.diamondb.xyz/)

## Requirements

- `npm` (node v8)
- `bash` (v4.2 or later)
- `gnuplot`
- Some binaries (`JSON.sh`, `oppai`,...)

## Credits

- JSON parser from https://github.com/dominictarr/JSON.sh
- URL encoder from https://gist.github.com/cdown/1163649
- Lots and lots of help from https://github.com/RumbleFrog
- `oppai-ng` (forked) from https://github.com/Francesco149/oppai-ng
- `ojsama` (with mods) from https://github.com/Francesco149/ojsama

## License

Mozilla Public License v2.0 (MPL)