#!/usr/bin/env bash
args=( $@ )
set -- "${args[@]}"

if [[ "$*" = "" || "$*" = " " ]]; then
	echo $"Choose? \`~choose a or b or c\`"
	exit
fi

if [[ $# = 1 ]]; then
	echo "One choice? Really? Seperate choices with `or`."
fi

STRING=$(echo $@ | sed 's~ or ~\n~g')

SAVEIFS=$IFS
IFS=$'\n'
ARRAY=( $STRING )
IFS=$SAVEIFS

rand=$[$RANDOM % ${#ARRAY[@]}]

answer=${ARRAY[$rand]}
echo "$(tr '[:lower:]' '[:upper:]' <<< ${answer:0:1})${answer:1}"'.'