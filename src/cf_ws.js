var http = require('http')
var cloudscraper = require('cloudscraper')

function respond(res, sig, msg) {
    res.writeHead(sig, msg.split(`\n`)[0], {})
    res.write(msg)

    return res.end()
}

module.exports = {
    ListenCF: async function (host, port) {
        http.createServer(function (req, res) {
            let url = new URL(`http://${host}:${port}${req.url}`)
             params = url.searchParams.get("url")

            if (!params) {
                return respond(res, 400, "Missing `url' form.")
            }

            try {
                new URL(params)
            } catch {
                return respond(res, 400, "Invalid URL.")
            }

            cloudscraper.get(params, (erro, resp, body) => {
                if (erro) {
                    return respond(res, 500, `Failed\n${JSON.stringify(erro)}\n${JSON.stringify(body)}}`)
                }

                res.writeHead(200, "Success", {})
                res.write(body)
                return res.end()
            })
        }).listen(port, host, () => {
            console.log(`Listening at ${host}:${port}?url=`)
        })
    }
}
