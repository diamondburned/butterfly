package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/go-redis/redis"
	"gopkg.in/ini.v1"
)

// Request is the struct for data received
type Request struct {
	Bot       string `json:"bot"`
	User      string `json:"user"`
	Type      string `json:"type"`
	Query     string `json:"query"`
	IsWeekend bool   `json:"isWeekend"`
}

func initialize() (*ini.File, *redis.Client) {
	cfg, err := ini.Load("config")
	if err != nil {
		log.Fatalln(err)
	}

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	
	return cfg, client
}

var cfg, client = initialize()

// Lazy ass
func err_handler(err error) {
	log.Printf("Failed at ", err)
}

// Lazy ass ep 2
func quickini(ini *ini.File, key string) string {
	return ini.Section("").Key(key).String()
}

// Maim func
func main() {
	// Load in port        v PORT variable in config
	PORT := quickini(cfg, "discordbots_port")

	// Verbosity
	log.Println("Listening on port", PORT)

	//          Root v   v Function
	http.HandleFunc("/", handleWebhook)

	// v Log       v Open HTTP server v PORT
	log.Fatalln(http.ListenAndServe(":"+PORT, nil))
}

// Check token
func verifytoken(token string) bool {
	//                     v Token variable in config
	TOKEN := quickini(cfg, "discordbots_token")

	// Check token
	if token != TOKEN {
		log.Println("Invalid token, rejecting.")
		return false
	}

	return true
}

func handleWebhook(writer http.ResponseWriter, request *http.Request) {
	// Verbosity
	//fmt.Printf("Headers: %v\n", request.Header)

	// Verify token
	if !verifytoken(request.Header.Get("Authorization")) {
		return
	}

	// Extract response Body into variable BODY
	BODY, err := ioutil.ReadAll(request.Body)
	if err != nil {
		err_handler(err)
		return
	}

	// JSON variable
	var JSON Request

	// Parse body into JSON from struct Request
	json.Unmarshal([]byte(BODY), &JSON)

	// Only when the request type is an upvote
	if JSON.Type != "upvote" {
		log.Println("ERR: Not an upvote")
		log.Println("Type:", JSON.Type, "/ User:", JSON.User)
		return
	}

	// Only if the bot is yours
	if JSON.Bot != quickini(cfg, "BOT_ID") {
		log.Println("ERR: Bot ID doesn't match")
		log.Println("Bot ID:", JSON.Bot)
		return
	}

	//fmt.Printf("JSON: %v\n", JSON)

	// Verbosity
	log.Println("Passed - User", JSON.User, "voted, weekend", JSON.IsWeekend)

	// Append into Redis
	set(JSON.User+":discordbot", string(BODY[:]))

}

// Redis set function minimized
func set(key string, value string) {
	err := client.Set(key, value, 86400 * time.Second).Err()
	if err != nil {
		log.Fatalln(err)
	}
}

// Redis get function minimized
func get(key string) string {
	val, err := client.Get(key).Result()
	if err == redis.Nil {
		return ""
	} else if err != nil {
		log.Fatalln(err)
	}

	return val
}
