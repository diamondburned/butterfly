#!/usr/bin/env bash
set -- "$@"
export IFS=$'\n'
export GLOBIGNORE='*'

if [[ -z "$*" ]]; then
	echo "No input."
	exit 1
fi

INPUT=$(echo -e "${@// | /\\n}")

if [[ "$*" = '`'*'`' ]]; then
	INPUT="${INPUT/#\`/}"
	INPUT="${INPUT/%\`/}"
fi

IFS=$'\n' read -d '' -r -a INPUT <<< "$INPUT"

export BC_LINE_LENGTH=0

if [[ "${#INPUT[@]}" -ge 12 ]]; then
	>&2 echo "**Error** / Too many operations! Please keep it at 12 maximum."
	exit 1
fi

for EACH in "${INPUT[@]}"; do
	RESULTS=$(echo "$EACH" | bc -l &> /dev/stdout)
	if [[ "$RESULTS" = *"syntax error"* ]]; then
		>&2 echo "Syntax error at \`$EACH\`."
		exit 1
	elif [[ "$RESULTS" = *"Runtime error"* ]]; then
		>&2 echo '**Error** at `'$EACH'` / '$(echo -n "$RESULTS" | awk -F ': ' '{print $2}')
		exit 1
	elif [[ "$RESULTS" = "(standard_in)"* ]]; then
		err=$(echo -n "$RESULTS" | head -n1 | cut -d':' -f2-)
		err="${err/# /}"
		>&2 echo '**Error** at `'$EACH'` / '$(echo ${err^})
		exit 1
	else
		OUTPUT=$(echo -n "${RESULTS}")
		if [ "${#OUTPUT}" -ge 2044 ]; then
			echo 'Number is too large! Digit count: '"${#OUTPUT}"
		else
			echo "$EACH"
			case "$OUTPUT" in
			     *".")	echo "${OUTPUT%%.*}"
					;;
				 "."*)	echo '**0**'"$OUTPUT"
					;;
				"-."*)	echo "**-0**.""${OUTPUT:2}"
					;;
				*"."*)	echo '**'"$OUTPUT" | sed 's~\.~**.~g'
					;;
				    *)	echo '**'"$OUTPUT"'**'
					;;
			esac
		fi
	fi
done
