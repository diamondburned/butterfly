#!/usr/bin/env bash
set -eo pipefail

function err {
	echo "Error triggered, exiting"
}
trap err ERR


string="$@"
if [[ $string = "" ]]; then
	echo "ERR_NO_STRING"
	exit 1
fi

if [[ $string = "-c"* ]]; then
	string="${@:2}"
	UNIT="celcius"
elif [[ $string = "-f"* ]]; then
	string="${@:2}"
	UNIT="fahrenheit"
else
	string="$@"
fi

string=$(echo "$string" | sed 's/[^A-Za-z ]*//g' | sed 's~ ~+~g' )

case $UNIT in
	"fahrenheit")	searchurl='wttr.in/'"$string"'?0?T?u'
		;;
	*)				searchurl='wttr.in/'"$string"'?0?T?m'
		;;
esac

CURL=$(curl -s "$searchurl")

if [[ $CURL = *"ERROR: Unknown location"* ]]; then
	echo "ERR_UNKNOWN_LOCATION"
	exit 1
fi

echo '```'"$CURL"'```'