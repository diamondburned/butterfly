#!/usr/bin/env bash
urlencode() {
	# https://gist.github.com/cdown/1163649
	old_lc_collate=$LC_COLLATE
	LC_COLLATE=C
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
			*) printf '%%%02X' "'$c" ;;
		esac
	done
	LC_COLLATE=$old_lc_collate
}


args=( $@ )
set -- "${args[@]}"

if [[ $1 = "exact" ]]; then
	thing="${@:2}"
	EXACT=1
else
	thing="$@"
fi

keyword=$(urlencode "$thing")

useragent="Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"

if [[ $EXACT = "1" ]]; then
	searchurl='https://www.archlinux.org/packages/search/json/?name='"$keyword"
else
	searchurl='https://www.archlinux.org/packages/search/json/?q='"$keyword"
fi

JSON=$(curl -s -A "$useragent" "$searchurl" | sh JSON.sh)

TOTAL=6
FAILED=0
for e in {0..5}; do
	OUT=$(echo "$JSON" | grep -F '["results",'$e',"pkgbase"]')
	if [[ $OUT = "" ]]; then
		((FAILED++))
	else
		:
	fi
done

RESULTScount=$(( $TOTAL - $FAILED ))
if [[ $RESULTScount = "1" ]]; then
	echo 'Showing '"$RESULTScount"' result.'
else
	echo 'Showing '"$RESULTScount"' results.'
fi

echo 'https://www.archlinux.org/packages/?sort=&q='"$keyword"

for i in {0..5}; do
	NAME=$(echo "$JSON"			| grep -F '["results",'$i',"pkgbase"]'	| awk -F '	' '{print $2}'	| sed -e 's/^"//' -e 's/"$//')
	VERSION=$(echo "$JSON"		| grep -F '["results",'$i',"pkgver"]'	| awk -F '	' '{print $2}'	| sed -e 's/^"//' -e 's/"$//')
	DESCRIPTION=$(echo "$JSON"	| grep -F '["results",'$i',"pkgdesc"]'	| awk -F '	' '{print $2}'	| sed -e 's/^"//' -e 's/"$//')
	ARCHITECTURE=$(echo "$JSON"	| grep -F '["results",'$i',"arch"]'		| awk -F '	' '{print $2}'	| sed -e 's/^"//' -e 's/"$//')
	REPOSITORY=$(echo "$JSON"	| grep -F '["results",'$i',"repo"]'		| awk -F '	' '{print $2}'	| sed -e 's/^"//' -e 's/"$//')
	URL='https://www.archlinux.org/packages/'"$REPOSITORY"'/'"$ARCHITECTURE"'/'"$NAME"'/'
	if [[ $NAME = "" ]]; then
		:
	else
		echo $((i+1))'/ '"$NAME"' / '"$VERSION"
		echo '['"$REPOSITORY"'/'"$ARCHITECTURE"'/'"$NAME"']('"$URL"') / '"$DESCRIPTION"
	fi
done

#date -d@226 -u +%M:%S

