#!/usr/bin/env bash
. config
. bin/funcs.bash
string="$*"

#echo Saucenao is disabled until further notice. Sorry for the inconvenience.
#exit 0

export LC_ALL=C.UTF-8

#if [[ $string != "https://media.discordapp.net/attachments/"* ]] || \
#   [[ $string != "https://cdn.discordapp.com/attachments/"*   ]]; then
#	echo $"Please either upload to Discord with the command as body or have a \`cdn.discordapp.com\` link as an argument."
#	exit 0
#fi

string=$(sed 's/?.*//' <<< "$string")

formats=( '.gif' '.jpg' '.png' '.bmp' '.svg' '.webp' )
CORRECT=0
for format in "${formats[@]}"; do
	if [[ "$string" = *"${format}" ]]; then
		CORRECT=$(( $CORRECT + 1 ))
	fi
done

if [[ $CORRECT != "1" ]]; then
	echo Invalid format.
	exit 1
fi

string=$(echo -n "$string")
URL="$string"
CURL=$(curl -s -d "api_key=${saucenao}" -d "db=999" -d "output_type=2" -d "numres=3" -d "url=${URL}" "https://saucenao.com/search.php" | sh JSON.sh -b) || { exit 1; }

CURLstatus=$(echo "$CURL" | grep -F '["header","status"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//')
	if [[ $CURLstatus != 0 ]]; then
		exit 1
	fi
CURLresults=$(echo "$CURL" | grep -F ',"header","similarity"]	' | wc -l)
#CURLresults=$(echo "$CURL" | grep -F '["header","results_requested"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//')
echo "Showing ${CURLresults} results"
CURLthumb="$(echo "$CURL" | grep -F '["results",0,"header","thumbnail"]' | cut -d'	' -f2 | sed -e 's/^"//' -e 's/"$//' | sed 's~\\~~g')"
echo "$CURLthumb" > tmp/thumb
THUMBavail=$(curl -s -D - "$CURLthumb" -o /dev/null | head -n1)
if [[ $THUMBavail = *"200" ]]; then
	echo "$THUMBavail" # CURLthumb
else
	echo "https://saucenao.com/images/static/banner.gif"
fi
ENTRY=0

until [[ "$ENTRY" = $(( $CURLresults )) ]]; do
	CURLengname=$(jsonparse "$CURL" '["results",'$ENTRY',"data","eng_name"]')
	CURLtitle=$(jsonparse "$CURL" '["results",'$ENTRY',"data","title"]')
	CURLindexname=$(jsonparse "$CURL" '["results",'$ENTRY',"header","index_name"]')

	[[ -z $CURLindexname ]] && exit 1

	if [[ ! -z "$CURLengname" ]]; then
		name=$(echo -e "$CURLengname")
	elif [[ ! -z "$CURLtitle" ]]; then
		name=$(echo -e "$CURLtitle")
	else
		name=$(echo -e "$CURLindexname")
	fi

	CURLsimil=$(jsonparse "$CURL" '["results",'$ENTRY',"header","similarity"]')
	CURLurl=$(jsonparse "$CURL" '["results",'$ENTRY',"data","ext_urls",0]')
	echo "$CURL" &> /tmp/saucenao.tmp
	if [[ $name != "" ]]; then
		echo "**${CURLsimil}** / ["${name}"](${CURLurl})"
	fi
	ENTRY=$(( $ENTRY + 1 ))
done
