#!/usr/bin/env bash
urlencode() {
	# https://gist.github.com/cdown/1163649
	LC_COLLATE=C
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
			*) printf '%%%02X' "'$c" ;;
		esac
	done
	LC_COLLATE=""
}

. config
source bin/funcs.bash

args=( $@ )
set -- "${args[@]}"

shift=1

STRING=()
until [[ $# = 0 ]]; do
	case $1 in
		--sort)
			sort="$2" # popularity, name
			if ! [[ "$sort" = "popularity" || "$sort" = "name" ]]; then
				echo "Invalid arguments! \`--sort\` only takes in \`popularity\` or \`name\`."
				exit
			fi
			shift
			shift
			;;
		-q|--query)
			query="$2" # name, name-desc
			shift
			shift
			;;
		-s|--shift)
			shift="$2"
			shift
			shift
			;;
		*)
			STRING+=( "$1" )
			shift
			;;
	esac
done

set -- "${STRING[@]}"

[[ -z $query ]] && query="name-desc"

thing="$*"
keyword=$(urlencode "$thing")

useragent="Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"
searchurl="https://aur.archlinux.org/rpc/?v=5&type=search&by=${query}&arg=${keyword}"

JSON=$(curl -s -A "$useragent" "$searchurl" | sh JSON.sh -b)

echo "$JSON" > tmp/aur

IFS=$'\n'
JSONarr=( $(echo "$JSON" | tail -n+4) )

entries=$(json-parse "$JSON" '["resultcount"]')

pkgName=()
pkgDescription=()
pkgVersion=()
pkgPopularity=()

#2Name
#5Version
#6Description
#9Pop

function processString() {
	input="$1"
	c1="${input#*	}" # get real string
	c2="${c1/#\"/}"
	c3="${c2/%\"/}"
	echo -en "$c3"
}

entry=0
until [[ $entry -eq $entries ]]; do	
	nameint=$(( entry*14+3 ))
	descint=$(( entry*14+5 ))
	versint=$(( entry*14+4 ))
	popuint=$(( entry*14+8 ))

	pkgName+=( "$(			processString "${JSONarr[$nameint]}")" )
	pkgDescription+=( "$(	processString "${JSONarr[$descint]}")" )
	pkgVersion+=( "$(		processString "${JSONarr[$versint]}")" )
	pkgPopularity+=( "$(	processString "${JSONarr[$popuint]}")" )

	((entry++))
done

entry=0
sortCache=()
for e in "${pkgPopularity[@]}"; do
	sortCache+=( "${pkgPopularity[$entry]}|${entry}" )
	((entry++))
done

sortedCache=( $(printf "%s\n" "${sortCache[@]}" | sort -r -n -t'|') )
sortedEntries=( $(cut -d"|" -f2 <<< "${sortedCache[*]}") )

shift=$(( shift - 1 ))
rangestart=$(( 5 * shift))

rangeend=$(( rangestart + 4 ))

int=$rangestart
prints=()

while true; do
	if [[ "$sort" = "name" ]]; then
		entry=$int
	else
		entry=${sortedEntries[$int]}
	fi

	[[ $int -eq $entries ]] && break

	prints+=( "${pkgName[$entry]} / ${pkgPopularity[$entry]} / ${pkgVersion[$entry]}" )
	prints+=( '[AUR Page](https://aur.archlinux.org/packages/'"${pkgName[$entry]}"') / '"${pkgDescription[$entry]}" )
	[[ $int -eq $rangeend ]] && break
	((int++))
done

echo "Showing from $((rangestart+1)) to $((int+1))"
echo 'https://aur.archlinux.org/packages/?O=0&K='"$keyword"

printf "%s\n" "${prints[@]}"

